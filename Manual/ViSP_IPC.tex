\documentclass[11pt]{article}

\usepackage{amsmath,siunitx,bm,graphicx}
\usepackage[letterpaper,margin=1in]{geometry}

\newcommand{\ion}[2]{#1\,\uppercase\expandafter{\romannumeral#2\relax}}

\begin{document}

\title{The Instrument Performance Calculator of the\break
Visible Spectro-Polarimeter (ViSP)}

\author{R.\ Casini \& A.\ G.\ De Wijn}

\maketitle

The Visible Spectro-Polarimeter (ViSP) is one of the first-light polarimetric instruments of the Daniel K.~Inouye Solar Telescope (DKIST).
It is a wavelength versatile, grating-based spectrograph, with three spectral arms (or channels) that can be positioned within the angular range of the diffraction pattern of the grating, so to be able to perform multi-wavelength observations of the solar spectrum.
The ViSP relies on the use of low-order (up to $\sim 15$) reflective echelle gratings to have a sufficiently broad diffraction angular range to enable many interesting combinations of spectral diagnostics.

The ViSP Instrument Performance Calculator (IPC) is a tool to explore the performance of ViSP configurations.
For a given configuration it calculates expected signal-to-noise, spatial resolution and field of view, spectral resolution and bandwidth, map size and duration, and data rate and volume.
The interface allows the user full control over the spectrograph configuration.
However, because of the ViSP's ability to tune continuously over the visible solar spectrum (380 to 900~nm) in \emph{each} of its spectral channels, the most efficient configuration of the spectrograph for a desired set of wavelength regions typically results from the optimization of the spectrograph performance over a large parameter space, consisting of the grating angle, the diffraction order for each of the specified wavelengths, the angular position of each spectral channel, the pairings of the three desired wavelengths with the spectral channels, and, if applicable, the grating.
To this end, the ViSP IPC is paired with a ViSP Instrument Configuration Optimizer (ICO).

The optimization's merit function maximizes the spectrograph efficiency at the desired wavelengths constrained by the ViSP design to only allow configurations that do not lead to mechanical interference among the spectral channels.
The optimizer reads in, and interpolates over, a database of measured vector efficiencies by default.
If no measurements are available, it falls back on a scalar theory of diffraction \cite{CN14}.
It can also be forced to use the scalar calculation to estimate the grating efficiency of modestly polarizing diffraction gratings.
The output of the ICO can be conveniently imported into the IPC.

\begin{figure}[t!]
	\centering
	\includegraphics[width=.85\hsize]{layout}
	\caption{\label{fig:layout}
	Geometric convention adopted by the ViSP IPC and ICO for the definition of the 
	incidence and diffraction angles (respectively, $\alpha$ and $\beta$) 
	relative to the grating normal $\bm{n}$. The direction of the incident 
	radiation is $-z$, whereas that for the diffracted radiation is $+\zeta$. 
	The deviation angle between the diffracted and incident radiation is 
	defined by $\delta=\alpha+\beta$, and is a negative quantity for the
	normal operation of the ViSP (assuming the usual convention of 
	counterclockwise angles being positive). The angle $\varphi$ measures 
	the inclination of the normal to the blazed
	facets of the grating, $\bm{b}$, measured from the grating normal.}
\end{figure}

\section{The ViSP IPC GUI}

\begin{figure}[t!]
	\centering
	\includegraphics[scale=0.4]{ViSP_IPC_GUI}
	\caption{\label{fig:ipcgui}The ViSP IPC GUI in advanced mode showing a configuration for the triplet of spectral lines \ion{H}{1} 656.3~nm, \ion{Ca}{2} 393.4~nm, and \ion{Ca}{2} 854.2~nm.}
\end{figure}

The ViSP IPC GUI is written in the Interactive Data Language (IDL).
It is started from the IDL command line by issuing the command
\begin{verbatim*}
calculator_gui
\end{verbatim*}
The IPC can be started in \emph{advanced} mode that grants access to all input fields by adding the keyword \texttt{/advanced}.
The IPC can be forced to calculate grating efficiencies using a scalar calculation by adding the keyword \texttt{/scalar}.
A DKIST operator can enable the DSP export functionality by starting the calculator by adding the keyword \texttt{/operator}.
Here, we will discuss the IPC inputs and outputs in each section of the GUI.

The ViSP IPC GUI requires at least IDL version 8.0 or better.
For users that do not have access to IDL, or do not have a license for a recent version, a pre-compiled \texttt{sav} file is provided that can be run with the IDL Virtual Machine that is freely availble from Harris Geospatial Solutions, Inc.
It can be started from the command line by issuing the command
\begin{verbatim*}
idl -vm=calculator_gui.sav
\end{verbatim*}

Figure~\ref{fig:ipcgui} shows the ViSP IPC GUI.

\subsection{ViSP Configuration}

This section holds parameters not specific to an arm of the spectrograph, i.e., grating, slit, area to map, camera, modulator, operation time, and data volume.

\begin{description}
	\item[Name] This field holds the name of this ViSP configuration.
	\item[Mode] ViSP has two distinct modes of operation.
		This drop-down allows the user to select the desired mode.
		In \emph{polarimetric} mode ViSP records the full state of polarized light.
		The slit is stationary during data acquisition, and subsequently moved to the next position.
		In \emph{intensity} mode ViSP records only intensity.
		The slit is moved continuously during data acquisition.
		ViSP can map large areas quickly in intensity mode.
		The ``Map Setup'' and ``Camera \&\ Modulator'' areas have different options depending on the mode selected.
	\item[Grating] ViSP is designed to accommodate multiple gratings.
		This drop-down allows the user to select the desired grating.
		Currently only one grating is available.
	\item[Tilt Angle] The desired tilt angle of the grating in degrees.
		The ViSP IPC adheres to the geometric convention described in Fig.~\ref{fig:layout}.
	\item[Slit]ViSP has a library of 5 slits with widths ranging from \ang{;;0.0282} to \ang{;;0.2130}.
		This drop-down allows the user to select the desired slit.
\end{description}

\subsection{Map Setup}

\begin{description}
	\item[Slit Step (polarimetric mode)] The desired step size between slit positions in arcseconds.
	\item[Slit Velocity (intensity mode)] The desired slit velocity in arcseconds per second.
	\item[Slit Positions (polarimetric mode)] The desired number of slit positions.
	\item[Exposures (intensity mode)] The desired number of exposures.
	\item[Map Width] Calculated size of the resulting map in arcseconds.
	\item[Map Repeats] Desired number of repeats of the map.
	\item[Mu] Cosine of the angle between the surface normal and the line of sight to the observer.
		This parameter is used to correct signal-to-noise calculation for the effects of limb darkening.
\end{description}

\subsection{Camera \&\ Modulator}

\begin{description}
	\item[Integration Time (polarimetric mode)] The desired integration time per position of the slit.
	\item[Frame Rate (intensity mode)] Desired camera frame rate.
	\item[Modulation States (polarimetric mode)] The desired number of modulation states.
		ViSP requires at least 5 modulation states to determine the full Stokes vector due to symmetry of the modulator.
		Larger numbers of modulation states result in higher modulation efficiency, but slower modulation rate.
	\item[Frame Rate (polarimetric mode)] Calculated camera frame rate.
	\item[Exposure Time (intensity mode)] Calculated exposure time.
	\item[Modulation Rate (polarimetric mode)] Calculated modulation rate.
	\item[Modulation Cycles per Integration (polarimetric mode)] Calculated number of modulation cycles per integration.
	\item[Modulation Efficiency (polarimetric mode)] Calculated estimated efficiency of the polarimetric modulation.
\end{description}

\subsection{Time \&\ Data}

\begin{description}
	\item[Map Time] Calculated time required for the map.
	\item[Map Cadence] Calculated map cadence.
		This includes the time to reset the instrument to start the next map.
	\item[Total Time] Calculated total time to complete the operation.
	\item[Duty Cycle] Calculated ratio of time spent acquiring observations and total time spent.
	\item[Data Rate] Calculated average data rate during the operation.
	\item[Data Volume] Calculated total volume of data for the operation.
\end{description}

\subsection{Buttons}

\begin{description}
	\item[Update] Force an update of all calculated values.
		The GUI will update if a field is changed and an event is generated.
		In some cases no event will be generated and it is required to update the calculated values manually by pressing this button.
	\item[Close] Close the GUI.
	\item[Load Config] Load a configuration from file.
	\item[Save Config] Save the current configuration to file.
	\item[DSP Export] Export the current configuration to a file that can be imported by the ViSP IP creator tool.
	\item[Optimizer] Start the ViSP ICO.
	\item[Load Optimizer Output] Configurations found by the ICO can be selected from this drop-down menu and subsequently loaded by pressing the \textbf{Load} button.
\end{description}

\subsection{Arms}

There are three identical sections for the three arms.

\subsubsection{Arm Inputs}

\begin{description}
	\item[Enable] Enable or disable the arm.
		If the arm is disabled it will be parked so as to not interfere with the other arms, if possible.
	\item[Position] Desired arm position in degrees.
		The ViSP IPC adheres to the geometric convention described in Fig.~\ref{fig:layout}.
	\item[Order] Desired diffraction order.
	\item[Spectral Binning] Desired binning of camera pixels in the spectral dimension.
	\item[Spatial Binning] Desired binning of camera pixels in the spatial dimension.
	\item[Spectral ROI Length] Desired length of the spectral region of interest in pixels.
		The spectral ROI length is limited to 890~pixels.
		ViSP uses Andor Zyla cameras that have $2560\times2160$ pixels.
		The spectrum is imaged in the short direction of the camera, and the two beams are placed next to one another.
		There is a bit of dead space between the two images, resulting in this limit.
	\item[Spectral ROI Start] Desired starting position of the spectral region of interest in pixels.
	\item[Spatial ROI Length] Desired length of the spatial region of interest in pixels.
	\item[Spectral ROI Start] Desired starting position of the spectral region of interest in pixels.
		The IPC does not use this field and it is not editable.
	\item[Coherent Illumination] The choice of slit width has bearing on whether the illumination of the slit aperture can be considered spatially coherent or incoherent, from the point of view of the extended source.
		A slit width that samples at or below the spatial resolution limit of the source (which is typically determined by a combination of diffraction from the system aperture and the atmospheric seeing) can be considered to be coherently illuminated.
		Since the conditions of coherent and incoherent illumination affect the modeling of the spectral resolution of a slit spectrograph, it is possible to control the assumption of the illumination condition using this drop-down menu.
		If set to ``Automatic'' the IPC will select the mode using an approximate decision algorithm.
		This feature is only available in advanced mode.
\end{description}

\subsubsection{Arm Outputs}

\begin{description}
	\item[Wavelength] Calculated central wavelength for the spectrograph arm configuration.
	\item[Grating Efficiency] Efficiency of the grating at the arm angle and diffraction order specified.
		The calculation of this value employs the scalar theory of diffraction.
	\item[Vignetting Loss] Losses due to vignetting at the collimator lens, at the grating, and at the camera lens.
	\item[Slit Diffraction Loss] Losses due to diffraction at the slit \cite{CdW14}.
	\item[Transmission] Transmission of the DKIST and ViSP optics at the wavelength of interest.
	\item[Detector QE] Quantum Efficiency of the detector at the wavelength of interest.
	\item[System Efficiency] Total system efficiency.
	\item[Camera Duty Cycle] The camera duty cycle, i.e., fraction of the inverse of the frame rate that the camera is exposed.
		The exposure time may have to be shorter than dictated by the frame rate in order to avoid saturation.
		The duty cycle will be $1$ except in cases where the overall system efficiency is very high, or the frame rate is low.
	\item[Ic SNR/Pixel/Integration] Estimated continuum intensity signal-to-noise per pixel per integration.
	\item[Spatial FWHM] Calculated full width at half maximum of the spatial point spread function in arcseconds.
	\item[Spatial Sampling] Number of pixels that correspond to the spatial FWHM.
	\item[Spatial Resolution] Predicted instrument spatial resolution.
	\item[FOV Height] Calculated height of the field of view.
	\item[Spectral FWHM] Calculated full width at half maximum of the spectral point spread function in pm.
	\item[Spectral Sampling] Number of pixels that correspond to the spectral FWHM.
	\item[Spectral Resolution] Predicted instrument spectral resolving power.
	\item[Bandwidth] Calculated spectral bandwidth.
	\item[Spectrum Plot] The plot shows the Quiet-Sun spectrum from the NSO Kitt-Peak FTS Solar Flux Atlas \cite{Ku84} in black, with continuum normalized to 1.
		The expected signal-to-noise for Stokes~I is plotted in solid blue.
		The dotted blue line shows the expected signal-to-noise for Stokes~Q, U, and~V.
		The spectra are convolved with an approximate spectral profile.
		The green shaded area indicates the pixels recorded.
		Red shaded areas fall on the detector but are outside of the specified spectral ROI.
\end{description}

\section{The ViSP ICO GUI}

\begin{figure}[t!]
	\centering
	\includegraphics[scale=0.4]{ViSP_ICO_GUI}
	\caption{\label{fig:icogui}The ViSP ICO GUI in advanced mode.}
\end{figure}

The ViSP ICO GUI is written in the Interactive Data Language (IDL).
It is started from the ViSP IPC GUI or via the IDL command line by issuing the command
\begin{verbatim*}
optimizer_gui
\end{verbatim*}
The ICO can be started in \emph{advanced} mode that grants access to all input fields by adding the keyword \texttt{/advanced}.
The ICO can be forced to calculate grating efficiencies using a scalar calculation by adding the keyword \texttt{/scalar}.
Here, we will discuss the ICO inputs and outputs in each section of the GUI.

Figure~\ref{fig:icogui} shows the ViSP ICO GUI.

\subsection{Configuration}

\begin{description}
	\item[Number of Lines] The ICO allows the user to specify a number of lines of interest that may be larger than the three arms of ViSP.
		The ICO will attempt to find ViSP configurations of singlets, doublets, or triplets of lines as specified by the combination option, testing every possible combination.
		The user may then explore the configurations found by the ICO to select the most desirable combination.
	\item[Optimze as] This drop-down is used to select if lines should be combine as triplets or doublets, or should be optimized for in isolation.
	\item[Use Arms] When combining lines into doublets or when optimizing single lines the user can select which spectrograph arms or arm should be used.
		The focal length of the arms increases from arm 1 to arm 3 so that the user can choose to prioritize, e.g., bandwidth over spectral resolution.
	\item[Minimum Efficiency] Configurations in which the grating efficiency drops below this level are discarded.
		By default this is set to a reasonably conservative value of 0.05.
		This field is only editable in advanced mode.
	\item[Allow Shadowing] When enabled this allows configurations in which $|\beta|>|\alpha|$, i.e., the grating is illuminated in a ``shadowing'' configuration.
		This is normally not allowed.
		This feature is only available in advanced mode.
\end{description}

\subsection{Grating}

\begin{description}
	\item[Grating] This drop-down allows the user to select a particular grating to use.
		Currently only one grating is available.
		By default the ICO tests all gratings available.
		This drop-down is only changeable in advanced mode.
	\item[Min. Tilt] Minimum grating tilt angle permitted, set to a grating-dependent minimum value by default.
		This field is only editable in advanced mode.
		The ViSP ICO adheres to the geometric convention described in Fig.~\ref{fig:layout}.
	\item[Max. Tilt] Maximum grating tilt angle permitted, set to a grating-dependent maximum value by default.
		This field is only editable in advanced mode.
		The ViSP ICO adheres to the geometric convention described in Fig.~\ref{fig:layout}.
\end{description}

\subsection{LHS Parameters}

The ICO adopts the Latin Hypercube Sampling (LHS) of the parameter space in order to efficiently approach the optimal spectrograph configuration for a given science case via a random search.
Once a first solution better than the initial guess is found, the algorithm begins narrowing down the search over the next iterations in a neighborhood of that solution. 

The default values typically guarantee that an optimal solution is always found if it exists.
However, since it is a random process, in some cases the optimizer may converge to a less optimal solution or may fail to find a solution when one does exist.

LHS parameters can only be changed from their default values in advanced mode.

\begin{description}
	\item[Rate] Parameter that controls the minimum acceptable improvement rate of the merit function at each search iteration.
		Lower values require higher rates of improvement.
	\item[Steps] The number of sample points for each dimension of the parameter space.
	\item[Err. Reduct.] The size reduction factor of the search space at the end of each search iteration.
	\item[Iterations] The number of search iterations used for the optimization.
\end{description}

\subsection{Buttons}

\begin{description}
	\item[Close] Close the ICO window.
	\item[Load] Load a configuration from file.
	\item[Save] Save the current configuration to file.
	\item[Optimize] Run the optimizer.
		If the ICO was started from the IPC results will be automatically loaded in the drop-down menu in the IPC.
		If the ICO was started stand-alone a new window will pop up with results.
\end{description}

\subsection{Line Entries}

For each line the GUI allows the user to enter parameters.

\begin{description}
	\item[Wavelength] The desired wavelength of interest.
	\item[Min. Pos.] Minimum permissible position of the arm in degrees, set to the most extended position available by default.
		The ViSP ICO adheres to the geometric convention described in Fig.~\ref{fig:layout}.
		This field is only editable in advanced mode.
	\item[Max. Pos.] Maximum permissible position of the arm in degrees, set to the position closest to the Littrow configuration by default.
		The ViSP ICO adheres to the geometric convention described in Fig.~\ref{fig:layout}.
		This field is only editable in advanced mode.
	\item[Overlap] When enabled, override the mechanical constraints of the ViSP in the optimization.
		This feature may be used to quantify the mechanical proximity of close pairs of spectral lines, e.g., as a way to assess whether it is possible to observe them within the same spectral channel, but may result in configurations that cannot be achieved in reality.
		This feature is only available in advanced mode.
\end{description}

\begin{thebibliography}{2}

	\bibitem{CN14} R.\ Casini \& P.\ G.\ Nelson, \textit{On the intensity 
		distribution function of blazed reflective diffraction gratings}, 
		JOSA\ A, 31, 2179 (2014)

	\bibitem{CdW14} R.\ Casini \& A.\ G.\ de Wijn, \textit{On the instrument 
		profile of slit spectrographs}, JOSA A, 31, 2002 (2014)

	\bibitem{Ku84} R.\ L.\ Kurucz, I.\ Furenlid, J.\ Brault, \& L. Testerman,
		\emph{Solar Flux Atlas from 296 to 1300\,nm}, National Solar
		Observatory, Sunspot, NM (1984)

\end{thebibliography}

\end{document}

