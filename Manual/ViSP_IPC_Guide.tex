\documentclass[11pt]{article}

\usepackage{amsmath,siunitx,bm,graphicx,ragged2e}
\usepackage[letterpaper,margin=1in]{geometry}

\newcommand{\ion}[2]{#1\,\uppercase\expandafter{\romannumeral#2\relax}}

\begin{document}

\title{The Step-by-Step Guide to the Instrument Performance Calculator of the Visible Spectro-Polarimeter}

\author{A.\ G.\ De Wijn}

\maketitle

\section{Decide on Wavelengths to Observe}

Based on desired diagnostics, determine suitable lines to observe.
Some suggestions: \SI{630.2}{\nano\meter} for photospheric magnetic field, \SI{656.3}{\nano\meter} for chromospheric structure and dynamics, \SI{854.2}{\nano\meter} for chromospheric structure, dynamics, and magnetic field.

Select as many lines as you like at this point.
ViSP can only observe three lines at once, so in a following step we will select a subset of up to three lines.

\section{Use the Optimizer to Calculate ViSP Configurations}

Start the ViSP IPC, and press the button to start the ICO.
Enter the number of lines you have selected in the previous step at the top right and press enter.
The ICO GUI will re-build itself to allow you to input the wavelengths in the bottom panel.
Now enter the wavelengths.

If you have more lines than you are planning to observe at once (e.g., you have 4 lines of interest and are observing triplets, or you have 2 lines and are observing singlets, etc.) the ICO will try to optimize all possible combinations.
You can explore the configurations that the ICO found later in the IPC.

If you want to find doublets or singlets you have a choice of which arms to use.
Arm 1 has the shortest focal length camera lens, which implies the biggest field of view and bandwidth, but the lowest spatial and spectral resolution.
Arm 3 has the longest focal length camera lens, which implies the highest spatial and spectral resolution, but the smallest field of view and bandwidth.
At lower resolution more light falls onto a single pixel, so that the same signal-to-noise can be reached with less integration time, yielding faster cadence.

Once you are happy with the settings, press Optimize.
The ICO will take a bit of time to run.
Once it is done, all acceptable configurations are passed to the IPC and the first one is loaded.

The ICO uses a Monte-Carlo type search algorithm to find solutions.
Some amount of randomness should be expected when using it, e.g., re-running the optimizer or re-ordering the wavelengths may produce different results.

\section{Explore Configurations with the IPC}

In the top section of the IPC on the right you can load configurations from the ICO.
If there is more than one, you will have to look through them and decide which one you want.

Now we will refine the observing settings.
The first choice to make is between polarimetry or intensity-only.
In polarimetric mode the slit is stepped from one position to the next.
Integration times are generally long.
In intensity-only mode the slit moves continuously while the camera exposes.
There is a bit of blurring of the image as a result but with reasonable settings this can be kept to a minimum while still yielding very fast mapping.

Don't change the grating or tilt angle setting.
There is only one grating.
The tilt angle is calculated by the ICO.
If you change it, you will change the wavelengths that ViSP observes.

You will want to adjust the slit width, slit step, slit positions, integration time, and binning options to get the spatial and spectral resolution, map width, map time, and signal-to-noise that you want.
This can be done in any order, and likely will require iterations.
A good place to start is to select the slit width that you want.
The handy table below shows the result a change in a parameter has.

\begin{center}
	\begin{tabular}{>{\RaggedRight}p{1.25in}|>{\RaggedRight}p{.75in}>{\RaggedRight}p{.75in}>{\RaggedRight}p{.75in}>{\RaggedRight}p{.5in}>{\RaggedRight}p{.5in}>{\RaggedRight}p{.5in}}
	\hfill effect on&X Spatial&Y Spatial&Spectral&Map&Map&SNR\\
	of an increase in&Resolution&Resolution&Resolution&Width&Time&\\\hline
	Slit Width& reduce& 0& reduce& 0& 0& increase\\
	Slit Step& reduce& 0& 0& increase& 0& 0\\
	Slit Positions& 0& 0& 0& increase& increase& 0\\
	Integration Time& 0& 0& 0& 0& increase& increase\\
	Spectral Binning& 0& 0& reduce& 0& 0& increase\\
	Spatial Binning& 0& reduce& 0& 0& 0& increase\\
\end{tabular}
\end{center}

In general you will want to match the slit step size to the slit width in polarimetric mode, and you want to keep the frame rate as high as possible to minimize polarimetric cross-talk.
Correspondingly in intensity-only mode you will want to match the slit velocity divided by the frame rate to the slit width.

Have a look at the spectral windows in the arms.
Don't change the position angle.
It is calculated by the ICO.
If you change it, you will change the wavelengths that ViSP observes.
The spatial resolution shown here is the resolution along the slit, i.e., Y direction.

Have a look at the spectral resolution.
If it is higher than you need you can set binning in the spectral direction to reduce the resolution but increase the signal-to-noise.
For photospheric magnetic field diagnostics a spectral resolution of \num{120e3} is probably adequate.

If you've selected polarimetric mode and are using a very short integration time ViSP may spend a relatively substantial amount of time moving the slit.
Moving the slit to an adjacent position takes at most 200~ms.
The duty cycle number will tell you what percentage of the time ViSP is actually acquiring observations.

Iterate on these parameters to get what you want.
Unfortunately because of physics you can't have everything.

\section{Save the Configuration}

Once you get to a point where the configuration looks reasonable, you can save it so you can share it with collaborators or look at it again later.

\section{Notes, Tips, and Tricks}

Reducing the size of the camera readout causes a reduction in data rate but doesn't do anything else.

If you \emph{really} want one or two lines, start optimizing for those lines.
Then if you find a good configuration, try adding a second and/or third line to see if you can get them for free by going back to the optimizer and adding those lines.

If you want to observe two lines that are really close together try optimizing for a wavelength in the middle and see if the bandwidth is adequate to capture both at once. You can also nudge arms around a little bit (the IPC will impose hardware constraints) to move the spectral range that falls on the detector.

\end{document}

