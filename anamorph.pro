function anamorph, alpha, position
	; calculates anamorphic magnification
	return, cos(!dtor * alpha) / cos(!dtor * (position - alpha))
end
