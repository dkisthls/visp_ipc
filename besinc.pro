function besinc,r

  z=2.D*beselj(r,1,/double)

  dump=where(r,complement=zero)

;  print,zero

  if (zero ne -1) then begin
    z(zero)=1.D
    r(zero)=1.D
  endif

  return,z/r

end
