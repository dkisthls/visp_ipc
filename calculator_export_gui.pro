pro calculator_export_gui, ev
	; get the mode of this DSP - polarimetric (0) or intensity (1)
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; make the base widget, and store the calculator base widget id in the uvalue for later use
	export_base = widget_base(title='Export DSP', column=1, /modal, $
		/base_align_left, group_leader=ev.top, uvalue=ev.top)

	base = widget_base(export_base, /frame, column=1)
	widget = widget_label(base, value='Select Tasks:', /align_center)

	base = widget_base(base, column=2, /nonexclusive, /grid)
	tasks = ['Dark', 'Gain', 'Focus', 'Target', 'Align', 'WaveCal', 'PolCal', 'Observe']
	unames = strlowcase(tasks)

	for i=0,n_elements(tasks)-1 do begin
		widget = widget_button(base, value=tasks[i], uname=unames[i], $
			sensitive=(tasks[i] ne 'PolCal' or mode eq 0))
		widget_control, widget, set_button=(tasks[i] ne 'PolCal' or mode eq 0)
	endfor

	base = widget_base(export_base, column=1, /nonexclusive)
	widget = widget_button(base, value='Create IPs', sensitive=1, uname='ips')
	widget_control, widget, set_button=1

	base = widget_base(export_base, row=1)
	button = widget_button(base, value='Cancel', uname='button_cancel')
	button = widget_button(base, value='Export', uname='button_export', uvalue=tasks)

	widget_control, base, /realize
	xmanager, 'calculator_export_gui', export_base
end
