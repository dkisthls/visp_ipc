pro calculator_export_gui_event, ev
	uname = widget_info(ev.id, /uname)

	case 1 of
		strmatch(uname, 'button_cancel'): widget_control, ev.top, /destroy
		strmatch(uname, 'button_export'): begin
			; get tasks list
			widget_control, ev.id, get_uvalue=tasks
			; which tasks are checked?
			unames = strlowcase(tasks)
			set = bytarr(n_elements(tasks))
			for i=0, n_elements(tasks)-1 do $
				set[i] = widget_info(widget_info(ev.top, find_by_uname=unames[i]), /button_set)
			; are we exporting IPs?
			widget = widget_info(ev.top, find_by_uname='ips')
			export_ips = widget_info(widget, /button_set)
			; get the calculator base widget id, stored in the top-level base uvalue
			widget_control, ev.top, get_uvalue=calc_base
			; destroy the dialog
			widget_control, ev.top, /destroy
			; export DSPs
			w = where(set, n)
			if n gt 0 then calculator_gui_export_config, calc_base, tasks, w, export_ips
		end
		else: begin
;			print, 'widget ', uname, ' created an unhandled event.'
;			help, ev, /str
		end
	endcase
end
