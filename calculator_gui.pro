pro calculator_gui, advanced=adv, scalar=scalar, operator=operator
	if float(!version.release) lt 8.3 then begin
		print, 'The ViSP GUIPC requires at least IDL version 8.3, sorry.'
		print, 'Please update your IDL version or use the IDL run-time environment with the GUIPC IDL Save file.'
		return
	endif

	adv = keyword_set(adv)
	scalar = keyword_set(scalar)
	operator = keyword_set(operator)

	visp = get_visp_parameters(/force)

	calc_base = widget_base(column=1, title='ViSP IPC')
	;print, 'Calculator base widget id: ', calc_base
	conpar_base = widget_base(calc_base, row=1, xpad=0, ypad=0)
	arms_base = widget_base(calc_base, row=3, uname='arms_base', xpad=0, ypad=0)

	; configuration widgets
	base = widget_base(conpar_base, column=1, /frame, /base_align_right, uname='conpar_base')
	widget = widget_label(base, value='Configuration', /align_center)
	widget = visp_cw_field(base, /string, xsize=22, $
		title='Name:', value='-', uname='config_name')
	base2 = widget_base(base, row=1, xpad=0, ypad=0)
	widget = widget_label(base2, value='Mode:')
	widget = widget_combobox(base2, value=visp.modes, uname='mode', editable=0)
	widget_control, widget, set_combobox_select=0
	base2 = widget_base(base, row=1, xpad=0, ypad=0)
	widget = widget_label(base2, value='Grating:')
	widget = widget_combobox(base2, value=visp.gratings.name, uname='grating', editable=0)
	widget_control, widget, set_combobox_select=0
	widget = visp_cw_field(base, /float, xsize=8, /return_events, /focus_events, $
		title='Tilt Angle:', value=-visp.gratings[0].blaze, uname='alpha')
	alpha = widget ; used later to generate an update event
	base2 = widget_base(base, row=1, xpad=0, ypad=0)
	widget = widget_label(base2, value='Slit:')
	widget = widget_combobox(base2, value=visp.slit_names, uname='slit', editable=0)
	widget_control, widget, set_uvalue={widths:visp.slit_widths}
	widget_control, widget, set_combobox_select=1

	; map parameter widgets
	base = widget_base(conpar_base, column=1, /frame, /base_align_right)
	widget = widget_label(base, value='Map Setup', /align_center)
	base2 = widget_base(base, xpad=0, ypad=0)
	widget = visp_cw_field(base2, /float, xsize=9, /return_events, /focus_events, $
		title='      Slit Step ("):', value=0.1, uname='slit_step', $
		xoffset=0, yoffset=0)
	widget = visp_cw_field(base2, /float, xsize=9, /return_events, /focus_events, $
		title='Slit Velocity ("/s):', value=2.035, uname='slit_velocity', $
		xoffset=0, yoffset=0)
	widget_control, widget, map=0
	base2 = widget_base(base, xpad=0, ypad=0)
	widget = visp_cw_field(base2, /long, xsize=5, /return_events, /focus_events, $
		title='Slit Positions:', value=1000, uname='slit_positions', $
		xoffset=0, yoffset=0)
	widget = visp_cw_field(base2, /long, xsize=5, /return_events, /focus_events, $
		title='     Exposures:', value=1000, uname='exposures', $
		xoffset=0, yoffset=0)
	widget = widget_label(base, value='Map Width ("):   10.00', uname='map_width')
	widget = visp_cw_field(base, /long, xsize=5, /return_events, /focus_events, $
		title='Map Repeats:', value=1, uname='map_reps')
	widget = visp_cw_field(base, /float, xsize=9, /return_events, /focus_events, $
		title='Mu:', value=1d, uname='mu')

	; camera/modulator parameter widgets
	base = widget_base(conpar_base, column=1, /frame, /base_align_right)
	widget = widget_label(base, value='Camera & Modulator', /align_center)
	base2 = widget_base(base, xpad=0, ypad=0, /base_align_right)
	widget = visp_cw_field(base2, /float, xsize=9, /return_events, /focus_events, $
		title='Integration Time (s):', value=5., $
		uname='dwell_time', xoffset=0, yoffset=0)
	widget = visp_cw_field(base2, /float, xsize=9, /return_events, /focus_events, $
		title='     Frame Rate (Hz):', value=33, $
		uname='frame_rate', xoffset=0, yoffset=0)
	widget = visp_cw_field(base, /long, xsize=5, /return_events, /focus_events, $
		title='Modulation States:', value=10, uname='modulation_states')
	base2 = widget_base(base, xpad=0, ypad=0, /base_align_right)
	base3 = widget_base(base2, xpad=0, ypad=0, /base_align_right)
	widget = widget_label(base3, value='  Frame Rate (Hz):  0.00', uname='fps')
	base3 = widget_base(base2, xpad=0, ypad=0, /base_align_right)
	widget = widget_label(base3, value='Exposure Time (s): 0.020', uname='exposure_time')
	base2 = widget_base(base, xpad=0, ypad=0, /base_align_right)
	base3 = widget_base(base2, xpad=0, ypad=0, /base_align_right)
	widget = widget_label(base3, value='Motion During Exposure ("): 0.0407', uname='exposure_motion')
	base3 = widget_base(base2, xpad=0, ypad=0, /base_align_right)
	widget = widget_label(base3, value='       Modulation Rate (Hz):  0.00', uname='modulation_rate')
	base2 = widget_base(base, xpad=0, ypad=0, /base_align_right, column=1)
	widget = widget_label(base2, value='Number of Modulation Cycles:     0', $
		uname='modulation_cycles')
	widget = widget_label(base2, value='Modulation Efficiency:  0.00', uname='modulation_efficiency')

	; time and data rate
	base = widget_base(conpar_base, column=1, /frame, /base_align_right)
	widget = widget_label(base, value='Time & Data', /align_center)
	widget = widget_label(base, value='Map Time: 00:00:00', uname='map_time')
	widget = widget_label(base, value='Map Cadence: 00:00:00', uname='map_cadence')
	widget = widget_label(base, value='Total Time: 00:00:00', uname='total_time')
	widget = widget_label(base, value='Duty Cycle:   0%', uname='duty_cycle')
	widget = widget_label(base, value='Data Rate (XB/s):    0.00', uname='data_rate')
	widget = widget_label(base, value='Data Volume (XB):    0.00', uname='data_volume')

	; control buttons (set width to make them all the same)
	ww = 112
	base2 = widget_base(conpar_base, row=4)
	base3 = widget_base(base2, column=2)
	button = widget_button(base3, value='Update', uname='button_update', xsize=ww)
	button = widget_button(base3, value='Close', uname='button_close', xsize=ww)
	base4 = widget_base(base3, xsize=ww, /base_align_center, column=1)
	draw = widget_draw(base4, xsize=71, ysize=48)
	base3 = widget_base(base2, column=2)
	button = widget_button(base3, value='Load Config', uname='button_load', xsize=ww)
	button = widget_button(base3, value='Save Config', uname='button_save', xsize=ww)
	button = widget_button(base3, value='Optimizer', uname='button_optimizer', xsize=ww)
	widget_control, button, set_uvalue={opt_base:0l, advanced:adv, scalar:scalar}
	button = widget_button(base3, value='IP/DSP Export', uname='button_export', $
		sensitive=operator, xsize=ww)

	; combobox with optimized configurations
	widget = widget_label(base2, value='Load Optimizer Output:', /align_left)
	base = widget_base(base2, column=2, xpad=0, ypad=0)
	value = string('N/A',format='(a-24)') ; make it 21 characters long
	widget = widget_combobox(base, value=[value], uname='optimizer_config', /align_right, $
		sensitive=0, editable=0)
;	base = widget_base(base2, column=1, xpad=0, ypad=0)
	button = widget_button(base, value=' Load ', uname='button_load_optimizer', $
		sensitive=0)

	calculator_gui_arm, arms_base, 1, advanced=adv, scalar=scalar
	calculator_gui_arm, arms_base, 2, advanced=adv, scalar=scalar
	calculator_gui_arm, arms_base, 3, advanced=adv, scalar=scalar

	; realize the widget and start the manager
	widget_control, calc_base, /realize
	xmanager, 'calculator_gui', calc_base, /no_block

	; have to load the image after realizing the draw window
	widget_control, draw, get_value=index  
	wset, index
	tv, read_bmp('ViSP_small.bmp')

	; load the default configuration
	ev = {id:alpha, top:calc_base, handler:calc_base}
	calculator_gui_load_config, ev, file='ViSP_Configuration_Default.ini'
end
