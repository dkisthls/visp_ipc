pro calculator_gui_arm, arms_base, arm, advanced=adv, scalar=scalar
	adv = keyword_set(adv)
	scalar = keyword_set(scalar)

	visp = get_visp_parameters()
	camera = get_camera_parameters()
	td = get_transmission_data()

	name = string('arm', arm, format='(a,i1)')

	; arm widgets
	arm_base = widget_base(arms_base, column=2, /frame, uname=name)
	widget_control, arm_base, set_uvalue={scalar:scalar}

	leg_base = widget_base(arm_base, row=2, xpad=0, ypad=0)

	base = widget_base(leg_base, row=1, ypad=0)
	widget = widget_label(base, value='Arm '+string(arm, format='(i1)'))
	base2 = widget_base(base, row=1, /nonexclusive, xpad=8)
	widget = widget_button(base2, value='Enable', sensitive=1, uname=name+'_use')
	widget_control, widget, set_button=1

	widget = widget_label(base, value='Wavelength (nm):  630.200', uname=name+'_lambda')

	base = widget_base(leg_base, column=1, /align_center, xpad=0, ypad=0)
	widget = widget_label(base, value='Message here.', uname=name+'_message', /dynamic_resize)

	base2 = widget_base(leg_base, column=1, /base_align_right, /frame, $
		uname=name+'_input_base')
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = visp_cw_field(base3, /float, xsize=9, /return_events, /focus_events, $
		title='Position (deg):', value=-10d, uname=name+'_position')
	widget = visp_cw_field(base3, /long, xsize=2, /return_events, /focus_events, $
		title='Order:', value=10, uname=name+'_order')
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = widget_label(base3, value='Binning:')
	widget = widget_combobox(base3, value=['1','2','4','8'], $
		uname=name+'_spectral_binning', editable=0, xsize=40)
	widget = widget_label(base3, value='Spectral x')
	widget = widget_combobox(base3, value=['1','2','4','8'], $
		uname=name+'_spatial_binning', editable=0, xsize=40)
	widget = widget_label(base3, value='Spatial')
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = visp_cw_field(base3, /long, xsize=4, /return_events, /focus_events, $
		title='Spectral ROI Length:', value=camera.spectral_pixels, $
		uname=name+'_spectral_roi_length')
	widget = visp_cw_field(base3, /long, xsize=4, /return_events, /focus_events, $
		title='Start:', value=0, uname=name+'_spectral_roi_start')
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = visp_cw_field(base3, /long, xsize=4, /return_events, /focus_events, $
		title='Spatial ROI Length:', value=camera.spatial_pixels, $
		uname=name+'_spatial_roi_length')
	widget = visp_cw_field(base3, /long, xsize=4, /return_events, /focus_events, $
		title='Start:', value=0, uname=name+'_spatial_roi_start')
	widget_control, widget, sensitive=0
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = widget_label(base3, value='Order-sorting Filter')
	widget = widget_combobox(base3, value=['Automatic',td.nFilter], uname=name+'_filter_select', $
		editable=0, sensitive=adv)
	if not adv then widget_control, widget, map=0
	base3 = widget_base(base2, row=1, xpad=0, ypad=0)
	widget = widget_label(base3, value='Coherent Illumination?')
	widget = widget_combobox(base3, value=['No','Yes','Automatic'], uname=name+'_coherence', $
		editable=0, sensitive=adv)
	if not adv then widget_control, widget, map=0
	widget_control, widget, set_combobox_select=2

	base2 = widget_base(leg_base, column=2, /frame, uname=name+'_output_base')

	base3 = widget_base(base2, column=1, /base_align_right)
	widget = widget_label(base3, value='Vignetting and Diffraction: 0.00000', uname=name+'_vignetting_diffraction')
	widget = widget_label(base3, value=               'Detector QE: 0.00000', uname=name+'_qe')
	widget = widget_label(base3, value=         'Camera Duty Cycle: 0.00000', uname=name+'_duty_cycle')
	widget = widget_label(base3, value=                        'P   |   S  ', uname=name+'_te_tm')
	widget = widget_label(base3, value=  'Grating Efficiency: 0.000 | 0.000', uname=name+'_grating_eff')
	widget = widget_label(base3, value=        'Transmission: 0.000 | 0.000', uname=name+'_transmission')
	widget = widget_label(base3, value=   'System Efficiency: 0.000 | 0.000', uname=name+'_system_eff')
	widget = widget_label(base3, value=       'Ic SNR/Px/Int:  0000 |  0000', uname=name+'_snr')
	widget = widget_label(base3, value=  'Beam-combined Ic SNR/Px/Int: 0000', uname=name+'_bc_snr')

	base3 = widget_base(base2, column=1, /base_align_right)
	widget = widget_label(base3, value='Spatial FWHM ("): 0.00000', uname=name+'_spatial_fwhm')
	widget = widget_label(base3, value='Spatial Sampling: 0.00000', $
		uname=name+'_spatial_sampling')
	widget = widget_label(base3, value='Spatial Resolution ("): 0.00000', $
		uname=name+'_spatial_resolution')
	widget = widget_label(base3, value='FOV Height ("): 000.000', uname=name+'_FOV_height')
	widget = widget_label(base3, value='Spectral FWHM (pm): 0.00000', uname=name+'_spectral_fwhm')
	widget = widget_label(base3, value='Spectral Sampling: 0.00000', $
		uname=name+'_spectral_sampling')
	widget = widget_label(base3, value='Spectral Resolution: 000000.', $
		uname=name+'_spectral_resolution')
	widget = widget_label(base3, value='Bandwidth (nm): 0.00000', uname=name+'_bandwidth')
	widget = widget_label(base3, value='Order-sorting Filter: AVRO-XXXX', uname=name+'_filter')

	base = widget_base(arm_base, column=1, xpad=0, ypad=0)
	;widget = widget_draw(base, retain=2, xsize=341, ysize=205, uname=name+'_spectrum')
	widget = widget_window(base, xsize=341, ysize=225, uname=name+'_spectrum')
end
