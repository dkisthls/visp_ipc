pro calculator_gui_check_arm_positions, ev
	visp = get_visp_parameters()

	; check all arm positions and attempt to adjust disabled arms if necessary

	; get all arm positions
	positions = fltarr(3)
	enabled = bytarr(3)
	arms = 'arm' + ['1', '2', '3']
	for i=0,2 do begin
		widget = widget_info(ev.top, find_by_uname=arms[i]+'_position')
		widget_control, widget, get_value=position
		positions[i] = position
		widget = widget_info(ev.top, find_by_uname=arms[i]+'_use')
		enabled[i] = widget_info(widget, /button_set)
	endfor

	; now check for interference with other arms
	sep = positions[0:1] - positions[1:2] ; this way around because position < 0
	interference = (visp.min_separation - sep) gt 10. * (machar()).eps ; handle float precision

	if not array_equal(interference, [0,0]) then begin ; there is interference, try to do something
		; check if arms are disabled and move them if that would help
		case 1 of
			array_equal(enabled, [1,1,1]): ; all arms are enabled, no help
			array_equal(enabled, [1,1,0]): begin ; arm 3 is disabled
				; in case arms 2 and 3 interfere, move arm 3 as much as we need or can
				if interference[1] eq 1 then $
					positions[2] = (positions[1] - visp.min_separation[1]) > visp.min_cam_angle
			end
			array_equal(enabled, [1,0,1]): begin ; arm 2 is disabled
				; move arm 2 if it can fit between 1 and 3
				if positions[0] - positions[2] ge total(visp.min_separation) then $
					if interference[0] eq 1 then begin
						positions[1] = positions[0] - visp.min_separation[0] 
					endif else begin
						positions[1] = positions[2] + visp.min_separation[1] 
					endelse
			end
			array_equal(enabled, [0,1,1]): begin ; arm 1 is disabled
				; in case arms 1 and 2 interfere, move arm 1 as much as we need or can
				if interference[0] eq 1 then $
					positions[0] = (positions[1] + visp.min_separation[0]) < visp.max_cam_angle
			end
			array_equal(enabled, [1,0,0]): begin ; only arm 1 is enabled, can move 2 and 3
				; move arm 2 if we need to
				if interference[0] eq 1 then $
					positions[1] = (positions[0] - visp.min_separation[0]) > $
						(visp.min_cam_angle + visp.min_separation[1])
				; move arm 3 if we need to - possibly as a result of moving arm 2
				if interference[1] eq 1 or $
					positions[1] - positions[2] lt visp.min_separation[1] then $
					positions[2] = (positions[1] - visp.min_separation[1]) > $
						visp.min_cam_angle
			end
			array_equal(enabled, [0,1,0]): begin ; only arm 2 is enabled, can move 1 and 3
				; move arm 1 if we can
				if interference[0] eq 1 then $
					positions[0] = (positions[1] + visp.min_separation[0]) < visp.max_cam_angle
				; move arm 3 if we can
				if interference[1] eq 1 then $
					positions[2] = (positions[1] - visp.min_separation[1]) > visp.min_cam_angle
			end
			array_equal(enabled, [0,0,1]): begin ; only arm 3 is enabled, can move 1 and 2
				; move arm 2 if we need to
				if interference[1] eq 1 then $
					positions[1] = positions[2] + visp.min_separation[1] < $
						(visp.max_cam_angle - visp.min_separation[0])
				; move arm 1 if we need to - possibly as a result of moving arm 2
				if interference[0] eq 1 or $
					positions[0] - positions[1] lt visp.min_separation[0] then $
					positions[0] = (positions[1] + visp.min_separation[0]) > $
						visp.max_cam_angle
			end
		endcase

		; update the arm positions
		; only disabled arms will have been updated, so no need to refresh
		for i=0,2 do begin
			widget = widget_info(ev.top, find_by_uname=arms[i]+'_position')
			widget_control, widget, set_value=positions[i]
		endfor
	endif
end
