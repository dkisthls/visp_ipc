pro calculator_gui_enable_arm, ev
	; enable or disable an arm

	; figure out which arm we need to update
	uname = widget_info(ev.id, /uname)
	name = strmid(uname, 0, 4)

	widget = widget_info(ev.top, find_by_uname=name+'_input_base')
	widget_control, widget, sensitive=ev.select

	widget = widget_info(ev.top, find_by_uname=name+'_output_base')
	widget_control, widget, sensitive=ev.select

	if ev.select eq 0 then begin
		; wipe the wavelength and plot
		widget = widget_info(ev.top, find_by_uname=name+'_lambda')
		widget_control, widget, set_value='Wavelength (nm):         '
		widget = widget_info(ev.top, find_by_uname=name+'_message')
		widget_control, widget, set_value='Arm is disabled and parked.'
		widget = widget_info(ev.top, find_by_uname=name+'_spectrum')
		widget_control, widget, get_value=wid
		wid.SetCurrent
		wid.Erase
		;wset, wid
		;erase
		;wset, -1
		; check for interference and update arm position if required
		calculator_gui_check_arm_positions, ev
	endif
end
