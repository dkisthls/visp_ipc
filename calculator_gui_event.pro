pro calculator_gui_event, ev
	visp = get_visp_parameters()
	camera = get_camera_parameters()

	; if it is a CW_FIELD widget, check if it has been updated
	if strmatch(widget_info(ev.id, /event_func), 'CW_FIELD_EVENT') then begin
		; old value is stored in the uvalue
		widget_control, ev.id, get_uvalue=old_value
		; return if not updated, otherwise update uvalue
		if old_value eq ev.value then return else $
			widget_control, ev.id, set_uvalue=ev.value
	endif

	; get the widget uname
	uname = widget_info(ev.id, /uname)

	; save the input value
	if tag_exist(ev, 'value') then value_in = ev.value
	msg = ''
	constrained = 0b
	if (!d.name eq 'WIN') then newline = string([13b, 10b]) else newline = string(10b)

	case 1 of
		strmatch(uname, 'mode'): begin
			; set the whole widget insensitive and disable updates while we work
			widget_control, ev.top, sensitive=0
			widget_control, ev.top, update=0 ; this doesn't seem to do much?
			; combobox, no need to sanitize
			calculator_gui_switch_mode, ev
			; update map and modulator
			calculator_gui_update_map, ev
			calculator_gui_update_modulator, ev
			; update the rest
			calculator_gui_update_time, ev
			calculator_gui_update_data, ev
			calculator_gui_update_arm, ev
			; and make the top level widget sensitive again
			widget_control, ev.top, sensitive=1
			widget_control, ev.top, update=1
		end
		strmatch(uname, 'slit_step'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='slit_positions')
			widget_control, widget, get_value=positions
			ev.value = ev.value > 0. < visp.fov_x / (positions-1) ; not negative and map size smaller than the FOV
			msg += newline + 'Slit step size was constrained to not exceed the maximum FOV.'
			widget_control, ev.id, set_value=ev.value
			; update the map width, time estimates, and data estimates
			calculator_gui_update_map, ev
			calculator_gui_update_time, ev
		end
		strmatch(uname, 'slit_velocity'): begin
			; sanitize data
			ev.value = ev.value > 0. < visp.slit_move_speed ; not negative and less than the max slit speed
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Slit velocity must be between 0 and ' + $
					strcompress(string(visp.slit_move_speed), /remove_all) + '.'
			endif
			; also low enough to not exceed the max FOV
			widget = widget_info(ev.top, find_by_uname='exposures')
			widget_control, widget, get_value=exposures
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate
			fps = fps(1./framerate, 1)
			ev.value = ev.value < visp.fov_x / exposures * fps.fps
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Slit velocity was constrained to not exceed the maximum FOV.'
			endif
			widget_control, ev.id, set_value=ev.value
			; update the map width, time estimates, and data estimates
			calculator_gui_update_map, ev
			calculator_gui_update_modulator, ev
			calculator_gui_update_time, ev
		end
		strmatch(uname, 'slit_positions'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='slit_step')
			widget_control, widget, get_value=step
			; at least one position in the map but not so many as to exceed the max FOV
			ev.value = ev.value < long(ceil(visp.fov_x / step)) > 1
			msg += newline + 'Slit positions was constrained to not exceed the maximum FOV.'
			widget_control, ev.id, set_value=ev.value
			; update the map width, time estimates, and data estimates
			calculator_gui_update_map, ev
			calculator_gui_update_time, ev
			calculator_gui_update_data, ev
		end
		strmatch(uname, 'exposures'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='slit_velocity')
			widget_control, widget, get_value=velocity
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate
			fps = fps(1./framerate, 1)
			; at least one exposure in the map but not so many as to exceed the max FOV
			ev.value = ev.value < long(visp.fov_x / velocity * fps.fps) > 1
			msg += newline + 'Exposures was constrained to not exceed the maximum FOV.'
			widget_control, ev.id, set_value=ev.value
			; update the map width, time estimates, and data estimates
			calculator_gui_update_map, ev
			calculator_gui_update_time, ev
			calculator_gui_update_data, ev
		end
		strmatch(uname, 'map_reps'): begin
			; sanitize data
			ev.value = ev.value > 1 ; at least one repetition
			msg += newline + 'Number of map repeats must be at least 1.'
			widget_control, ev.id, set_value=ev.value
			; update the time estimates and data estimates
			calculator_gui_update_time, ev
			calculator_gui_update_data, ev
		end
		strmatch(uname, 'mu'): begin
			; sanitize data
			ev.value = ev.value > 0d < 1d ; between 0 and 1
			msg += newline + 'Mu must be between 0 and 1 inclusive.'
			widget_control, ev.id, set_value=ev.value
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'frame_rate'): begin
			; sanitize data
			; must be high enough to not exceed the max FOV
			widget = widget_info(ev.top, find_by_uname='slit_velocity')
			widget_control, widget, get_value=velocity
			widget = widget_info(ev.top, find_by_uname='exposures')
			widget_control, widget, get_value=exposures

			ev.value = ev.value > velocity * exposures / visp.fov_x < camera.max_fps
			if value_in gt ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Frame rate was constrained to not exceeed the maximum frame rate.'
			endif else if value_in lt ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Frame rate was constrained to not exceed the maximum FOV.'
			endif
			widget_control, ev.id, set_value=ev.value
			; update the map
			calculator_gui_update_map, ev
			; update the time and data estimates
			calculator_gui_update_time, ev
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'dwell_time'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='modulation_states')
			widget_control, widget, get_value=states
			; minimum dwell based on maximum modulator rotation speed or camera FPS
			ev.value = ev.value > 60d / visp.max_modulator_speed / 2d
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Integration time was constrained to not exceed the maximum modulator rotation rate.'
			endif
		   	ev.value = ev.value > states / camera.max_fps
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Integration time was constrained to not exceed the maximum camera frame rate.'
			endif
			widget_control, ev.id, set_value=ev.value
			; update the time and data estimates
			calculator_gui_update_time, ev
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'modulation_states'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='dwell_time')
			widget_control, widget, get_value=dwell
			ev.value = ev.value > visp.min_modulation_states
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Must have at least ' + $
					strcompress(string(visp.min_modulation_states), /remove_all) + $
					' modulation states.'
			endif
			ev.value = ev.value < fix(dwell * camera.max_fps)
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Number of modulation states was constrained to not exceed the maximum camera frame rate.'
			endif
			widget_control, ev.id, set_value=ev.value
			; update the modulator and data estimates
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'grating'): begin
			; sanitize alpha
			widget = widget_info(ev.top, find_by_uname='grating') ; grating
			grating_text = widget_info(widget, /combobox_gettext)
			widget_control, widget, get_value=list
			grating = (where(list eq grating_text))[0]
			widget = widget_info(ev.top, find_by_uname='alpha') ; tilt angle
			widget_control, widget, get_value=alpha
			alpha = alpha > visp.gratings[grating].min_tilt_angle $
				< visp.gratings[grating].max_tilt_angle
			widget_control, widget, set_value=alpha
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'alpha'): begin
			; sanitize data
			widget = widget_info(ev.top, find_by_uname='grating') ; grating
			grating_text = widget_info(widget, /combobox_gettext)
			widget_control, widget, get_value=list
			grating = (where(list eq grating_text))[0]
			ev.value = ev.value > visp.gratings[grating].min_tilt_angle $
				< visp.gratings[grating].max_tilt_angle
			msg += newline + 'Tilt angle must be between ' + $
				strcompress(string(visp.gratings[grating].min_tilt_angle), /remove_all) + $
				' and ' + $
				strcompress(string(visp.gratings[grating].max_tilt_angle), /remove_all) + '.'
			widget_control, ev.id, set_value=ev.value
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'slit'): begin
			; combobox, no need to sanitize
			; update the arms
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'arm?_use'): begin
			calculator_gui_enable_arm, ev
			; update arms, re-calculate modulator and data rates
			calculator_gui_update_arm, ev
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
		end
		strmatch(uname, 'arm?_position'): begin
			; sanitize data
			ev.value = ev.value > visp.min_cam_angle < visp.max_cam_angle
			msg += newline + 'Position must be between ' + $
				strcompress(string(visp.min_cam_angle), /remove_all) + ' and ' + $
				strcompress(string(visp.max_cam_angle), /remove_all) + '.'
			widget_control, ev.id, set_value=ev.value
			; update the arm
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'arm?_order'): begin
			; sanitize data
			ev.value = ev.value > 1
			msg += newline + 'Order must be greater or equal to 1.'
			widget_control, ev.id, set_value=ev.value
			; update the arm
			calculator_gui_update_arm, ev
		end
		stregex(uname, 'arm._[a-z]+_binning', /boolean): begin
			value = long(ev.str)
			; check that the window length is divisible and update it if not
			len_uname = strmid(uname, 0, strpos(uname, 'binning')) + 'roi_length'
			widget = widget_info(ev.top, find_by_uname=len_uname)
			widget_control, widget, get_value=length
			newlength = (length / value) * value
			if length ne newlength then begin
				constrained = 1b
				if strmatch(uname, '*spectral*') then type = 'Spectral' else type = 'Spatial'
				msg += newline + 'ROI length was constrained to a multiple of the binning.'
				widget_control, widget, set_value=newlength
			endif
			; update the modulator and data estimates
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; update the arm
			calculator_gui_update_arm, ev
		end
		stregex(uname, 'arm._[a-z]+_roi_start', /boolean): begin
			; sanitize data
			len_uname = strmid(uname, 0, strpos(uname, 'start')) + 'length'
			widget = widget_info(ev.top, find_by_uname=len_uname)
			widget_control, widget, get_value=length
			if strmatch(uname, '*spectral*') then begin
				type = 'Spectral'
				max = camera.spectral_pixels - length
			endif else begin
				type = 'Spatial'
				max = camera.spatial_pixels - length
			endelse
			ev.value = ev.value > 0 < max
			msg += newline + type + ' ROI start position must be between 0 and ' + $
				strcompress(string(max), /remove_all) + '.'
			widget_control, ev.id, set_value=ev.value
			; update the arm
			calculator_gui_update_arm, ev
		end
		stregex(uname, 'arm._[a-z]+_roi_length', /boolean): begin
			; sanitize data - don't exceed max length
			if strmatch(uname, '*spectral*') then begin
				start_uname = strmid(uname, 0, strpos(uname, 'length')) + 'start'
				widget = widget_info(ev.top, find_by_uname=start_uname)
				widget_control, widget, get_value=start
				type = 'Spectral'
				max = camera.spectral_pixels - start
			endif else begin
				type = 'Spatial'
				max = camera.spatial_pixels
			endelse
			ev.value = ev.value > 0 < max
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + type + ' ROI length must be between 0 and ' + $
					strcompress(string(max), /remove_all) + '.'
			endif
			; sanitize data - ensure window is divisible by the binning
			bin_uname = strmid(uname, 0, strpos(uname, 'roi_length')) + 'binning'
			widget = widget_info(ev.top, find_by_uname=bin_uname)
			binning = long(widget_info(widget, /combobox_gettext))
			ev.value = (ev.value / binning) * binning
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + type + ' ROI length was constrained to a multiple of the binning.'
			endif
			; update the value if required
			widget_control, ev.id, set_value=ev.value
			; update the modulator and data estimates
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; update the arm
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'arm?_filter_select'): begin
			; update the arm
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'arm?_coherence'): begin
			; update the arm
			calculator_gui_update_arm, ev
		end
		strmatch(uname, 'button_update'): if ev.select then begin
			; update everything
			calculator_gui_update_map, ev
			calculator_gui_update_time, ev
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			calculator_gui_update_arm, ev
		endif
		strmatch(uname, 'button_optimizer'): if ev.select then begin
			widget_control, ev.id, get_uvalue=uvalue
			opt_base = uvalue.opt_base
			optimizer_gui, calc_base=ev.top, opt_base=opt_base, advanced=uvalue.advanced
			widget_control, ev.id, set_uvalue={opt_base:opt_base, advanced:uvalue.advanced}
		endif
		strmatch(uname, 'button_save'): calculator_gui_save_config, ev
		strmatch(uname, 'button_export'): if ev.select then begin
			calculator_export_gui, ev
		endif
		strmatch(uname, 'button_load'): calculator_gui_load_config, ev
		strmatch(uname, 'button_close'): widget_control, ev.top, /destroy
		strmatch(uname, 'optimizer_config'):
		strmatch(uname, 'button_load_optimizer'): begin
			; set the whole widget insensitive and disable updates while we work
			widget_control, ev.top, sensitive=0
			widget_control, ev.top, update=0 ; this doesn't seem to do much?
			; find the combobox widget
			combobox = widget_info(ev.top, find_by_uname='optimizer_config')
			; find the currently selected configuration
			combo = widget_info(combobox, /combobox_gettext)
			widget_control, combobox, get_value=list
			index = (where(list eq combo))[0]
			; load the configuration and then update everything
			widget_control, combobox, get_uvalue=results
			; name, but prepend an index number
			widget = widget_info(ev.top, find_by_uname='config_name')
			widget_control, widget, set_value=results[index].name
			; grating
			widget = widget_info(ev.top, find_by_uname='grating')
			widget_control, widget, set_combobox_select=results[index].grating
			; tilt
			widget = widget_info(ev.top, find_by_uname='alpha')
			widget_control, widget, set_value=results[index].tilt
			; arms
			arms = ['arm1', 'arm2', 'arm3']
			parm = results[index].arm
			for i=0,2 do begin
				arm = arms[parm[i]]
				; enabled
				widget = widget_info(ev.top, find_by_uname=arm + '_use')
				enabled = results[index].enabled[i] ne 0
				widget_control, widget, set_button=enabled
				if enabled eq 1 then begin
				   	; position
					widget = widget_info(ev.top, find_by_uname=arm + '_position')
					widget_control, widget, set_value=results[index].position[i]
					; order
					widget = widget_info(ev.top, find_by_uname=arm + '_order')
					widget_control, widget, set_value=results[index].order[i]
				endif
				; set the enabled state right
				calculator_gui_enable_arm, {id:widget, top:ev.top, select:enabled}
			endfor
			; update arms
			calculator_gui_update_arm, ev
			; re-calculate modulator and data rates
			calculator_gui_update_modulator, ev
			calculator_gui_update_data, ev
			; and make the top level widget sensitive again
			widget_control, ev.top, sensitive=1
			widget_control, ev.top, update=1
		end
		else: begin
			print, 'widget ', uname, ' created an unhandled event.'
			help, ev, /str
		end
	endcase

	if tag_exist(ev, 'value') then if ev.value ne value_in then constrained = 1b
	if constrained ne 0 then $
		tmp = dialog_message('Inputs were constrained.' + newline + msg, dialog_parent=ev.top)
end

