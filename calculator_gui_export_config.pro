pro calculator_gui_export_config, calc_base, tasks, which, export_ips
	if n_elements(tasks) eq 0 then return ; nothing to do
	
	camera = get_camera_parameters()
	visp = get_visp_parameters()
	visp_fq = 'atst.ics.visp'

	; export the current configuration to a file that can be imported by
	; the IP creator tool

	; arm enumeration
	cs = ['1', '2', '3']

	; get the configuration name
	widget = widget_info(calc_base, find_by_uname='config_name')
	widget_control, widget, get_value=name

	; find out what mode we're operating in
	widget = widget_info(calc_base, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]
	; map repeats
	widget = widget_info(calc_base, find_by_uname='map_reps')
	widget_control, widget, get_value=repeats
	if repeats eq 1 then r = ' repeat' else r = ' repeats'
	; get some parameters we'll need and build the description
	arcsec = '"'
	if mode eq 0 then begin
		; polarimetry
		; modulation states and cycles
		widget = widget_info(calc_base, find_by_uname='dwell_time')
		widget_control, widget, get_value=dwell
		widget = widget_info(calc_base, find_by_uname='modulation_states')
		widget_control, widget, get_value=states
		fps = fps(dwell, states)
		; map step size, slit positions, and start and end
		widget = widget_info(calc_base, find_by_uname='slit_step')
		widget_control, widget, get_value=step
		widget = widget_info(calc_base, find_by_uname='slit_positions')
		widget_control, widget, get_value=positions
		width = step * positions
		description = strcompress(string(name, width, arcsec, dwell, repeats, r, $
			format='(a,", polarimetric, ",f8.2,a,", ",f8.2," s, ",i6,a)'))
	endif else begin
		; intensity
		; map scan velocity, and start and end
		widget = widget_info(calc_base, find_by_uname='slit_velocity')
		widget_control, widget, get_value=velocity
		widget = widget_info(calc_base, find_by_uname='exposures')
		widget_control, widget, get_value=exposures
		widget = widget_info(calc_base, find_by_uname='frame_rate')
		widget_control, widget, get_value=framerate
		fps = fps(1./framerate, 1)
		width = velocity / fps.fps * exposures
		description = strcompress(string(name, width, arcsec, velocity, arcsec, repeats, r, $
			format='(a,", intensity, ",f8.2,a,", ",f8.2,a,"/s, ",i6,a)'))
	endelse


	; first create the main section that contains the spectrograph setup
	out_setup = []
	; also create an array to hold focus-specific settings
	out_focus = visp_fq + ['.focusSteps,11']
	; grating position
	widget = widget_info(calc_base, find_by_uname='alpha') ; tilt angle
	widget_control, widget, get_value=alpha
	out_setup = [out_setup, visp_fq + '.gratingpos,' + string(alpha)]
	; grating
	widget = widget_info(calc_base, find_by_uname='grating') ; grating
	grating_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	grating = (where(list eq grating_text))[0]
	out_setup = [out_setup, visp_fq + '.gratingName,"' + string(grating_text) + '"']
	; slit
	widget = widget_info(calc_base, find_by_uname='slit')
	slit = widget_info(widget, /combobox_gettext)
	out_setup = [out_setup, visp_fq + '.slitWidth,' + string(strmid(slit,0,strlen(slit)-1))]
	; calculate the slit velocity to match slit width at max fps
	widget_control, widget, get_value=slit_list
	slit_velocity = visp.slit_widths[(where(slit_list eq slit))[0]] * visp.plate_scale * camera.max_fps
	; loop over the arms for enabled state, position, exposure time, and wavelength
	for i=0,2 do begin
		c = cs[i]
		arm = 'arm' + c
		; enabled state
		widget = widget_info(calc_base, find_by_uname=arm + '_use')
		enabled = widget_info(widget, /button_set) ? 'true' : 'false'
		out_setup = [out_setup, visp_fq + '.camera' + c + 'enabled,' + enabled]
		; arm position
		widget = widget_info(calc_base, find_by_uname=arm + '_position')
		widget_control, widget, get_value=position
		out_setup = [out_setup, visp_fq + '.arm' + c + 'pos,' + string(position)]
		; exposure time
		widget = widget_info(calc_base, find_by_uname=arm+'_duty_cycle')
		widget_control, widget, get_value=duty_cycle_string
		reads, (strsplit(duty_cycle_string,':',/extract))[1], duty_cycle
		exptime = duty_cycle / fps.fps * 1e3 ; in ms
		out_setup = [out_setup, visp_fq + '.vcc' + c + 'expTime,' + string(exptime)]
		; calculate wavelength using the grating equation
		widget = widget_info(calc_base, find_by_uname=arm + '_order')
		widget_control, widget, get_value=order
		lambda = 1d / order * visp.gratings[grating].density * $
			(sin(!dtor * (position - alpha)) - sin(!dtor * alpha))
		out_setup = [out_setup, visp_fq + '.wavelength' + c + ',' + string(lambda*1d9)]
		; calculate focus start position and step size
		; depth of focus is +-2 * lambda * N^2, by default we make 11 steps of lambda * N^2
		ap = get_arm_parameters(lambda, i)
		; round the step size to the nearest um
		fss = round(lambda * (ap.focal_length / ap.lens_width)^2d * 1d6)
		out_focus = [out_focus, visp_fq + '.focus' + c + 'begPos,0',$
			visp_fq + '.focus' + c + 'stepSz,' + string(fss)]
	endfor
	out_setup = strcompress(out_setup,/remove_all)


	; now create the section for the map
	; create a couple of sections for different situations
	; focus does not create a map, create a minimal one, but add focus parameters
	out_map_focus = visp_fq + ['.numModStates,1', '.numModCycles,1', '.mapRepeats,1', $
		'.mapStart,0']
	; align scans a small area around 0 to find the pinhole in intensity mode
	; calculate the slit scan velocity that corresponds to 1 slit with per 1/FPS
	out_map_align = visp_fq + ['.slitVelocity,' + string(slit_velocity), $
		'.mapStart,-10', '.mapEnd,10', '.mapRepeats,1']
	; target scans +-10" or the desired FOV, whichever is bigger, in intensity mode
	out_map_target = visp_fq + ['.slitVelocity,' + string(slit_velocity), $
		'.mapStart,' + string(-1*width/2 < (-10)), $
		'.mapEnd,' + string(width/2 > 10), $
		'.mapRepeats,1']
	; wavecal is currently equal to align
	out_map_wavecal = out_map_align
	; polcal, dark, gain, and observe are different for polarimetric and intensity mode
	out_map_darkgain = []
	out_map_observe = []
	if mode eq 0 then begin
		; polarimetry
		; polcal is a polarimetric single observation at 0
		out_map_polcal = visp_fq + ['.numModStates,' + string(states), $
			'.numModCycles,' + string(fps.cycles), $
			'.slitStepSz,0', '.slitSteps,1', '.mapStart,0', '.mapRepeats,1']
		; dark and gain are sit-and-stare at 0 in polarimetric mode
		out_map_darkgain = visp_fq + ['.numModStates,' + string(states), $
			'.numModCycles,' + string(fps.cycles), $
			'.slitStepSz,0', $
			'.slitSteps,25', $
			'.mapStart,0', $
			'.mapRepeats,1']
		; observe scans the desired FOV
		out_map_observe = visp_fq + ['.numModStates,' + string(states), $
			'.numModCycles,' + string(fps.cycles), $
			'.slitStepSz,' + string(step), $
			'.slitSteps,' + string(positions), $
			'.mapStart,' + string(-1*width/2), $
			'.mapRepeats,' + string(repeats)]
	endif else begin
		; intensity
		; polcal doesn't exist
		out_map_polcal = []
		; dark and gain are equal to align in intensity mode
		out_map_darkgain = out_map_align
		; slit velocity and exposure time for intensity
		out_map_observe = visp_fq + ['.slitVelocity,' + string(velocity), $
			'.mapStart,' + string(-1*width/2), $
			'.mapEnd,' + string(width/2), $
			'.mapRepeats,' + string(repeats)]
	endelse


	; create the output for the ROIs
	out_full_rois = []
	out_small_rois = []
	; parameters to get
	names_combo = ['spectral_binning', 'spatial_binning']
	names_entry = ['spectral_roi_start', 'spectral_roi_length', $
		'spatial_roi_length']
	values = intarr(5)
	for i=0,2 do begin
		; add a blank line between cameras
		if i ne 0 then begin
		   	out_full_rois = [out_full_rois, '']
		   	out_small_rois = [out_small_rois, '']
		endif

		c = cs[i]
		arm = 'arm' + c

		; binning
		for j=0,n_elements(names_combo)-1 do begin
			widget = widget_info(calc_base, find_by_uname=arm + '_' + names_combo[j])
			values[j] = widget_info(widget, /combobox_gettext)
		endfor

		; nominal ROIs
		for j=0,n_elements(names_entry)-1 do begin
			widget = widget_info(calc_base, find_by_uname=arm + '_' + names_entry[j])
			widget_control, widget, get_value=value
			values[j+n_elements(names_combo)] = value
		endfor

		; small ROIs in the spectral dimension for focus
		small_values = values
		small_values[2] = values[2] + values[3]/2 - 5
		small_values[3] = 10

		; construct output lines
		out_full_rois = [out_full_rois, visp_fq + '.vcc' + c + '_hw_binSize,1\,1', $
			visp_fq + '.vcc' + c + '_hw_win_numberOfROIs,2', $
			visp_fq + '.vcc' + c + '_hw_win_ROI_1,' + $
			string((camera.sensor_pixels[0] - values[4]) / 2) + $
			'\,' + string(values[2]) + $
			'\,' + string(values[4]) + $
			'\,' + string(values[3]), $
			visp_fq + '.vcc' + c + '_hw_win_ROI_2,' + $
			string((camera.sensor_pixels[0] - values[4]) / 2) + $
			'\,' + string(camera.sensor_pixels[1] - values[3] - values[2]) + $
			'\,' + string(values[4]) + $
			'\,' + string(values[3]), $
			visp_fq + '.vcc' + c + '_hw_win_ROI_3,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_hw_win_ROI_4,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_binSize,' + $
			string(values[0]) + $
			'\,' + string(values[1]), $
			visp_fq + '.vcc' + c + '_sw_win_roiType,"horizontal"', $
			visp_fq + '.vcc' + c + '_sw_win_numberOfROIs,1', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_1,0\,0\,' + $
			string(values[4]) + $
			'\,' + string(values[3]*2), $
			visp_fq + '.vcc' + c + '_sw_win_ROI_2,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_3,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_4,0\,0\,0\,0']

		out_small_rois = [out_small_rois, visp_fq + '.vcc' + c + '_hw_binSize,1\,1', $
			visp_fq + '.vcc' + c + '_hw_win_numberOfROIs,2', $
			visp_fq + '.vcc' + c + '_hw_win_ROI_1,' + $
			string((camera.sensor_pixels[0] - small_values[4]) / 2) + $
			'\,' + string(small_values[2]) + $
			'\,' + string(small_values[4]) + $
			'\,' + string(small_values[3]), $
			visp_fq + '.vcc' + c + '_hw_win_ROI_2,' + $
			string((camera.sensor_pixels[0] - small_values[4]) / 2) + $
			'\,' + string(camera.sensor_pixels[1] - small_values[3] - small_values[2]) + $
			'\,' + string(small_values[4]) + $
			'\,' + string(small_values[3]), $
			visp_fq + '.vcc' + c + '_hw_win_ROI_3,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_hw_win_ROI_4,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_binSize,' + $
			string(small_values[0]) + $
			'\,' + string(small_values[1]), $
			visp_fq + '.vcc' + c + '_sw_win_roiType,"horizontal"', $
			visp_fq + '.vcc' + c + '_sw_win_numberOfROIs,1', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_1,0\,0\,' + $
			string(small_values[4]) + $
			'\,' + string(small_values[3]*2), $
			visp_fq + '.vcc' + c + '_sw_win_ROI_2,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_3,0\,0\,0\,0', $
			visp_fq + '.vcc' + c + '_sw_win_ROI_4,0\,0\,0\,0']
	endfor
	; remove all spaces - they are introduced by string formatting
	out_full_rois = strcompress(out_full_rois,/remove_all)
	out_small_rois = strcompress(out_small_rois,/remove_all)


	; get the file template and the id
	caldat, systime(/julian,/utc), mm, dd, yyyy, hh, nn, ss
	dt_string=STRING(yyyy,mm,dd,hh,nn,ss, $
		format='(i04,i02,i02,"T",i02,i02,i02,"Z")')
	file_template = 'ViSP_Configuration_'+dt_string
	file = dialog_pickfile(dialog_parent=calc_base, /write, multiple_files=0, $
		file=file_template, title='Enter DSP file name base', $
		filter=[['*.dsp;*.ip','*.*'],['DSPs and IPs','All files']])
	if file eq '' then return

	; do some sanitizing
	if stregex(file, '\.dsp$', /fold_case, /bool) then $
		file = strmid(file, 0, strlen(file)-4) ; strip trailing .dsp
	if stregex(file, '\.ip$', /fold_case, /bool) then $
		file = strmid(file, 0, strlen(file)-3) ; strip trailing .ip
	for i=0, n_elements(tasks)-1 do if stregex(file, '\.'+tasks[i]+'$', /fold_case, /bool) then $
		file = strmid(file, 0, strlen(file)-strlen(tasks[i])-1) ; strip trailing task names
	if stregex(file, '\.$', /fold_case, /bool) then $
		file = strmid(file, 0, strlen(file)-1) ; strip trailing .

	; now loop over all the tasks we need to save
	get_lun, unit
	for i=0, n_elements(tasks[which])-1 do begin
		thistask = strlowcase(tasks[which[i]])
		thistask_id = 'visp-' + thistask
		if thistask eq 'observe' then case mode of
			0: thistask_id = thistask_id + '.pol'
			1: thistask_id = thistask_id + '.int'
		endcase

		; make the DSP header
		out_header = $
			[strjoin(replicate('#',80)), $
			'# inst-name   = atst.ics.visp', $
			'# obs-task    = ' + thistask, $
			'# dsp-id      = ' + thistask_id + '.' + dt_string, $
			'# description = ' + description, $
			strjoin(replicate('#',80)), $
			'__dsp.modifable,*.*']

		; get the obsmode
		case 1 of
			strmatch(thistask, 'focus'): begin
				; focus has its own obsmode
				obsmode = 'focus'
				out_map = [out_focus, out_map_focus, '', out_small_rois]
			end
			strmatch(thistask, 'align'): begin
				; align is always intensity
				obsmode = 'intensity'
				out_map = [out_map_align, '', out_full_rois]
			end
			strmatch(thistask, 'target'): begin
				; target is always intensity
				obsmode = 'intensity'
				out_map = [out_map_target, '', out_full_rois]
			end
			strmatch(thistask, 'wavecal'): begin
				; wavecal is always intensity
				obsmode = 'intensity'
				out_map = [out_map_wavecal, '', out_full_rois]
			end
			strmatch(thistask, 'polcal'): begin
				; this is always polarimetric
				obsmode = 'polarimetric'
				out_map = [out_map_polcal, '', out_full_rois]
			end
			strmatch(thistask, 'dark'): begin
				; can be intensity or polarimetric, check what the user wants
				if mode eq 0 then obsmode = 'polarimetric' else obsmode = 'intensity'
				out_map = [out_map_darkgain, '', out_full_rois]
			end
			strmatch(thistask, 'gain'): begin
				; can be intensity or polarimetric, check what the user wants
				if mode eq 0 then obsmode = 'polarimetric' else obsmode = 'intensity'
				out_map = [out_map_darkgain, '', out_full_rois]
			end
			strmatch(thistask, 'observe'): begin
				; can be intensity or polarimetric, check what the user wants
				if mode eq 0 then obsmode = 'polarimetric' else obsmode = 'intensity'
				out_map = [out_map_observe, '', out_full_rois]
			end
			else: begin
				print, 'Encountered an unknown task: ', task
			end
		endcase

		; create the DSP file name
		thisfile = file + '.' + thistask + '.dsp'

		; test if file exists, and ask to overwrite
		if file_test(thisfile) then begin
			resp = dialog_message('Overwrite ' + file_basename(thisfile) + '?', $
				/cancel, /question, dialog_parent=calc_base, title='File Exists')
		   	if resp eq 'No' then continue
			if resp eq 'Cancel' then break
		endif
		openw, unit, thisfile
		printf, unit, strjoin(out_header, string(10B))
		printf, unit, ''
		printf, unit, strjoin(strcompress(out_setup,/remove_all), string(10B))
		printf, unit, ''
		printf, unit, visp_fq + '.obsmode,' + obsmode
		printf, unit, strjoin(strcompress(out_map,/remove_all), string(10B))
		close, unit

		; create the associated IP if requested
		if export_ips then begin
			thisip_id = 'ip_' + thistask
			out = [strjoin(replicate('#',80)), $
				'# category    = ip.atst.ics.visp', $
				'# name        = ' + name, $
				'# id          = ' + thisip_id + '.' + dt_string, $
				'# description = ' + description, $
				strjoin(replicate('#',80)), $
				'__instProg.modifiable,*.*', $
				'__instProg.obsTask,' + thistask, $
				'__instProg.script,std-' + thistask, $
				'atst.ics.visp.repeat,no', $
				'atst.ics.visp.sif:dataSetParamSeq,' + thistask_id + '.' + dt_string, $
				'atst.ics.visp.sif:numCycles,1', $
				'atst.ics.visp.sif:obsTask,"' + thistask + '"', $
				'atst.ics.visp.sif:saveData,true']

			; create the IP file name
			thisfile = file + '.' + thistask + '.ip'

			; test if file exists, and ask to overwrite
			if file_test(thisfile) then begin
				resp = dialog_message('Overwrite ' + file_basename(thisfile) + '?', $
					/cancel, /question, dialog_parent=calc_base, title='File Exists')
				if resp eq 'No' then continue
				if resp eq 'Cancel' then break
			endif
			openw, unit, thisfile
			printf, unit, strjoin(out, string(10B))
			close, unit
	endif
	endfor
	free_lun, unit
end
