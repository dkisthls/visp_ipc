pro calculator_gui_load_config, ev, file=file
	visp = get_visp_parameters()

	if size(file, /type) eq 7 then begin
		info = file_info(file)
		if info.exists eq 0 then file = ''
	endif else file = ''

	if file eq '' then file = dialog_pickfile(default_extension='ini', $
			dialog_parent=ev.top, /read, /must_exist, multiple_files=0, $
			file='ViSP_Configuration_Default.ini', $
			filter=[['*.ini','*.*'],['INI files','All files']])

	if file eq '' then return

	; read a configuration from file
	config = mg_read_config(file, /fold_case)

	; test if it appears to be a valid configuration
	if config['visp','mode'] eq !null then begin
		tmp = dialog_message('Configuration file appears to be invalid!', $
			dialog_parent=ev.top)
		return
	endif

	; check the version of the configuration
	version = config->get('config_version', section='visp', type=3)
	msg = [ ]
	switch 1 of
		(version eq !null):
		(version lt 20180220): begin
			msg = [msg, 'Configuration is from an old version, migrating slit value.']
			case config['visp','slit'] of
				'0.0282"': tmp = '17.6D-6'
				'0.0407"': tmp = '25.4D-6'
				'0.0532"': tmp = '33.2D-6'
				'0.1065"': tmp = '66.4D-6'
				'0.2130"': tmp = '132.8D-6'
				else: begin
					msg = [msg, 'Migration of slit value failed.']
					msg = [msg, 'Setting default value. Please verify slit selection!']
					tmp = '25.4D-6'
				end
			endcase
			config['visp','slit'] = tmp
		end
		(version lt 20190802): begin
			if config['visp','mode'] eq 'polarimetry' then begin
				msg = [msg, 'Configuration is from an old version, migrating mode value.']
				config['visp','mode'] = 'polarimetric'
			endif
		end
	endswitch
	if version gt visp.version then begin
		msg = [msg, 'Configuration is from a newer version.']
		msg = [msg, 'You should upgrade to the latest IPC.']
		msg = [msg, 'Attempting to read configuration, please verify results!']
	endif
	if msg ne !NULL then tmp = dialog_message(msg)

	; fill in the configuration

	; set the whole widget insensitive and disable updates while we work
	widget_control, ev.top, sensitive=0
	widget_control, ev.top, update=0 ; this doesn't seem to do much?

	; mode
	widget = widget_info(ev.top, find_by_uname='mode')
	widget_control, widget, get_value=list
	index = (where(list eq config['visp','mode']))[0]
	if index eq -1 then index = 0 ; sanitze
	widget_control, widget, set_combobox_select=index

	; grating
	widget = widget_info(ev.top, find_by_uname='grating')
	widget_control, widget, get_value=list
	index = (where(list eq config['visp','grating']))[0]
	if index eq -1 then index = 0 ; sanitze
	widget_control, widget, set_combobox_select=index

	; slit
	widget = widget_info(ev.top, find_by_uname='slit')
	widget_control, widget, get_uvalue=uvalue
	; pick the closest one, avoids issues comparing float to double etc
	val = config->get('slit', section='visp', type=4)
	index = (sort(abs(uvalue.widths - val)))[0]
	if (abs(uvalue.widths - val))[index] gt 1d-4 then begin
		print, 'Slit value in config deviates from possible values.'
		print, 'Picking closest value. Please verify slit selection!'
	endif
	widget_control, widget, set_combobox_select=index

	; name (string) is a special case, have to strip [ and ] around the string
	widget = widget_info(ev.top, find_by_uname='config_name')
	value = config->get('config_name', section='visp', type=7)
	; test if there are [] around the string
	if strmid(value, 0, 2) eq '[ ' and strmid(value, 1, 2, /reverse_offset) eq ' ]' then $
		value = strmid(value, 2, strlen(value)-4)
	; update the widget
	widget_control, widget, set_value=value

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; alpha and mu are shared (float)
	; map repeats is shared (long)
	; slit step size and slit step time for polarimetry (float)
	; slit positions and modulation states for polarimetry (long)
	; slit velocity and exposure time for intensity (float)
	; exposures for intensity (long)
	names = ['alpha', 'mu', 'map_reps']
	types = [4, 4, 3]
	case mode of
		0: begin
			names = [names, 'slit_step', 'slit_positions', 'dwell_time', 'modulation_states']
			types = [types, 4, 3, 4, 3]
		end
		1: begin
			names = [names, 'slit_velocity', 'exposures', 'frame_rate']
			types = [types, 4, 3, 4]
		end
	endcase
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		widget_control, widget, set_value=config->get(names[j], section='visp', type=types[j])
	endfor

	; set the mode
	calculator_gui_switch_mode, ev

	; loop over the 3 arms
	arms = ['arm1', 'arm2', 'arm3']
	; and parameters
	; position (float)
	; order, spectral_roi_start, spectral_roi_length, spatial_roi_length (long)
	names = ['position', 'order', $
		'spectral_roi_start', 'spectral_roi_length', 'spatial_roi_length']
	types = [4, 3, 3, 3, 3]
	; also fill in comboboxes spectral_binning and spatial_binning
	binning = ['spectral_binning','spatial_binning']
	for i=0,2 do begin
		arm = arms[i]

		for j=0,n_elements(names)-1 do begin
			widget = widget_info(ev.top, find_by_uname=arm + '_' + names[j])
			widget_control, widget, set_value=config->get(names[j], section=arm, type=types[j])
		endfor

		; restore binning comboboxes
		for j=0,n_elements(binning)-1 do begin
			widget = widget_info(ev.top, find_by_uname=arm + '_' + binning[j])
			widget_control, widget, get_value=list
			index = (where(list eq config->get(binning[j], section=arm)))[0]
			if index eq -1 then index = 0 ; sanitze
			widget_control, widget, set_combobox_select=index
		endfor

		; restore enabled state
		widget = widget_info(ev.top, find_by_uname=arm + '_use')
		value = config->get('enabled', section=arm, type=2) ne 0
		widget_control, widget, set_button=value

		; set the enabled state right
		calculator_gui_enable_arm, {id:widget, top:ev.top, select:value}
	endfor
	; re-calculate modulator and data rates
	calculator_gui_update_modulator, ev
	calculator_gui_update_data, ev
	; re-calculate map and timing
	calculator_gui_update_map, ev
	calculator_gui_update_time, ev
	; update the arms
	calculator_gui_update_arm, ev

	; and make the top level widget sensitive again
	widget_control, ev.top, sensitive=1
	widget_control, ev.top, update=1
end
