pro calculator_gui_save_config, ev
	visp = get_visp_parameters()

	config = mgffoptions() ; create empty config

	; config version
	config->put, 'config_version', visp.version, section='visp'

	; mode & grating
	names = ['mode', 'grating']
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		text = widget_info(widget, /combobox_gettext)
		config->put, names[j], text, section='visp'
	endfor

	; slit
	name = 'slit'
	widget = widget_info(ev.top, find_by_uname='slit')
	slit_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list, get_uvalue=uvalue
	slit = (where(list eq slit_text))[0]
	config->put, 'slit', uvalue.widths[slit], section='visp'

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; name, alpha, mu, and map repeats are shared
	; slit step size, slit positions, slit step time, and modulation states for polarimetry
	; slit velocity, exposures, and exposure time for intensity
	names = ['config_name', 'alpha', 'mu', 'map_reps']
	case mode of
		0: names = [names, 'slit_step', 'slit_positions', 'dwell_time', 'modulation_states']
		1: names = [names, 'slit_velocity', 'exposures', 'frame_rate']
	endcase
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		widget_control, widget, get_value=value
		config->put, names[j], value, section='visp'
	endfor

	; loop over the 3 arms
	arms = ['arm1', 'arm2', 'arm3']
	; and parameters
	names = ['position', 'order', $
		'spectral_roi_start', 'spectral_roi_length', 'spatial_roi_start', 'spatial_roi_length']
	; and binning
	binning = ['spectral_binning','spatial_binning']
	for i=0,2 do begin
		arm = arms[i]

		; save the enabled state
		widget = widget_info(ev.top, find_by_uname=arm + '_use')
		config->put, 'enabled', widget_info(widget, /button_set), section=arm

		; save the binning options
		for j=0,n_elements(binning)-1 do begin
			widget = widget_info(ev.top, find_by_uname=arm + '_' + binning[j])
			text = widget_info(widget, /combobox_gettext)
			config->put, binning[j], text, section=arm
		endfor

		; save the parameters
		for j=0,n_elements(names)-1 do begin
			widget = widget_info(ev.top, find_by_uname=arm + '_' + names[j])
			widget_control, widget, get_value=value
			config->put, names[j], value, section=arm
		endfor
	endfor

	; save configuration to file
	caldat, systime(/julian,/utc), mm, dd, yyyy, hh, nn, ss
	dt_string=STRING(yyyy,mm,dd,hh,nn,ss, $
		format='(i04,i02,i02,"T",i02,i02,i02,"Z")')
	file_template = 'ViSP_Configuration_'+dt_string+'.ini'
	file = dialog_pickfile(default_extension='ini', $
		dialog_parent=ev.top, /write, multiple_files=0, /overwrite_prompt, $
		file=file_template, $
		filter=[['*.ini','*.*'],['INI files','All files']])
	if file ne '' then mg_write_config, file, config
end
