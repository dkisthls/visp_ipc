pro calculator_gui_switch_mode, ev
	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; get a bunch of widgets
	widget_step = widget_info(ev.top, find_by_uname='slit_step')
	widget_velocity = widget_info(ev.top, find_by_uname='slit_velocity')
	widget_positions = widget_info(ev.top, find_by_uname='slit_positions')
	widget_exposures = widget_info(ev.top, find_by_uname='exposures')
	widget_dwell = widget_info(ev.top, find_by_uname='dwell_time')
	widget_framerate = widget_info(ev.top, find_by_uname='frame_rate')
	widget_exptime = widget_info(ev.top, find_by_uname='exposure_time')
	widget_expmotion = widget_info(ev.top, find_by_uname='exposure_motion')
	widget_fps = widget_info(ev.top, find_by_uname='fps')
	widget_modstates = widget_info(ev.top, find_by_uname='modulation_states')
	widget_modrate = widget_info(ev.top, find_by_uname='modulation_rate')
	widget_modparams = widget_info(ev.top, find_by_uname='modulation_cycles')
	; unmap things
	case mode of
		0: begin
			widget_control, widget_velocity, map=0
			widget_control, widget_exposures, map=0
			widget_control, widget_framerate, map=0
			widget_control, widget_exptime, map=0
			widget_control, widget_expmotion, map=0
		end
		1: begin
			widget_control, widget_step, map=0
			widget_control, widget_positions, map=0
			widget_control, widget_dwell, map=0
			widget_control, widget_modstates, map=0
			widget_control, widget_modrate, map=0
			widget_control, widget_modparams, map=0
			widget_control, widget_fps, map=0
		end
	endcase
	; now re-map the display
	case mode of
		0: begin
			widget_control, widget_dwell, map=1
			widget_control, widget_step, map=1
			widget_control, widget_positions, map=1
			widget_control, widget_modstates, map=1
			widget_control, widget_modrate, map=1
			widget_control, widget_modparams, map=1
			widget_control, widget_fps, map=1
		end
		1: begin
			widget_control, widget_velocity, map=1
			widget_control, widget_exposures, map=1
			widget_control, widget_framerate, map=1
			widget_control, widget_exptime, map=1
			widget_control, widget_expmotion, map=1
		end
	endcase
end
