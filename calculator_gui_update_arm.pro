pro calculator_gui_update_arm, ev
	camera = get_camera_parameters()
	visp = get_visp_parameters()

	; use cubic spline interpolation
	spline = 1

	; get all arm enabled status
	arms = 'arm' + ['1', '2', '3']
	enabled = bytarr(3)
	for i=0,2 do begin
		widget = widget_info(ev.top, find_by_uname=arms[i]+'_use')
		enabled[i] = widget_info(widget, /button_set)
	endfor

	; get filter selection, positions, and orders
	filter_select = strarr(3)
	positions = fltarr(3)
	orders = intarr(3)
	for i=0,2 do begin
		widget = widget_info(ev.top, find_by_uname=arms[i]+'_position')
		widget_control, widget, get_value=position
		positions[i] = position
		if enabled[i] ne 0 then begin
			widget = widget_info(ev.top, find_by_uname=arms[i]+'_filter_select')
			filter_select[i] = widget_info(widget, /combobox_gettext)
			widget = widget_info(ev.top, find_by_uname=arms[i]+'_order')
			widget_control, widget, get_value=order
			orders[i] = order
		endif
	endfor

	; get spectrograph configuration parameters
	widget = widget_info(ev.top, find_by_uname='alpha') ; tilt angle
	widget_control, widget, get_value=alpha
	widget = widget_info(ev.top, find_by_uname='grating') ; grating
	grating_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	grating = (where(list eq grating_text))[0]
	widget = widget_info(ev.top, find_by_uname='slit') ; slit
	slit_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	slit = (where(list eq slit_text))[0]

	; get map parameters
	widget = widget_info(ev.top, find_by_uname='mu')
	widget_control, widget, get_value=mu

	; calculate wavelengths using the grating equation
	lambdas = 1d / orders * visp.gratings[grating].density * $
		(sin(!dtor * (positions - alpha)) - sin(!dtor * alpha))

	; read in transmission data
	td = get_transmission_data()

	; find the possible filters for each arm
	indexes = (lambdas - td.lambda[0]) / (td.lambda[-1] - td.lambda[0]) * $
		(n_elements(td.lambda) - 1)
	fts = interpolate(smooth(td.tFilter, [1,5], /edge_truncate), indexes, cubic=-0.5*spline)
	possible = fts gt visp.min_filter_transmission

	filters = intarr(3) ; array that contains the filter that was picked for each arm
	used_filters = bytarr(n_elements(td.nFilter)) ; to keep track of which filters are used

	; now decide which filter will be used in which arm
	arm_picked = bytarr(3) ; array that indicates a filter has been picked for each arm
	; handle disabled arms
	for i=0,2 do if not enabled[i] then arm_picked[i] = 1b
	; handle forces selection
	for i=0,2 do if enabled[i] and not strcmp(filter_select[i],'Automatic') then begin
		arm_picked[i] = 1b
		filters[i] = where(filter_select[i] eq td.nFilter)
		used_filters[filters[i]] += 1b
		print, 'arm ', i+1, ' is forced, filter selected is ', filters[i]
		print, filter_select[i]
	endif

	; we start by looking for arms that have no possible filters, then one filter, etc.
	np = 0
	while not array_equal(arm_picked, [1b,1b,1b]) do begin
		; loop over the arms
		for i=0,2 do if enabled[i] and not arm_picked[i] then begin
			if total(possible[*,i], /preserve_type) eq np then begin
				; we have to pick a filter now
				arm_picked[i] = 1b
				w = where(possible[*,i] eq 1 and used_filters eq 0, nw)
				if nw eq 0 then begin
					; there are no filters to pick from
					if np eq 0 then begin
						; special case, there is no filter for this wavelength
						filters[i] = -1
					endif else begin
						; there are filters for this wavelength but they are in use
						; pick the best filter, mark it as used again
						bft = max(fts[*,i], bf)
						filters[i] = bf
						used_filters[bf] += 1b
					endelse
				endif else begin
					; there are filters to pick from, get the best one
					bft = max(fts[w,i], bf)
					filters[i] = w[bf]
					used_filters[w[bf]] = 1b
				endelse
			endif
		endif
		np++
	endwhile

	for arm=0,2 do begin ; loop over all arms
		; shortcut, check if the arm is enabled and continue to the next one if not
		if not enabled[arm] then continue

		name = arms[arm]

		; inhibit updates while we work
		arm_base = widget_info(ev.top, find_by_uname=name)
		widget_control, arm_base, update=0, get_uvalue=uvalue

		; get arm parameters
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_binning')
		spectral_binning = long(widget_info(widget, /combobox_gettext))
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_binning')
		spatial_binning = long(widget_info(widget, /combobox_gettext))
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_roi_start')
		widget_control, widget, get_value=spectral_roi_start
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_roi_length')
		widget_control, widget, get_value=spectral_roi_length
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_roi_length')
		widget_control, widget, get_value=spatial_roi_length
		widget = widget_info(ev.top, find_by_uname=name+'_coherence')
		coher_text = widget_info(widget, /combobox_gettext)
		widget_control, widget, get_value=list
		coherflag = (where(list eq coher_text))[0]

		; get the size of the draw area
		widget = widget_info(ev.top, find_by_uname=name+'_spectrum')
		widget_control, widget, get_value=wid
		wid.SetCurrent
		wid.Erase

		; check if lambda is in a reasonable range
		; limit the range to what is in the atlas
		if lambdas[arm] ge 378d-9 and lambdas[arm] le 1095d-9 then begin

			; calculate efficiencies, flux, and SNR

			; calculate the spectral and spatial FWHM, sampling, and resolution
			resol = resol(alpha, positions[arm], lambdas[arm], arm, grating, slit, $
				spectral_binning, spatial_binning, coherflag)

			; calculate the spectral bandwidth and offset
			spectral_pixel = resol.lambdaFWHM / resol.X_sampling / spectral_binning ; m 
			full_bandwidth = spectral_pixel * camera.spectral_pixels
			bandwidth = spectral_pixel * spectral_roi_length
			bandwidth_offset = spectral_pixel * (spectral_roi_start - camera.spectral_pixels / 2d)

			; calculate the spatial FOV
			spatial_pixel = resol.airy / resol.Y_sampling / spatial_binning ; "
			full_FOV = spatial_pixel * camera.spatial_pixels
			FOV = spatial_pixel * spatial_roi_length
			spatial_roi_start = (camera.spatial_pixels - spatial_roi_length) / 2

			; calculate the losses
			losses = losses(alpha, positions[arm], orders[arm], lambdas[arm], arm, $
				grating, slit, filters[arm], scalar=uvalue.scalar)

			; calculate the photon flux
			flux = flux(lambdas[arm], full_bandwidth, mu) ; W/nm through the 2.8-arcmin field stop

			; convert flux from power through the field stop (W/nm) to energy flux
			; (photons/nm/s/arcsec^2)
			; calculate field stop aperture (arcsec^2) times photon energy hc / lambda (J)
			conv = (1.4d*60)^2*!dpi * 1.986446d-25 / lambdas[arm]
			energy_flux = flux.flux / conv
			cont_energy_flux = flux.cont_flux / conv

			; get the mode
			widget = widget_info(ev.top, find_by_uname='mode')
			mode_text = widget_info(widget, /combobox_gettext)
			widget_control, widget, get_value=list
			mode = (where(list eq mode_text))[0]

			; need the camera FPS for the next calculation
			case mode of
				0: begin ; polarimetric
					widget = widget_info(ev.top, find_by_uname='dwell_time')
					widget_control, widget, get_value=dwell
					widget = widget_info(ev.top, find_by_uname='modulation_states')
					widget_control, widget, get_value=states
				end
				1: begin ; intensity
					widget = widget_info(ev.top, find_by_uname='frame_rate')
					widget_control, widget, get_value=framerate
					dwell = 1./framerate
					states = 1
				end
			endcase
			fps = fps(dwell, states)

			; calculate photo-electrons/pixel/frame
			; resol.lambdaFWHM is in m, but the energy flux is in photons/nm/s/arcsec^2
			; so scale by 1d9 to convert resol.lambdaFWHM from m to nm
			f = losses.system_efficiency * $
				(resol.airy / resol.Y_sampling) * $
				(resol.lambdaFWHM * 1d9 / resol.X_sampling) * $
				visp.slit_widths[slit] * visp.plate_scale / $
				fps.fps
			ph_flux_P = f[0] * energy_flux
			ph_flux_S = f[1] * energy_flux
			cont_ph_flux_P = f[0] * cont_energy_flux
			cont_ph_flux_S = f[1] * cont_energy_flux

			case mode of
				0: begin ; polarimetric
					; camera duty cycle
					duty_cycle = spatial_binning * spectral_binning * camera.well_depth * $
						camera.nte_fill / max([cont_ph_flux_P,cont_ph_flux_S]) < 1d
					; calculate the modulation efficiency
					x = 32d / states
					modeff = poly(x, visp.modulator_parameters)
				end
				1: begin ; intensity
					; camera duty cycle
					duty_cycle = dwell * fps.fps < spatial_binning * spectral_binning * $
						camera.well_depth * camera.nte_fill / $
						max([cont_ph_flux_P,cont_ph_flux_S])
					; set modulation efficiency to zero
					modeff = 0
				end
			endcase

			; noise-to-signal in the integration
			S_P = duty_cycle * ph_flux_P
			S_S = duty_cycle * ph_flux_S
			nsr_P = sqrt(S_P + duty_cycle * camera.dark / fps.fps + camera.readout^2d) / S_P
			nsr_S = sqrt(S_S + duty_cycle * camera.dark / fps.fps + camera.readout^2d) / S_S
			; beam combination
			nsr = sqrt(nsr_P^2 + nsr_S^2) / 2.
			; account for total number of frames
			snr = sqrt(states * fps.cycles) / nsr
			snr_pol = snr * modeff

			; determine the scale for the axis in the plot
			snr_scale_10 = 10d^fix(alog10(max(snr)))
			snr_scale = (fix(max(snr) / snr_scale_10) + 1d)
			if snr_scale lt 3 then $ ; do a little better
				snr_scale = (fix(max(snr) / snr_scale_10 * 5d) + 1d) / 5d
			snr_scale *= snr_scale_10

			; continuum signal-to-noise, same as above
			cont_S_P = duty_cycle * cont_ph_flux_P
			cont_S_S = duty_cycle * cont_ph_flux_S
			cont_nsr_P = sqrt(cont_S_P + duty_cycle * camera.dark / fps.fps + camera.readout^2d) / $
				cont_S_P
			cont_nsr_S = sqrt(cont_S_S + duty_cycle * camera.dark / fps.fps + camera.readout^2d) / $
				cont_S_S
			cont_nsr = sqrt(cont_nsr_P^2 + cont_nsr_S^2) / 2.
			; account for total number of frames
			cont_snr_s = sqrt(states * fps.cycles) / cont_nsr_P
			cont_snr_p = sqrt(states * fps.cycles) / cont_nsr_S
			cont_snr = sqrt(states * fps.cycles) / cont_nsr
			;cont_snr = sqrt(states * fps.cycles) / sqrt(cont_nsr_P_2 + cont_nsr_S_2) * 2d

			; smooth the intensity spectrum to the spectral resolution
			effFWHM = lambdas[arm] / resol.spectral_resolution / 2d / sqrt(2d * alog(2d))
			sint = gauss_smooth(flux.int, effFWHM / (flux.lambda[1] - flux.lambda[0]), $
				/edge_truncate)

			; create the plot window, but don't draw anything yet
			p = plot(([-1.,1.] * 1.5d * full_bandwidth / 2d + lambdas[arm]) * 1d9, [0,1], $
				xstyle=1, ystyle=1, position=[0.13,0.15,0.83,0.97], /nodata, axis_style=1, $
				xtitle='Wavelength [nm]', ytitle='Intensity', /current, font_size=8)
			xax = axis('X', location='top', showtext=0)

			; plot extent of the sensor
			poly = polygon(([-1.,1,1,-1] * full_bandwidth / 2d + lambdas[arm]) * 1d9, [0,0,1,1], $
				/fill_background, fill_color='crimson', /data, linestyle=' ')

			; plot the extent of the spectral ROI
			poly = polygon(([0.,1,1,0] * bandwidth + bandwidth_offset + lambdas[arm]) * 1d9, [0,0,1,1], $
				/fill_background, fill_color='lime green', /data, linestyle=' ')

			; plot the spectrum on the camera
			p = plot(flux.lambda * 1d9, sint, color='black', /overplot, thick=2, $
				xrange=([-1.,1.] * 1.5d * full_bandwidth / 2d + lambdas[arm]) * 1d9, yrange=[0,1])

			; plot the 2nd axis for SNR
			yax = axis('Y', location='right', coord_transform=[0d,snr_scale], color='blue', $
				title='SNR')

			; plot the SNR on the bottom in blueish
			p = plot(flux.lambda * 1d9, snr / snr_scale, color='blue', /overplot, thick=2, $
				xrange=([-1.,1.] * 1.5d * full_bandwidth / 2d + lambdas[arm]) * 1d9, yrange=[0,1])
			if mode eq 0 then $
				p = plot(flux.lambda * 1d9, snr_pol / snr_scale, color='blue', /overplot, thick=1, $
				xrange=([-1.,1.] * 1.5d * full_bandwidth / 2d + lambdas[arm]) * 1d9, yrange=[0,1])

		endif else begin

			; wavelength is out of range, fill in values with nonsense
			spatial_roi_start = !values.d_nan
			losses = {grating_efficiency:!values.d_nan * [1,1], vignetting:!values.d_nan, $
				diffrloss:!values.d_nan, transmission:!values.d_nan * [1,1], filter:'         ', $
				qe:!values.d_nan, system_efficiency:!values.d_nan * [1,1]}
			ph_flux = !values.d_nan
			duty_cycle = !values.d_nan
			snr = !values.d_nan
			resol = {airy:!values.d_nan, Y_sampling:!values.d_nan, $
				spatial_resolution:!values.d_nan, lambdaFWHM:!values.d_nan, $
				X_sampling:!values.d_nan, spectral_resolution:!values.d_nan}
			FOV = !values.d_nan
			bandwidth = !values.d_nan
			cont_snr = !values.d_nan
			cont_snr_s = !values.d_nan
			cont_snr_p = !values.d_nan

		endelse

		; update the widgets
		; spatial ROI start
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_roi_start')
		widget_control, widget, $
			;set_value=string('Start: ', spatial_roi_start, format='(a,i4)')
			set_value=spatial_roi_start
		; wavelength
		widget = widget_info(ev.top, find_by_uname=name+'_lambda')
		widget_control, widget, $
			set_value=string('Wavelength (nm): ', lambdas[arm]*1e9, format='(a,f8.3)') 
		; losses and efficiencies
		widget = widget_info(ev.top, find_by_uname=name+'_snr')
		widget_control, widget, $
			set_value=string('Ic SNR/Px/Int: ', fix(round(cont_snr_s)), format='(a,i4)') + $
			string(' |  ', fix(round(cont_snr_p)), format='(a,i4)')
		widget = widget_info(ev.top, find_by_uname=name+'_bc_snr')
		widget_control, widget, $
			set_value=string('Beam-combined Ic SNR/Px/Int: ', fix(round(cont_snr)), format='(a,i4)')
		widget = widget_info(ev.top, find_by_uname=name+'_vignetting_diffraction')
		widget_control, widget, $
			set_value=string('Vignetting and Diffraction: ', losses.vignetting * losses.diffrloss, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_qe')
		widget_control, widget, $
			set_value=string('Detector QE: ', losses.qe, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_duty_cycle')
		widget_control, widget, $
			set_value=string('Camera Duty Cycle: ', duty_cycle, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_grating_eff')
		widget_control, widget, $
			set_value=string('Grating Efficiency: ', losses.grating_efficiency[0], format='(a,f5.3)') + $
			string(' | ', losses.grating_efficiency[1], format='(a,f5.3)')
		widget = widget_info(ev.top, find_by_uname=name+'_transmission')
		widget_control, widget, $
			set_value=string('Transmission: ', losses.transmission[0], format='(a,f5.3)') + $
			string(' | ', losses.transmission[1], format='(a,f5.3)')
		widget = widget_info(ev.top, find_by_uname=name+'_system_eff')
		widget_control, widget, $
			set_value=string('System Efficiency: ', losses.system_efficiency[0], format='(a,f5.3)') + $
			string(' | ', losses.system_efficiency[1], format='(a,f5.3)')
		; spatial
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_fwhm')
		widget_control, widget, $
			set_value=string('Spatial FWHM ("): ', resol.airy, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_sampling')
		widget_control, widget, $
			set_value=string('Spatial Sampling: ', resol.Y_sampling, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_spatial_resolution')
		widget_control, widget, $
			set_value=string('Spatial Resolution ("): ', resol.spatial_resolution, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_FOV_height')
		widget_control, widget, $
			set_value=string('FOV Height ("): ', FOV, format='(a,f7.3)')
		; spectral
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_fwhm')
		widget_control, widget, $
			set_value=string('Spectral FWHM (pm): ', resol.lambdaFWHM*1e12, format='(a,f7.4)')
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_sampling')
		widget_control, widget, $
			set_value=string('Spectral Sampling: ', resol.X_sampling, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_spectral_resolution')
		widget_control, widget, $
			set_value=string('Spectral Resolution: ', resol.spectral_resolution, $
			format='(a,f7.0)')
		widget = widget_info(ev.top, find_by_uname=name+'_bandwidth')
		widget_control, widget, $
			set_value=string('Bandwidth (nm): ', bandwidth*1e9, format='(a,f7.5)')
		widget = widget_info(ev.top, find_by_uname=name+'_filter')
		widget_control, widget, $
			set_value=string('Order-sorting Filter: ', losses.filter, format='(a,a)')

		; re-enable updates
		widget_control, arm_base, update=1
	endfor

	calculator_gui_check_arm_positions, ev

	; check for interference
	sep = positions[0:1] - positions[1:2] ; this way around because position < 0
	interference = (visp.min_separation - sep) gt 10. * (machar()).eps ; handle float precision

	; set some messages
	m = strarr(3)
	case 1 of
		array_equal(interference, [1, 0]): begin
			m[0] += ' Arm is interferes with arm 2!'
			m[1] += ' Arm is interferes with arm 1!'
		end
		array_equal(interference, [0, 1]): begin
			m[1] += ' Arm is interferes with arm 3!'
			m[2] += ' Arm is interferes with arm 2!'
		end
		array_equal(interference, [1, 1]): begin
			m[0] += ' Arm is interferes with arm 2!'
			m[1] += ' Arm is interferes with arms 1 and 3!'
			m[2] += ' Arm is interferes with arm 2!'
		end
		else: ; do nothing
	endcase
	for i=0,2 do if enabled[i] eq 0 then m[i] += ' Arm is disabled.' else begin
		if filters[i] lt 0 then m[i] += ' No order-sorting filter available!'
		if used_filters[filters[i]] gt 1 then m[i] += ' Duplicate order-sorting filter used!'
	endelse
	; set the messages
	for i=0,2 do begin
		widget = widget_info(ev.top, find_by_uname=arms[i]+'_message')
		widget_control, widget, set_value=m[i]
	endfor

end
