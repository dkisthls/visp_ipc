pro calculator_gui_update_data, ev
	camera = get_camera_parameters()

	conpar_base = widget_info(ev.top, find_by_uname='conpar_base')
	widget_control, conpar_base, update=0

	; get ROI sizes
	n_pixels = 0l
	names = 'arm' + ['1','2','3']
	for i=0,2 do begin
		name = names[i]

		widget = widget_info(ev.top, find_by_uname=name + '_use')
		use = widget_info(widget, /button_set)

		if not use then continue

		widget = widget_info(ev.top, find_by_uname=name + '_spectral_roi_length')
		widget_control, widget, get_value=roi_length
		widget = widget_info(ev.top, find_by_uname=name + '_spectral_binning')
		binning = long(widget_info(widget, /combobox_gettext))
		spectral_roi_size = roi_length / binning

		widget = widget_info(ev.top, find_by_uname=name + '_spatial_roi_length')
		widget_control, widget, get_value=roi_length
		widget = widget_info(ev.top, find_by_uname=name + '_spatial_binning')
		binning = long(widget_info(widget, /combobox_gettext))
		spatial_roi_size = roi_length / binning

		n_pixels += spectral_roi_size * spatial_roi_size
	end

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; get values
	case mode of
		0: begin ; polarimetric
			widget = widget_info(ev.top, find_by_uname='dwell_time')
			widget_control, widget, get_value=dwell
			widget = widget_info(ev.top, find_by_uname='modulation_states')
			widget_control, widget, get_value=states
			widget = widget_info(ev.top, find_by_uname='slit_positions')
			widget_control, widget, get_value=positions
		end
		1: begin ; intensity
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate
			fps = fps(1./framerate, 1)
			dwell = 1. / fps.fps
			widget = widget_info(ev.top, find_by_uname='exposures')
			widget_control, widget, get_value=positions
			states = 1
		end
	endcase

	widget = widget_info(ev.top, find_by_uname='map_reps')
	widget_control, widget, get_value=reps

	; calculate the total number of pixels recorded
	frame_pixels = 2d * n_pixels ; 2 beams
	accumulator_pixels = frame_pixels * states
	maps_pixels = accumulator_pixels * positions * reps

	; data rate for an accumulation
	accumulator_pps = accumulator_pixels / dwell ; pixels per second
	accumulator_rate = camera.bpp / 8d * accumulator_pps / 1000d^2d ; MB per second
	; scale to a convenient size
	ars = 0
	while accumulator_rate / 1000d gt 10 do begin
		ars++
		accumulator_rate /= 1000d
	endwhile

	; total data volume
	maps_data = camera.bpp / 8d * maps_pixels / 1000d^2d ; MB
	; scale to a convenient size
	mds = 0
	while maps_data / 1000d gt 10 do begin
		mds++
		maps_data /= 1000d
	endwhile

	; Volume units
	mgtp = ['M', 'G', 'T', 'P'] + 'B'

	; update the GUI
	widget = widget_info(ev.top, find_by_uname='data_rate')
	widget_control, widget, $
		set_value=string('Data Rate (', mgtp[ars], '/s): ', accumulator_rate, $
			format='(a,a,a,f7.2)')
	widget = widget_info(ev.top, find_by_uname='data_volume')
	widget_control, widget, $
		set_value=string('Data Volume (', mgtp[mds], '): ', maps_data, format='(a,a,a,f7.2)')

	widget_control, conpar_base, update=1
end
