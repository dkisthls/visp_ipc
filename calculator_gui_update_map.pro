pro calculator_gui_update_map, ev
	conpar_base = widget_info(ev.top, find_by_uname='conpar_base')
	widget_control, conpar_base, update=0

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	case mode of
		0: begin ; polarimetric
			; update the map width
			widget = widget_info(ev.top, find_by_uname='slit_step')
			widget_control, widget, get_value=step
			widget = widget_info(ev.top, find_by_uname='slit_positions')
			widget_control, widget, get_value=positions
			width = step * positions
		end
		1: begin ; intensity
			widget = widget_info(ev.top, find_by_uname='slit_velocity')
			widget_control, widget, get_value=velocity
			widget = widget_info(ev.top, find_by_uname='exposures')
			widget_control, widget, get_value=exposures
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate
			fps = fps(1./framerate, 1)
			width = velocity / fps.fps * exposures
		end
	endcase

	widget = widget_info(ev.top, find_by_uname='map_width')
	widget_control, widget, $
		set_value=string('Map Width ("): ', width, format='(a,f7.2)')

	widget_control, conpar_base, update=1
end
