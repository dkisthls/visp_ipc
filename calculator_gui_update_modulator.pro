pro calculator_gui_update_modulator, ev
	visp = get_visp_parameters()

	conpar_base = widget_info(ev.top, find_by_uname='conpar_base')
	widget_control, conpar_base, update=0

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	case mode of
		0: begin ; polarimetric
			; update the frame rate, modulation rate, and modulation cycles
			widget = widget_info(ev.top, find_by_uname='dwell_time')
			widget_control, widget, get_value=dwell
			widget = widget_info(ev.top, find_by_uname='modulation_states')
			widget_control, widget, get_value=states
		end
		1: begin ; intensity
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate
			dwell = 1./framerate
			states = 1
		end
	endcase

	fps = fps(dwell, states)

	widget = widget_info(ev.top, find_by_uname='fps')
	widget_control, widget, $
		set_value=string('  Frame Rate (Hz): ', fps.fps, format='(a,f5.2)')
	widget = widget_info(ev.top, find_by_uname='modulation_rate')
	widget_control, widget, $
		set_value=string('       Modulation Rate (Hz): ', fps.modrate, format='(a,f5.2)')
	widget = widget_info(ev.top, find_by_uname='modulation_cycles')
	widget_control, widget, $
		set_value=string('Number of Modulation Cycles: ', fps.cycles, format='(a,i5)')

	x = 32d / states
	modeff = poly(x, visp.modulator_parameters)
	widget = widget_info(ev.top, find_by_uname='modulation_efficiency')
	widget_control, widget, $
		set_value=string('Modulation Efficiency: ', modeff, format='(a,f5.2)')

	widget = widget_info(ev.top, find_by_uname='exposure_time')
	widget_control, widget, $
		set_value=string('Exposure Time (s): ', 1./fps.fps, format='(a,f5.3)')

	widget = widget_info(ev.top, find_by_uname='slit_velocity')
	widget_control, widget, get_value=velocity
	widget = widget_info(ev.top, find_by_uname='exposure_motion')
	widget_control, widget, $
		set_value=string('Motion During Exposure ("): ', velocity/fps.fps, format='(a,f6.4)')

	widget_control, conpar_base, update=1
end
