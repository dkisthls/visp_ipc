pro calculator_gui_update_time, ev
	conpar_base = widget_info(ev.top, find_by_uname='conpar_base')
	widget_control, conpar_base, update=1

	visp = get_visp_parameters()

	; get the mode
	widget = widget_info(ev.top, find_by_uname='mode')
	mode_text = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	mode = (where(list eq mode_text))[0]

	; get inputs from widgets
	widget = widget_info(ev.top, find_by_uname='map_reps')
	widget_control, widget, get_value=reps
	case mode of
		0: begin ; polarimetric
			widget = widget_info(ev.top, find_by_uname='slit_step')
			widget_control, widget, get_value=step_size
			widget = widget_info(ev.top, find_by_uname='slit_positions')
			widget_control, widget, get_value=positions
			widget = widget_info(ev.top, find_by_uname='dwell_time')
			widget_control, widget, get_value=dwell
			widget = widget_info(ev.top, find_by_uname='modulation_states')
			widget_control, widget, get_value=states

			; calculate the frame rate
			fps = fps(dwell, states)

			; calculate the appoximate time to move the slit from position to position
			slit_step_time = visp.slit_move_time + step_size / visp.slit_move_speed

			; slit step time is quantized by the camera frame rate
			slit_step_time = ceil(slit_step_time * fps.fps) / fps.fps

			; calculate the map time
			map_time = positions * dwell + (positions - 1) * slit_step_time

			; calculate the reset time
			width = step_size * positions
			reset_time = visp.slit_move_time + width / visp.slit_move_speed + visp.map_start_delay

			; calculate the time used for science
			science_time = 1l * positions * reps * dwell
		end
		1: begin ; intensity
			widget = widget_info(ev.top, find_by_uname='slit_velocity')
			widget_control, widget, get_value=velocity
			widget = widget_info(ev.top, find_by_uname='exposures')
			widget_control, widget, get_value=exposures
			widget = widget_info(ev.top, find_by_uname='frame_rate')
			widget_control, widget, get_value=framerate

			; calculate the map time, reset time, and science time
			fps = fps(1./framerate, 1)
			map_time = exposures / fps.fps
			reset_time = visp.slit_move_time + map_time * velocity / visp.slit_move_speed + $
				visp.map_start_delay
			science_time = 1l * exposures * reps / fps.fps
		end
	endcase

	; calculate the map cadence
	map_cadence = map_time + reset_time

	; calculate the total time
	total_time = reps * map_time + (reps - 1) * reset_time

	; calculate the duty cycle
	duty_cycle = science_time / total_time ; reps * positions could be large

	; update the gui
	widget = widget_info(ev.top, find_by_uname='map_time')
	widget_control, widget, $
		set_value=string('Map Time: ', fix(map_time/3600.), ':', fix(map_time/60. mod 60.), $
			':', fix(round(map_time mod 60.)), format='(a,i02,a,i02,a,i02)')
	widget = widget_info(ev.top, find_by_uname='map_cadence')
	widget_control, widget, $
		set_value=string('Map Cadence: ', fix(map_cadence/3600.), ':', $
			fix(map_cadence/60. mod 60.), ':', fix(round(map_cadence mod 60.)), $
			format='(a,i02,a,i02,a,i02)')
	widget = widget_info(ev.top, find_by_uname='total_time')
	widget_control, widget, $
		set_value=string('Total Time: ', fix(total_time/3600.), ':', fix(total_time/60. mod 60.), $
			':', fix(round(total_time mod 60.)), format='(a,i02,a,i02,a,i02)')
	widget = widget_info(ev.top, find_by_uname='duty_cycle')
	widget_control, widget, $
		set_value=string('Duty Cycle: ', fix(round(duty_cycle * 100)), '%', $
		format='(a,i3,a)')

	widget_control, conpar_base, update=0
end
