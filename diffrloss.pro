function diffrloss, lambda, slit
	visp = get_visp_parameters()
	slit_width = visp.slit_widths[slit]

	Atel = .25d * !dpi * visp.telescope_diameter^2d ; area of the telescope aperture

	; FFT resolution (high resolution is not important in Y,
	; since there is no diffraction in that direction)
	Nx = 256
	Ny = 64

	; the following rescaling parameters are used to define the span
	; of the frequency domain for proper renormalization of the transform
	rX = double(Nx) / double(Nx-1)
	rY = double(Ny) / double(Ny-1)

	imag = dcomplex(0.D,1.D)

	Mx = Nx / 2 + (Nx mod 2) - 1
	My = Ny / 2 + (Ny mod 2) - 1

	sX = -Mx
	sY = -My

	; calculate field at the slit
	Hslit = .5d * ((lambda - 380d-9) * (4.3d-3 - 1.5d-3) / (1100d-9 - 380d-9) + 1.5d-3) / $
		(1d0 + exp((59d0 - slit_width / lambda) / 39d0))

	; in order to center around (0,0) an even number of sampling points
	; we must offset the starting point by L/2N; the zero is intermediate
	; between the elements (Mx,My) and (Mx+1,My+1); for all other fields, 
	; the zero is exactly at (Mx,My)
	x0 = (1d / double(2*Nx) - .5d) * slit_width
	y0 = (1d / double(2*Ny) - .5d) * Hslit

	x = x0 + dindgen(Nx) * slit_width / double(Nx)
	y = y0 + dindgen(Ny) * Hslit / double(Ny)

	; calculate the radial distance from (0,0) for all points
	xi = dblarr(Nx,Ny)
	for i=0L,Nx-1 do for j=0L,Ny-1 do xi[i,j] = sqrt(x[i]^2d + y[j]^2d)

	; for problems with axial symmetry, the field at the focal plain is 
	; proportional to the Fourier-Bessel transform of the telescope aperture
	; which is a besinc function; one must also apply the windowing
	; function from the slit before displaying the field
	field1 = pupil(slit_width, Hslit, 0., 0., x, y, 0., 0., 0, 0)
	field2 = besinc(!dpi * visp.telescope_diameter * xi / $
		(lambda * visp.telescope_focal_length)) * $
		exp(imag * !dpi * xi^2d / (lambda * visp.telescope_focal_length))

	field0 = field1 * field2

	Uslit = Atel / sqrt(lambda * visp.telescope_focal_length) * field0

	; calculate field at the collimator lens (involves FFT)
	scaleX = slit_width / Hslit
	scaleY = 1.D

	; the following scaling matrix must be applied to the FFT of the
	; field at the slit
	PP = slit_width * Hslit * $
		exp(-imag * 2d * !dpi * (dindgen(Nx) + sX) * scaleX * x0 / slit_width) # $
		exp(-imag * 2d * !dpi * (dindgen(Ny) + sY) * scaleY * y0 / Hslit)

	Uslit_mod = exp(imag * !dpi * xi^2d / (lambda * visp.collimator_focal_length)) * Uslit

	Ucoll0 = PP * ffft2d(Uslit_mod, sX, sY, scaleX, scaleY) / $
		sqrt(lambda * visp.collimator_focal_length)

	; the frequencies of the last FFT are converted into coordinates
	; on the plane of the collimator lens
	; these coordinates are transferred 1:1 also to the grating plane
	; and to the camera lens
	ux = (dindgen(Nx) + sX) * scaleX * lambda * visp.collimator_focal_length / slit_width
	uy = (dindgen(Ny) + sY) * scaleY * lambda * visp.collimator_focal_length / Hslit

	Wcoll = rX * (ux[Nx-1] - ux[0])
	Hcoll = rY * (uy[Ny-1] - uy[0])

	ux0 = .5d * (ux[Nx-1] + ux[0] - Wcoll)
	uy0 = .5d * (uy[Ny-1] + uy[0] - Hcoll)

	u2 = dblarr(Nx,Ny)

	for i=0L,Nx-1 do for j=0L,Ny-1 do u2[i,j] = ux[i]^2d + uy[j]^2d

	; apply the windowing of the collimator lens before displaying the field
	Ucoll = Ucoll0 * pupil(visp.collimator_width, visp.collimator_width, $
		0., 0., ux, uy, 0., 0., 0, 0)

	loss = double(total(Ucoll * conj(Ucoll)) / total(Ucoll0 * conj(Ucoll0)))

	return, loss
end
