function ffft,x,kshift,scale
;
; fractional FFT algorithm (see eqs.[12]-[22] of Bailey & Swarztrauber 1991, 
; SIAM Review, 33, 389)
;
; define some local constants
;
N=n_elements(x)

imag=dcomplex(0.,1.)

alpha=double(scale/double(N))
;
; create the 2N-extended Y-vector (eqs.[13] and [15])
;
y=x*exp(-imag*!DPI*dindgen(N)^2d*alpha)
yext=replicate(0.0D,N)

yy=[y,yext]
;
; create the 2N-extended Z-vector, generally for kshift.ne.0 (eqs.[20]-[22])
; (NOTE: eq.[21] is not used because the extended vector is long 2N)
;
z=exp(imag*!DPI*(dindgen(N)+kshift)^2d*alpha)
zext=exp(imag*!DPI*(dindgen(N)+kshift-N)^2d*alpha)

zz=[z,zext]
;
; compute the FFTs of Y and Z, and then the inverse FFT properly
; scaled (eq.[19])
;
fy=fft(yy,/double)
fz=fft(zz,/double)

w=fy*fz
;
; (NOTE: since z is long N, the above scaling automatically cuts off
;  the final transform to only the first N elements); the factor "2"
; seems to be necessary for proper normalization of the transform (?)
;
ff=2.0D*conj(z)*fft(w,/double,/inverse)

return,ff

end
