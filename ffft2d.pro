function ffft2d,z,ksx,ksy,sx,sy

Nx=n_elements(reform(z(*,0)))
Ny=n_elements(reform(z(0,*)))

fz=dcomplexarr(Nx,Ny)
ffz=fz

for i=0L,Nx-1 do begin
  y=reform(z(i,*))
  fz(i,*)=ffft(y,ksy,sy)
endfor

for j=0L,Ny-1 do begin
  x=reform(fz(*,j))
  ffz(*,j)=ffft(x,ksx,sx)
endfor

return,ffz

end
