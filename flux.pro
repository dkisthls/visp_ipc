function flux, lambda, full_bandwidth, mu
	; use cubic spline interpolation
	spline = 1

	; read in irradiance
	fd = get_flux_data()

	; read the FTS atlas
	; read some extra to ensure we got it all
	ftsread, int, fix(round((lambda - full_bandwidth * 2) * 1d10)) > 3760l < 10950l, $
		fix(round((lambda + full_bandwidth * 2) * 1d10)) > 3780 < 11000l, xlam=lam
	int /= 1e4 ; normalize continuum
	lam *= 1d-10 ; scale to m

	; find the continuum flux level
	; interpolate the FTS atlas to the DKIST spectral positions
	wmin = min(where(fd.lambda gt min(lam)))
	wmax = max(where(fd.lambda lt max(lam)))
	s = fix((fd.lambda[wmin+1] - fd.lambda[wmin])/(lam[1]-lam[0]))
	ftsint = interpol(smooth(int,s), lam, fd.lambda[wmin:wmax], spline=spline)
	cont_flux = mean(fd.flux[wmin:wmax] / ftsint)

	; scale the FTS atlas to the DKIST flux level
	flux = cont_flux * int

	; correct for limb darkening
	limb = limbdarkening(lambda * 1e9, sqrt(1-mu^2))
	flux *= limb
	cont_flux *= limb

	; return also the FTS atlas since it'll be used later in the plots
	return, {lambda:lam, flux:flux, cont_flux:cont_flux, int:int}
end
