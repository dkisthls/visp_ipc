function fps, dwell, states
	camera = get_camera_parameters()

	; calculate the FPS and the number of cycles
	if states eq 1 then begin ; we're dealing with intensity mode
		fps = 1. / dwell < camera.max_fps
		cycles = 1
		modrate = 0
	endif else begin
		; cycles must be integer and FPS as close to max_camera_fps as possible
		cycles = long(dwell / states * camera.max_fps) > 1
		fps = cycles * states / dwell < camera.max_fps
		modrate = fps / states
	endelse

	return, {cycles:cycles, fps:fps, modrate:modrate}
end
