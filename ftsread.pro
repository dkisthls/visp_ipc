pro Ftsread, data, wl, nblocks, xlam=xlam, plot=iplot
;+
; NAME:
;	FTSREAD
; PURPOSE:
;	Extract spectral data from the Kitt Peak FTS-Spectral-Atlas
;	as provided by H. Neckel, Hamburg.
; CATEGORY:            @CAT-# 31 32 25@
;	Spectral Analysis , Spectral Lines Identification , Plotting
; CALLING SEQUENCE:
;	FTSREAD,data [,lambda_start , {lambda_end | nblocks}] ...
;               ... [,XLAM=xlam] [,/PLOT]
; INPUTS:
;	data    : Name of variable to store the profile
;	NBLOCKS : Number of blocks to extract (1000 mA each) or end
; 	          wavelength in Angstroem 
; OPTIONAL INPUT:
;	lambda_start  : Starting wavelength in Angstroem ( >= 3290)
;	          If not specified (only ONE non-keyword variable on call),
;		  the procedure will querry user for starting and end value.
;       lambda_end    : End wavelength in Angstroem ( <= 12510)
;          or :
;	nblocks : Number of blocks to extract (1000 mA each); this will be
;		  assumed if specified value is less than lambda_start.
; 	   If not specified,  the procedure will querry user for an end value.
; KEYWORDS:
;       DIR  :  string, the directory to be used for local file. 
;	        Usefull for example if the current directory is not 
;		mounted on daisy.
;       
;       PLOT :  if set, the selected part of the atlas will be plotted 
;		on the active device.
; OUTPUTS:
;	DATA    : Array, name of variable to store the spectral data.
; 	          Stepwidth is 2mA, i.e. 500 points are 1 Angstroem.
; OPTIONAL OUTPUT:
;       XLAM=x  : x must be a named variable; the wavelength values is 
;	          returned in this variable.
; RESTRICTIONS:
;   The original data in the atlas by H. Neckel span the range from 
;   3290 A to 12510 A.  They are based on Fourier-Transform-Spectra from
; 	the Kitt Peak Observatory taken by J. Brault et al.  Due to the
; 	nature of FTS-spectra (equidistancial in inverse wavenumbers),
; 	the wavelength-resolution varies from 3.66mA at 3290 A to 17.58
; 	mA at 12510 A. For this compilation, the data has been
; 	interpolated for a fixed stepwidth of 2mA. Therefore keep in
; 	mind that these data DON'T show the real resolution of the spectra.
; PROCEDURE:
;   If some input data is missing, the user is prompted to type them
; 	in.  The data is stored in an file (fortran unformatted
; 	integer), so one block (1A) equals 1000 Byte.
; MODIFICATION HISTORY:
;	PS  (KIS)  1992-06-12
;       nlte (KIS) 1993-04-08: plot-option, atlas on DAISY or GOOFY
;			       English messages.
;       nlte (KIS) 1993-07-07: bug determination host.
;       ps (USG)   1996-05-22: Adapt for USG, take out host code
;       ps (USG)   1997-10-15: Keyword swap (for linux machines)
;       pit(SIU)   1999-05-11: Automate swap
;       2003-Aug-07  variable nblocks got overwritten - use nbl internally
;       AdW        2017-02-13: get rid of dd hack
;---

on_error,2

swap=(byte(1, 0, 1))(0)

up = string(byte([27, 91, 65]))
left = '            '
left = left+string(byte([27, 91, 68, 27, 91, 68, 27, 91, 68, 27, 91, 68, $
                         27, 91, 68, 27, 91, 68, 27, 91, 68, 27, 91, 68, $
                         27, 91, 68, 27, 91, 68, 27, 91, 68, 27, 91, 68]))

 ;;; set the full path name of FTS-atlas file::
sdir = '.'

IF n_params() LT 1 THEN $
  message, 'usage: FTSREAD,data [,lambda_start,{lambda_end | nblocks}] [,XLAM=xlam] [,/PLOT]'

IF n_params() LT 2 THEN BEGIN
    print, 'Extract a wavelength range from the Kitt Peak FTS-Spectral-Atlas'
    print, '    (J. Brault et al., H.Neckel)'
    print & print
    REPEAT BEGIN
        ok = 0
        writeu, -1, up, 'Enter starting wavelength (3290 - 12508 A)', left
        read, wl & wl=fix(wl)
    ENDREP UNTIL wl GE 3290 AND wl LT 12509
ENDIF

IF n_params() LT 3 THEN BEGIN
    print
    writeu, -1, 'Enter end wavelength (3290 - 12508 A, must be > start wavel.)'
    writeu, -1, '  or number of blocks of 1 A size' 
    read, nblocks
    nblocks = fix(nblocks > 1)
ENDIF

IF nblocks GT wl THEN $
  nbl=nblocks-wl $
ELSE $
  nbl = nblocks

IF (wl LT 3290) OR (wl GT 12508) THEN $
  message, 'Wavelength-range 3290 to 12510 A only !!'
IF (wl+nbl) GT 12509 THEN BEGIN 
    message, 'Limit wavelength 12509 A. Cutting upper boundary...', /cont
    nbl = 12509-wl
ENDIF

skip = fix(wl-3290)

xlam = findgen(500l*nbl)*.002+wl
data = intarr(500l*nbl)
openr, unit, sdir+'/fts_cent.dat', /get_lun
point_lun, unit, skip*1000l
readu, unit, data
free_lun, unit

IF swap EQ 1 THEN byteorder, data

IF keyword_set(iplot) THEN $
  plot, xlam, data, ystyle=16, $
  tit='Kitt Peak FTS-Spectral-Atlas', xtit='Wavelength [A]'

END

