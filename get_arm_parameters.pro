function get_arm_parameters, lambda, arm
	common vispc_arm, parameters, lens_widths, lens_parameters

	if size(lens_widths, /type) eq 0 then begin

		lens_widths = [0.104d, 0.130d, 0.1575d] ; m
		lens_parameters = [[0.7613d0, 1.720d-4, -8.139d-8], $
			[0.9512d0, 2.141d-4, -1.014d-7], $
			[0.1153d1, 2.595d-4, -1.230d-7]] ; focal length fit parameters

	endif

	lens_width = lens_widths[arm]
	focal_length = lens_parameters[0,arm] + $
		lens_parameters[1,arm] * (lambda*1d9) + $
		lens_parameters[2,arm] * (lambda*1d9)^2d

	if n_elements(focal_length) gt 1 then focal_length = reform(focal_length)

	parameters = {lens_width:lens_width, focal_length:focal_length}

	return, parameters
end
