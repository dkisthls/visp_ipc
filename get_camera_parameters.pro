function get_camera_parameters
	common vispc_camera, parameters

	if size(parameters, /type) eq 0 then begin

		; parameters here should probably come from a config file

		sensor_pixels = [2560l,2160l]
		spatial_pixels = sensor_pixels[0]
		spectral_pixels = 1000l
		pixel_pitch = 6.5d-6 ; m
		well_depth = 3.0d4 ; e-
		readout = 2.6d ; e-
		dark = 0.14d ; e-/sec
		gain = 1/0.45d ; DN/e-
		;max_fps = 50d ; Hz - but may depend on ROI and binning
		bpp = 16l
		nte_fill = 0.75 ; Aim to not exceed 75% filled wells

		; mimic the ViSP software to calculate an accurate predicted frame rate
		; this should be updated if the ViSP software is improved, e.g., if it starts using the
		; DKIST-provided camera config factory
		; in principle we should check the duty cycle to determine the maximum exposure time,
		; but this causes a cyclic dependency so we're just going to assume the maximum exposure
		; time is 20 ms.
		max_exp_time = 0.020 ; s
		read_out_time_delay = 0.01014 ; s, readout time delay implemented in the ViSP software
		max_fps = floor(1 / (max_exp_time + read_out_time_delay))

		parameters = {sensor_pixels:sensor_pixels, $
			spatial_pixels:spatial_pixels, spectral_pixels:spectral_pixels, $
			well_depth:well_depth, readout:readout, dark:dark, gain:gain, $
			max_fps:max_fps, pixel_pitch:pixel_pitch, bpp:bpp, nte_fill:nte_fill}

	endif

	return, parameters
end
