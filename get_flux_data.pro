function get_flux_data
	common vispc_flux, fd

	if size(fd, /type) eq 0 then begin

		restore, 'DKIST_flux.sav'

		fd = {lambda:lambda * 1d-9, flux:flux}

	endif

	return, fd
end
