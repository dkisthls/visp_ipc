function get_grating_data
	common vispc_grating, effTETM

	if size(effTETM, /type) eq 0 then begin

		get_lun, unit

		; solar spectrum
		openr, unit, 'FC_grating.tsv'
		readf, unit, deltas, alphas, orders
		data = dblarr(6, deltas, alphas, orders)
		readf, unit, data
		close, unit

		free_lun, unit

		effTETM = {eff:data}

	endif

	return, effTETM
end
