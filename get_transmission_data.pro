function get_transmission_data
	common vispc_transmission, transmission

	if size(transmission, /type) eq 0 then begin

		restore, 'ViSP_transmission.sav'
		restore, 'ViSP_filters.sav'

		get_lun, unit

		; detector QE
		openr, unit, 'detector.tsv'
		readf, unit, Dlambda
		Ddata = dblarr(2, Dlambda)
		readf, unit, Ddata
		close, unit

		; interpolate on 1-nm grid
		qe = interpol(Ddata[1,*], Ddata[0,*], lambda, spline=spline)

		free_lun, unit

		; lambda is in nm, rescale to m
		transmission = {lambda:lambda * 1d-9, tArm:t_arm, tFilter:t_filter, nFilter:n_filter, qe:qe}
	endif

	return, transmission
end
