function get_visp_parameters, force=force
	common vispc_visp, parameters

	if size(parameters, /type) eq 0 or keyword_set(force) then begin

		; parameters here should probably come from a config file

		; version
		version = 20211005

		; telescope
		telescope_diameter = 4d ; m
		telescope_focal_length = 128d ; m

		; instrument operational modes
		modes = ['polarimetric','intensity']

		plate_scale = 1613d ; arcsec/m
		slit_widths = [17.6d-6, 25.4d-6, 33.2d-6, 66.4d-6, 132.8d-6] ; m
		; slit_names = ['450 nm','650 nm','850 nm','2x 850 nm','4x 850 nm']
		slit_names = ['0.0284"','0.0410"','0.0536"','0.1071"','0.2142"']
		fov_x = 170d ; arcsec

		modulator_parameters = [0.9880d, 1.912d-2, -7.996d-3] * 0.53d
		max_modulator_speed = 360d ; rpm
		min_modulation_states = 5 ; minimum number of states to get a Stokes vector

		; maybe make this into a get_grating_paramters()
		gratings = replicate({name:'', density:0d, width:0d, blaze:0d, loss:0d, nmosg:1, $
			min_tilt_angle:0d, max_tilt_angle:0d}, 1)
		gratings[0].name = '316/63'
		gratings[0].density = 1d-3/316d ; m
		gratings[0].width = 0.328d ; m
		gratings[0].blaze = 63.4d ; degrees
		gratings[0].loss = 0.35d
		gratings[0].nmosg = 1 ; # of gratings in the mosaic
		gratings[0].min_tilt_angle = -74d ; degrees
		gratings[0].max_tilt_angle = -54d ; degrees

		; min allowed transmission for an order-sorting filter to be acceptable
		min_filter_transmission = 0.75

		min_cam_angle = -35.3d ; degrees
		max_cam_angle = -3.3d ; degrees
		min_separation = [3.55d, 4.35d] ; degrees
		overlap_tolerance = 0.2d

		slit_move_speed = 4.0d ; arcseconds/second
		; move at least 1.01 650-nm matched slit widths in 0.2 s
		slit_move_time = 0.2 - 1.01 * slit_widths[1] * plate_scale / slit_move_speed ; seconds
		map_start_delay = 11d ; s, startup delay

		collimator_width = 0.09d ; m
		collimator_focal_length = 2.36665d ; m

		; optimizer parameters
		optimizer_combinations = ['Singlets', 'Doublets', 'Triplets']

		parameters = {version:version, modes:modes, $
			slit_widths:slit_widths, slit_names:slit_names, $
			plate_scale:plate_scale, fov_x:fov_x, $
			min_cam_angle:min_cam_angle, max_cam_angle:max_cam_angle, $
			min_separation:min_separation, overlap_tolerance:overlap_tolerance, $
			min_filter_transmission:min_filter_transmission, $
			gratings:gratings, $
			telescope_diameter:telescope_diameter, telescope_focal_length:telescope_focal_length, $
			modulator_parameters:modulator_parameters, max_modulator_speed:max_modulator_speed, $
			min_modulation_states:min_modulation_states, $
			slit_move_time:slit_move_time, slit_move_speed:slit_move_speed, $
			map_start_delay:map_start_delay, $
			collimator_width:collimator_width, collimator_focal_length:collimator_focal_length, $
			optimizer_combinations:optimizer_combinations}

	endif

	return, parameters
end
