function grating_eff, alpha, position, order, lambda, grating, scalar=scalar
	visp = get_visp_parameters()
	effTETM = get_grating_data()

	effTE = 0.
	effTM = 0.
	if not keyword_set(scalar) then begin

		; look for the correct order and alpha and delta values that bracket the desired one
		ordrng = where(effTETM.eff[3,0,0,*] eq order,o_count)
		alpha_rng1=where(reform(effTETM.eff[4,0,*,0]) le alpha,a_count1)
		alpha_rng2=where(reform(effTETM.eff[4,0,*,0]) ge alpha,a_count2)
		delta_rng1=where(reform(effTETM.eff[5,*,0,0]) le position,d_count1)
		delta_rng2=where(reform(effTETM.eff[5,*,0,0]) ge position,d_count2)

		; check to see if they are valid
		if o_count ne 0 and a_count1 ne 0 and a_count2 ne 0 and d_count1 ne 0 and d_count2 ne 0 then begin

			ord_ind=ordrng[0]

			alpha_ind1=alpha_rng1[0]
			alpha_ind2=alpha_rng2[a_count2-1]

			if (alpha_ind1 eq alpha_ind2) then alpha_ind=alpha_ind1 else begin

				alpha1=effTETM.eff[4,0,alpha_ind1,0]
				alpha2=effTETM.eff[4,0,alpha_ind2,0]

				alpha_ind=float(alpha_ind2-alpha_ind1)*(alpha-alpha1)/(alpha2-alpha1)+alpha_ind1

			endelse

			delta_ind1=delta_rng1[0]
			delta_ind2=delta_rng2[d_count2-1]

			if (delta_ind1 eq delta_ind2) then delta_ind=delta_ind1 else begin

				delta1=effTETM.eff[5,delta_ind1,0,0]
				delta2=effTETM.eff[5,delta_ind2,0,0]

				delta_ind=float(delta_ind2-delta_ind1)*(position-delta1)/(delta2-delta1)+delta_ind1

			endelse

			effTE=bilinear(reform(effTETM.eff[1,*,*,ord_ind]),delta_ind,alpha_ind)
			effTM=bilinear(reform(effTETM.eff[2,*,*,ord_ind]),delta_ind,alpha_ind)
		endif

		effPS = [effTE,effTM]

	endif

	; use a scalar calculation if that's forced or if the measurements are invalid
	if keyword_set(scalar) or (effTE eq 0. and effTM eq 0.) then begin
		beta = position - alpha

		; calculate shadowing factor
		shadow = 1d
		if -alpha lt beta then shadow = cos(!dtor * beta) / cos(!dtor * alpha) * $
			cos(!dtor * (alpha + visp.gratings[grating].blaze)) / $
			cos(!dtor * (beta - visp.gratings[grating].blaze))

		; determine the range of reflected orders for the current wavelength
		min_ord = fix((-1d - sin(!dtor * alpha)) * visp.gratings[grating].density / lambda)
		max_ord = fix((1d - sin(!dtor * alpha)) * visp.gratings[grating].density / lambda)

		; effective facet ratio
		rho = cos(!dtor  * (-alpha > visp.gratings[grating].blaze)) / $
			cos(!dtor * ((-alpha - visp.gratings[grating].blaze) > 0d))

		if (-alpha lt visp.gratings[grating].blaze) then $
			rho = rho / (tan(!dtor * visp.gratings[grating].blaze) * $
			tan(!dtor * (visp.gratings[grating].blaze + alpha)) + 1d)

		grat_arg = sin(!dtor * (-alpha - visp.gratings[grating].blaze)) + $
			sin(!dtor * (beta - visp.gratings[grating].blaze))
		arg = !dpi * rho * (visp.gratings[grating].density / lambda) * grat_arg

		efftmp = shadow * sinc(arg)^2d

		; calculate normalized efficiency

		effnorm = 0d

		for k = min_ord, max_ord do begin
			argtmp = sin(!dtor * alpha) + double(k) * lambda / visp.gratings[grating].density

			beta = asin(argtmp) / !dtor

			grat_arg = sin(!dtor * (-alpha - visp.gratings[grating].blaze)) + $
				sin(!dtor * (beta - visp.gratings[grating].blaze))
			arg = !dpi * rho * (visp.gratings[grating].density / lambda) * grat_arg

			effnorm = sinc(arg)^2d + effnorm
		endfor

		effPS = (1d - visp.gratings[grating].loss) * efftmp / effnorm * [1,1]
	endif

	return, effPS
end
