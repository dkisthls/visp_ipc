function lhs, model, opt, lhs, pwvl, scalar=scalar
	; maintain persistent knowledge of the RNG seed
	common random, seed
	if seed eq !NULL then seed = long(systime(1))

	scalar = keyword_set(scalar)

	visp = get_visp_parameters()

	; initialize tilt and position min, max, and range
	; these get shrunk during the LHS iteration
	tilt_min = model.tilt.tiltmin
	tilt_max = model.tilt.tiltmax
	tilt_rng = tilt_max - tilt_min
	pos_min = model.wvl[pwvl].posmin
	pos_max = model.wvl[pwvl].posmax
	pos_rng = pos_max - pos_min

	; error reduction and range reduction parameters
	err_red = 10d^(-alog10(lhs.reduct) / double(lhs.niters))
	rincr = 1d - lhs.rincr
	rincr_max = 1d - rincr / double(lhs.niters)

	; the grating initialization must be handled ad hoc
	grat_step = double(opt.ngrats) / double(lhs.nsteps)
	grat_min = opt.firstgrat

	; initialize model
	grating = grat_min
	result_str = {name:'', efficiency:0d, grating:0l, tilt:0d, $
		position:dblarr(3), order:lonarr(3), arm:lonarr(3), enabled:bytarr(3)}
	iter = result_str ; start pessimistic

	pos = dblarr(opt.nmult)
	for i=0, lhs.niters-1 do begin
		; initialize LHS
		igrat = indgen(lhs.nsteps+1)
		itilt = indgen(lhs.nsteps+1)
		ipos = indgen(lhs.nsteps+1)

		step = result_str ; start pessimistic

		; calculate granularity
		tilt_step = tilt_rng / double(lhs.nsteps)
		pos_step = pos_rng / double(lhs.nsteps)

		; loop over grid
		for k = lhs.nsteps-1,0,-1 do begin
			; decide which points to sample
			istep = long(round(double(k) * randomu(seed, opt.nmult+2, /double)))

			; grating
			grating = long(double(igrat[istep[opt.nmult]]) * grat_step + grat_min)
			; remove from selection
			if istep[opt.nmult] ne k then $
				igrat[istep[opt.nmult]:k-1] = igrat[istep[opt.nmult]+1:k]

			; tilt
			tilt = itilt[istep[opt.nmult+1]] * tilt_step + tilt_min
			if opt.shdwflag eq 0 then tilt = tilt < (-visp.gratings[grating].blaze)
			; remove from selection
			if istep[opt.nmult+1] ne k then $
				itilt[istep[opt.nmult+1]:k-1] = itilt[istep[opt.nmult+1]+1:k]

			for j=0,opt.nmult-1 do begin
				; arm positions
				pos[j] = ipos[istep[j]] * pos_step[j] + pos_min[j]
				; remove from selection
				if istep[j] ne k then $
					ipos[istep[j]:k-1] = ipos[istep[j]+1:k]
			endfor

			; calculate merit function
			result = lstsqr(model, opt, pwvl, tilt, pos, grating, step, scalar=scalar)

			; save it if it was better
			if result.efficiency gt step.efficiency then step = result
		endfor

		if rincr ne 0d and rincr_max ne 1d then $
			rincr *= 10d^(alog10((1d - rincr_max) / rincr) / double(lhs.niters-i))

		; only accept if there was adequate improvement
		if step.efficiency gt iter.efficiency / (1d - rincr) then iter = step

		; reduce ranges and set new minima and maxima
		tilt_rng = err_red * tilt_rng
		tilt_min = (iter.tilt - tilt_rng) > model.tilt.tiltmin
		tilt_max = (iter.tilt + tilt_rng) < model.tilt.tiltmax
		pos_rng = sqrt(err_red) * pos_rng
		pos_min = (iter.position - pos_rng) > model.wvl[pwvl].posmin
		pos_max = (iter.position + pos_rng) < model.wvl[pwvl].posmax
	endfor

	; give it a name
	formatstr = '(' + string(opt.nmult, format='(i0)') + '(f0.1, :, ", "))' 
	iter.name = string(model.wvl[pwvl[0:opt.nmult-1]].wvl * 1d9, format=formatstr)

	return, iter
end
