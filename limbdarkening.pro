; limb darkening function according to Neckel & Labs (1994)
; lambda0: wavelength for which to be computed.
; radius:  distance from disk center in units of solar radius
;          to compute mu = cos (asin(r/Rsun)), 'cosine of viewing angle'
;          if this is a vector, then the curve for all specified points is computed
function limbdarkening, lambda0, radius
  ; find best wavelength coeffs
  coeffs = readform('limbcoefficients.txt', 7)
  indexLo = max(where(coeffs[0,*] le lambda0))
  indexHi = min(where(coeffs[0,*] ge lambda0))
  if (indexHi eq -1) then indexHi = indexLo
  if (indexLo eq -1) then indexLo = indexHi
  ; compute mu
  mu = cos( asin(radius) )
  ; initialize variables
  curveLo = 0.*radius
  curveHi = 0.*radius

  ; create curves and normalize to maximum value
  for i=0,5 do curveLo += coeffs[i+1,indexLo]*mu^i
  for i=0,5 do curveHi += coeffs[i+1,indexHi]*mu^i
  ; interpolate
  if (indexLo ne indexHi) then $
    t = (lambda0-coeffs[0,indexLo])/(coeffs[0,indexHi]-coeffs[0,indexLo]) $
  else $
    t = 1.0

  output = (1-t)*curveLo + (t)*curveHi

;  print, 'lo data:', coeffs[*,indexLo]
;  print, 'hi data:', coeffs[*,indexHi]

  return, output
end

