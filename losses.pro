function losses, alpha, position, order, lambda, arm, grating, slit, filter, scalar=scalar
	; calculates the losses for an arm configuration

	visp = get_visp_parameters()

	; estimate grating efficiency
	grating_efficiency = grating_eff(alpha, position, order, lambda, grating, $
		scalar=keyword_set(scalar))

	; calculate vignetting
	vignetting = vignetting(alpha, position, lambda, arm, grating)
	
	; loss due to diffraction at the slit
	diffrloss = diffrloss(lambda, slit)

	; calculate spectrograph transmission
	trans = transmission(lambda, arm, filter)

	; calculate camera QE
	qe = qe(lambda, arm)

	; calculate total spectrograph system efficiency
	system_efficiency = qe * trans.trans * grating_efficiency * vignetting * diffrloss

	return, {grating_efficiency:grating_efficiency, vignetting:vignetting, diffrloss:diffrloss, $
		transmission:trans.trans, filter:trans.filter, qe:qe, system_efficiency:system_efficiency}
end
