function lstsqr, model, opt, pwvl, tilt, pos, grating, result, scalar=scalar
	wvl = model.wvl[pwvl].wvl
	camera = get_camera_parameters()
	visp = get_visp_parameters()

	; avoid overwriting result
	thisresult = result

	; default return values
	thisresult.efficiency = 0d
	ord = lonarr(3)
	weff = dblarr(3)
	effP = weff
	effS = weff

	; shortcuts
	offcam = opt.firstcam - 1
	rule = visp.gratings[grating].density
	blaze = visp.gratings[grating].blaze
	loss = visp.gratings[grating].loss
	width = visp.gratings[grating].width

	; take into account spectral arm encumbrances
	boff = 0d
	eoff = 0d
	if (offcam gt 0) then boff = total(visp.min_separation[0:offcam-1])
	if (offcam + opt.nmult lt 3) then eoff = total(visp.min_separation[offcam+opt.nmult-1:*])

	; calculate orders for given arm positions and tilt
	for ii=0, opt.nmult-1 do ord[ii] = long(round(visp.gratings[grating].density / wvl[ii] * $
			(sin(!dtor * (pos[ii] - tilt)) - sin(!dtor * tilt))))
	det0 = 2d * (1d + cos(!dtor * pos)) - (ord * wvl / rule)^2d

	; restrict the angular range by the encumbrance of the non-used cameras
	posmin = dblarr(opt.nmult)
	posmax = dblarr(opt.nmult)
	for ii=0, opt.nmult-1 do begin
		posmin[ii] = max([model.wvl[pwvl[ii]].posmin, visp.min_cam_angle + eoff])
		posmax[ii] = min([model.wvl[pwvl[ii]].posmax, visp.max_cam_angle - boff])
	endfor

	; check if any arm position is fixed, and find grating tilt angle if it is
	nfixedpos = 0
	for ii=0, opt.nmult-1 do if posmin[ii] eq posmax[ii] then begin
		; keep track of the number of fixed arms
		nfixedpos++
		if nfixedpos ne 0 then prevtilt = tilt

		tilt = 2d * atan(sqrt(det0[ii]) - (cos(!dtor * pos[ii]) + 1d), $
			ord[ii] * wvl[ii] / rule + sin(!dtor * pos[ii])) / !dtor

		; in case more than one arm is fixed, check that one grating tilt works for both
		; this is pretty unlikely to be the case exactly, so give it a bit of slack
		if nfixedpos ne 0 and $
			abs(prevtilt - tilt) gt (model.tilt.tiltmax - model.tilt.tiltmin) / 1000. then $
				return, thisresult

		; check if it is in the allowed range
		if (tilt gt model.tilt.tiltmax or $
			tilt lt model.tilt.tiltmin) then return, thisresult

		; check if it is below the blaze angle if shadowing is not allowed
		if opt.shdwflag eq 0 and tilt gt (-visp.gratings[grating].blaze) then return, thisresult
	endif

	for ii=0, opt.nmult-1 do begin
		; reject it if there is no room to position the arm
		if posmin[ii] gt posmax[ii] then return, thisresult

		; verify reality condition for the given wavelength and its positioning 
		; within the specified limits; otherwise pick another random model
		if posmin[ii] ne posmax[ii] then begin ; this arm is not fixed
			arg = sin(!dtor * tilt) + ord[ii] * wvl[ii] / rule

			if abs(arg) gt 1d then return, thisresult

			pos[ii] = asin(arg) / !dtor + tilt

			if pos[ii] lt posmin[ii] or pos[ii] gt posmax[ii] or $
				pos[ii] eq 2d * tilt then return, thisresult
		endif

		effPS = grating_eff(tilt, pos[ii], ord[ii], wvl[ii], grating, $
			scalar=keyword_set(scalar))
		effP[ii] = effPS[0]
		effS[ii] = effPS[1]
	endfor

	; assign the order based on positions
	parm = sort(-pos[0:opt.nmult-1])
	arm = lonarr(3)
	arm[parm] = indgen(opt.nmult) + offcam
	; fill the remaining ones up
	if opt.nmult lt 3 then begin
		if offcam eq 0 then arm[opt.nmult:2] = lindgen(3-opt.nmult) + opt.nmult
		if offcam eq 1 or 3 - opt.nmult - offcam eq 0 then arm[opt.nmult:2] = lindgen(3-opt.nmult)
		if offcam eq 1 and opt.nmult eq 1 then arm[opt.nmult:2] = [0,2]
	endif

	; get arm focal lengths
	arm_p = get_arm_parameters(wvl, arm)

	; calculate bandwidths (in degrees)
	BW = camera.spectral_pixels * camera.pixel_pitch / arm_p.focal_length / !dtor

	; test for separation, only for pairs of lines that are not allowed to overlap
	overlap = dblarr(opt.nmult)

overlap_loop:

	intrep = 0
	for ii=0, opt.nmult-2 do begin
		for jj=ii+1, opt.nmult-1 do begin
			overlap = visp.min_separation[ii + offcam] - abs(pos[parm[jj]] - pos[parm[ii]])

			; check for overlap and take care of the rare cases in which we have to iterate to
			; move the arms out
			if overlap gt 0.001d and $
				(model.wvl[pwvl[ii]].iover eq 0 or model.wvl[pwvl[jj]].iover eq 0) then begin

				intrep = 1

				if overlap lt visp.overlap_tolerance * (BW[ii] + BW[jj]) then begin
					pos[arm[ii]] = (pos[parm[ii]] + BW[ii] * overlap / (BW[ii] + BW[jj])) $
						< visp.max_cam_angle
					pos[arm[jj]] = (pos[parm[jj]] - BW[jj] * overlap / (BW[ii] + BW[jj])) $
						> visp.min_cam_angle
				endif else return, thisresult
			endif
		endfor
	endfor

	; iterate until done
	if intrep eq 1 then goto, overlap_loop ; ew, goto statement

	;  apply correction for vignetting
	for ii=0, opt.nmult-1 do begin
		vignett = vignetting(tilt, pos[ii], wvl[ii], arm[ii], grating)

		effP[ii] = vignett * effP[ii]
		effS[ii] = vignett * effS[ii]

		; reject if the efficiency is too low
		weff[ii] = .5d * (effP[ii] + effS[ii])
		;if weff[ii] lt opt.mineff then return, thisresult
		if effP[ii] lt opt.mineff or effS[ii] lt opt.mineff then return, thisresult
	endfor

	; fill in result structure
	thisresult.efficiency = product(weff[0:opt.nmult-1])^(1d / double(opt.nmult))
	thisresult.tilt = tilt
	thisresult.position[0:opt.nmult-1] = pos[0:opt.nmult-1]
	thisresult.order[0:opt.nmult-1] = ord[0:opt.nmult-1]
	thisresult.arm = arm
	thisresult.grating = grating
	thisresult.enabled[0:opt.nmult-1] = 1b

	return, thisresult
end
