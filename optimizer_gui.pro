pro optimizer_gui, opt_base=opt_base, calc_base=calc_base, advanced=adv, scalar=scalar
	if float(!version.release) lt 8.3 then begin
		print, 'The ViSP GUIPC requires at least IDL version 8.3, sorry.'
		print, 'Please update your IDL version or use the IDL run-time environment with the GUIPC IDL Save file.'
		return
	endif

	adv = keyword_set(adv)
	scalar = keyword_set(scalar)

	visp = get_visp_parameters()

	; check if widget already exists, and if so make it visible if it isn't already
	if keyword_set(opt_base) then $
		if widget_info(opt_base, /valid_id) then begin
			widget_control, opt_base, map=1
			return
		endif

	; base widget containers
	opt_base = widget_base(column=1, title='ViSP Optimizer', uname='optimizer')
	;print, 'Optimizer base widget id: ', opt_base

	base = widget_base(opt_base, row=1)
	config_base = widget_base(base, column=1, /frame)
	grating_base = widget_base(base, column=1, /frame, /base_align_right)
	lhs_base = widget_base(base, column=1, /frame, /base_align_right)
	buttons_base = widget_base(base, column=1)

	; configuration widgets
	widget = visp_cw_field(config_base, /long, xsize=3, $
		title='Number of Lines:', value=3, $
		uname='n_lines', /return_events, /focus_events)
	base2 = widget_base(config_base, row=1, xpad=0, ypad=0)
	widget = widget_label(base2, value='Optimize as:')
	widget = widget_combobox(base2, value=visp.optimizer_combinations, uname='combination')
	widget_control, widget, set_combobox_select=2
	base = widget_base(config_base, row=1)
	widget = widget_label(base, value='Use Arms:')
	base = widget_base(base, row=1, /nonexclusive)
	widget = widget_button(base, value='1', sensitive=0, uname='use_arm1')
	widget_control, widget, set_button=1
	widget = widget_button(base, value='2', sensitive=0, uname='use_arm2')
	widget_control, widget, set_button=1
	widget = widget_button(base, value='3', sensitive=0, uname='use_arm3')
	widget_control, widget, set_button=1
	widget = visp_cw_field(config_base, /float, xsize=4, /return_events, /focus_events, $
		title='Minimum Efficiency:', value=0.05, uname='min_eff')
	widget_control, widget, sensitive=adv
	if not adv then widget_control, widget, map=0
	shadow_base = widget_base(config_base, row=1, /nonexclusive)
	button = widget_button(shadow_base, value='Allow Shadowing', $
		uname='allow_shadowing', sensitiv=adv)
	if not adv then widget_control, button, map=0

	; lhs widgets
	label = widget_label(lhs_base, value='LHS Parameters', /align_center)
	widget = visp_cw_field(lhs_base, /float, xsize=5, /return_events, /focus_events, $
		title='Rate:', value=0.999, uname='lhs_rate')
	widget_control, widget, sensitive=adv
	widget = visp_cw_field(lhs_base, /long, xsize=5, /return_events, /focus_events, $
		title='Steps:', value=1000, uname='lhs_steps')
	widget_control, widget, sensitive=adv
	widget = visp_cw_field(lhs_base, /float, xsize=5, /return_events, /focus_events, $
		title='Err. Reduct.:', value=20., uname='lhs_err')
	widget_control, widget, sensitive=adv
	widget = visp_cw_field(lhs_base, /long, xsize=5, /return_events, /focus_events, $
		title='Iterations:', value=20, uname='lhs_iter')
	widget_control, widget, sensitive=adv
	if not adv then widget_control, lhs_base, map=0

	; ViSP logo
	draw = widget_draw(buttons_base, xsize=71, ysize=48)

	; control widgets
	button = widget_button(buttons_base, value=' Close ', uname='button_close')
	button = widget_button(buttons_base, value=' Load ', uname='button_load')
	button = widget_button(buttons_base, value=' Save ', uname='button_save')
	button = widget_button(buttons_base, value=' Optimize ', uname='button_optimize')

	; line widgets
	widget = widget_info(opt_base, find_by_uname='n_lines')
	widget_control, widget, get_value=n_lines
	optimizer_gui_line_base, $
		{id:widget, top:opt_base, handler:0l, value:n_lines, update:1}, advanced=adv

	; grating widgets
	base2 = widget_base(grating_base, row=1, xpad=0, ypad=0)
	widget = widget_label(base2, value='Grating:')
	widget = widget_combobox(base2, value=['auto','316/63'], uname='grating', sensitive=adv)
	widget = visp_cw_field(grating_base, /float, xsize=8, /return_events, /focus_events, $
		title='Min. Tilt (deg):', value=min(visp.gratings.min_tilt_angle), uname='alpha_min')
	widget_control, widget, sensitive=adv
	widget = visp_cw_field(grating_base, /float, xsize=8, /return_events, /focus_events, $
		title='Max. Tilt (deg):', value=max(visp.gratings.max_tilt_angle), uname='alpha_max')
	widget_control, widget, sensitive=adv
	if not adv then widget_control, grating_base, map=0

	; make the calculator the group leader if it exists
	if not keyword_set(calc_base) then calc_base = 0l
	if widget_info(calc_base, /valid_id) then $
		widget_control, opt_base, group_leader=calc_base

	; add state structure
	state = {opt_base:opt_base, calc_base:calc_base, advanced:adv, scalar:scalar}
	widget_control, opt_base, set_uvalue=state

	; create the widget
	widget_control, opt_base, /realize

	; have to load the image after realizing the draw window
	widget_control, draw, get_value=index  
	wset, index
	tv, read_bmp('ViSP_small.bmp')

	; and manage it
	xmanager, 'optimizer_gui', opt_base, /no_block
end
