pro optimizer_gui_combination, ev
	sensitive = [1,1,1]
	set = [1,1,1]
	case ev.index of
		0: set = [1,0,0] ; singlets
		1: begin ; doublets
			sensitive = [1,0,1]
			set = [1,1,0]
		end
		2: sensitive = [0,0,0] ; triplets
	endcase
	for i=0,2 do begin
		s = string(i+1, format='(i0)')
		widget = widget_info(ev.top, find_by_uname='use_arm'+s)
		widget_control, widget, sensitive=sensitive[i], set_button=set[i]
	endfor
end

