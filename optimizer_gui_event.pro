pro optimizer_gui_event, ev
	visp = get_visp_parameters()
	camera = get_camera_parameters()

	; if it is a CW_FIELD widget, check if it has been updated
	if strmatch(widget_info(ev.id, /event_func), 'CW_FIELD_EVENT') then begin
		; old value is stored in the uvalue
		widget_control, ev.id, get_uvalue=old_value
		; return if not updated, otherwise update uvalue
		if old_value eq ev.value then return else $
			widget_control, ev.id, set_uvalue=ev.value
	endif

	; get the widget uname
	uname = widget_info(ev.id, /uname)

	; save the input value
	if tag_exist(ev, 'value') then value_in = ev.value
	msg = ''
	constrained = 0b
	if (!d.name eq 'WIN') then newline = string([13b, 10b]) else newline = string(10b)

	case 1 of
		strmatch(uname, 'n_lines'): begin
			widget_control, ev.top, get_uvalue=state ; get the state
			; sanitze data
			ev.value = ev.value > 1
			msg += newline + 'Number of lines must be at least 1.'
			widget_control, ev.id, set_value=ev.value
			; force the combination type in case of < 3 lines
			widget = widget_info(ev.top, find_by_uname='combination')
			maxindex = 2 < (ev.value - 1)
			combo = widget_info(widget, /combobox_gettext)
			widget_control, widget, get_value=list
			index = (where(list eq combo))[0] < maxindex
			widget_control, widget, set_value=visp.optimizer_combinations[0:maxindex]
			widget_control, widget, set_combobox_select=index
			optimizer_gui_combination, {top:ev.top, id:ev.id, index:index}
			; redraw the line base
			optimizer_gui_line_base, ev, advanced=state.advanced
		end
		strmatch(uname, 'combination'): optimizer_gui_combination, ev ; combobox, no sanitation
		strmatch(uname, 'use_arm1'): optimizer_gui_use_arm, ev
		strmatch(uname, 'use_arm2'): optimizer_gui_use_arm, ev
		strmatch(uname, 'use_arm3'): optimizer_gui_use_arm, ev
		strmatch(uname, 'min_eff'): begin
			; sanitize data
			ev.value = ev.value > 0d < 1d
			msg += newline + 'Minimum efficiency must be between 0 and 1.'
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'allow_shadowing'): begin
			; do nothing
		end
		strmatch(uname, 'grating'): begin
			; do nothing for now, but in the future sanitize tilt angle parameters
		end
		strmatch(uname, 'alpha_min'): begin
			; sanitize data
			ev.value = ev.value > min(visp.gratings.min_tilt_angle) $
				< max(visp.gratings.max_tilt_angle)
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Minimum tilt angle must be between ' + $
					strcompress(string(min(visp.gratings.min_tilt_angle)), /remove_all) + $
					' and ' + $
					strcompress(string(max(visp.gratings.max_tilt_angle)), /remove_all) + '.'
			endif
			widget = widget_info(ev.top, find_by_uname='alpha_max')
			widget_control, widget, get_value=alpha_max
			ev.value = ev.value < alpha_max
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Minimum tilt angle must be less or equal than the maximum tilt angle.'
			endif
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'alpha_max'): begin
			; sanitize data
			ev.value = ev.value > min(visp.gratings.min_tilt_angle) $
				< max(visp.gratings.max_tilt_angle)
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Maximum tilt angle must be between ' + $
					strcompress(string(min(visp.gratings.min_tilt_angle)), /remove_all) + $
					' and ' + $
					strcompress(string(max(visp.gratings.max_tilt_angle)), /remove_all) + '.'
			endif
			widget = widget_info(ev.top, find_by_uname='alpha_min')
			widget_control, widget, get_value=alpha_min
			ev.value = ev.value > alpha_min
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Maximum tilt angle must be greater or equal than the minimum tilt angle.'
			endif
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'lhs_rate'): begin
			; sanitize data
			ev.value = ev.value > 0d < 1d
			msg += newline + 'LHS rate must be between 0 and 1.'
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'lhs_steps'): begin
			; sanitize data
			ev.value = ev.value > 1
			msg += newline + 'LHS steps must be at least 1.'
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'lhs_err'): begin
			; sanitize data
			ev.value = ev.value > 0d
			msg += newline + 'LHS error reduction must be greater or equal to 0.'
			widget_control, ev.id, set_value=ev.value
		end
		strmatch(uname, 'lhs_iter'): begin
			; sanitize data
			ev.value = ev.value > 1
			msg += newline + 'LHS iterations must be at least 1.'
			widget_control, ev.id, set_value=ev.value
		end
		stregex(uname, 'line[0-9]+_lambda', /boolean): begin
			; line wavelength
			ev.value = ev.value > 370d < 1100d ; effectively the max range
			msg += newline + 'Wavelength must be between 370 and 1100 nm.'
			widget_control, ev.id, set_value=ev.value
		end
		; line position parameters
		stregex(uname, 'line[0-9]+_position_min', /boolean): begin
			line = (stregex(uname, 'line([0-9]+)_position_min', /extract, /subexpr))[1]
			ev.value = ev.value > visp.min_cam_angle < visp.max_cam_angle
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Minimum position must be between ' + $
					strcompress(string(visp.min_cam_angle), /remove_all) + ' and ' + $
					strcompress(string(visp.max_cam_angle), /remove_all) + '.'
			endif
			widget = widget_info(ev.top, find_by_uname='line'+line+'_position_max')
			widget_control, widget, get_value=position_max
			ev.value = ev.value < position_max
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Minimum position must be less or equal than the maximum position.'
			endif
			widget_control, ev.id, set_value=ev.value
		end
		stregex(uname, 'line[0-9]+_position_max', /boolean): begin
			line = (stregex(uname, 'line([0-9]+)_position_max', /extract, /subexpr))[1]
			ev.value = ev.value > visp.min_cam_angle < visp.max_cam_angle
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'Maximum position must be between ' + $
					strcompress(string(visp.min_cam_angle), /remove_all) + ' and ' + $
					strcompress(string(visp.max_cam_angle), /remove_all) + '.'
			endif
			widget = widget_info(ev.top, find_by_uname='line'+line+'_position_min')
			widget_control, widget, get_value=position_min
			ev.value = ev.value > position_min
			if value_in ne ev.value then begin
				constrained = 1b
				value_in = ev.value
				msg += newline + 'maximum position must be greater or equal than the minimum position.'
			endif
			widget_control, ev.id, set_value=ev.value
		end
		stregex(uname, 'line[0-9]+_overlap', /boolean): begin
			; do nothing
		end
		strmatch(uname, 'button_optimize'): if ev.select then optimizer_gui_optimize, ev
		strmatch(uname, 'button_load'): if ev.select then optimizer_gui_load_config, ev
		strmatch(uname, 'button_save'): if ev.select then optimizer_gui_save_config, ev
		strmatch(uname, 'button_close'): begin
			widget_control, ev.top, get_uvalue=state ; get the state
			if ev.select then if widget_info(state.calc_base, /valid_id) then $
				widget_control, ev.top, map=0 else $
				widget_control, ev.top, /destroy
			return
		end
		else: begin
			print, 'widget ', uname, ' created an event.'
			help, ev, /str
		end
	endcase

	if tag_exist(ev, 'value') then if ev.value ne value_in then constrained = 1b
	if constrained ne 0 then $
		tmp = dialog_message('Inputs were constrained.' + newline + msg, dialog_parent=ev.top)
end

