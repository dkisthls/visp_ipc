pro optimizer_gui_line_base, ev, advanced=adv
	adv = keyword_set(adv)

	; rebuild the lines section if required
	if ev.update then begin
		; get instrument and camera parameters
		visp = get_visp_parameters()

		; unames and default values
		unames = ['lambda', 'position_min', 'position_max']
		values = rebin([656.3d, visp.min_cam_angle, visp.max_cam_angle], $
			3, ev.value)
		overlap = bytarr(ev.value)

		; read values from existing widgets
		for i=0, ev.value-1 do begin
			name_base = string('line', i+1, '_', format='(a,i0,a)')
			; test if the line exists
			if widget_info(ev.top, find_by_uname=name_base+unames[0]) eq 0 then break
			for j=0,n_elements(unames)-1 do begin
				widget = widget_info(ev.top, find_by_uname=name_base+unames[j])
				widget_control, widget, get_value=tmp
				values[j,i] = tmp
			endfor
			widget = widget_info(ev.top, find_by_uname=name_base+'overlap')
			overlap[i] = widget_info(widget, /button_set)
		endfor

		; destroy the old base
		widget = widget_info(ev.top, find_by_uname='line_base')
		if widget ne 0 then widget_control, widget, /destroy

		; build a new base
		; header
		line_base = widget_base(ev.top, row=ev.value+1, /scroll, /grid, $
			y_scroll_size=160, map=0, uname='line_base')
		widget_control, line_base, update=0
		values1 = ['Line', 'Wavelength', 'Min. Pos.', 'Max. Pos.', 'Overlap']
		values2 = ['', '(nm)', '(deg)', '(deg)', '']
		for i=0,n_elements(values1)-1 do begin
			label_base = widget_base(line_base, row=2)
			label_widget = widget_label(label_base, value=values1[i])
			label_widget = widget_label(label_base, value=values2[i])
			if not (i lt 2 or adv eq 1) then widget_control, label_widget, map=0
		endfor
		; line entries
		for i=0, ev.value-1 do begin
			name_base = string('line', i+1, '_', format='(a,i0,a)')
			label_base = widget_base(line_base, row=1)
			label_widget = widget_label(label_base, $
				value=strcompress(string(i+1), /remove_all))
			for j=0,n_elements(unames)-1 do begin
				widget = visp_cw_field(line_base, /float, xsize=8, /return_events, /focus_events, $
					title='', value=values[j,i], uname=name_base+unames[j])
				widget_control, widget, sensitive=(j eq 0 or adv eq 1)
				if not (j eq 0 or adv eq 1) then widget_control, widget, map=0
			endfor
			overlap_base = widget_base(line_base, row=1, /nonexclusive)
			overlap_widget = widget_button(overlap_base, $
				value=' ', uname=name_base+'overlap', sensitive=adv)
			widget_control, overlap_widget, set_button=overlap[i]
			if not adv then widget_control, overlap_widget, map=0
		endfor

		widget_control, line_base, update=1
		widget_control, line_base, /map
	endif
end
