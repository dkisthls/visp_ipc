pro optimizer_gui_load_config, ev, file=file
	if size(file, /type) eq 7 then begin
		info = file_info(file)
		if info.exists eq 0 then file = ''
	endif else file = ''

	if file eq '' then file = dialog_pickfile(default_extension='ini', $
			dialog_parent=ev.top, /read, /must_exist, multiple_files=0, $
			file='ViSP_Optimizer_Configuration_Default.ini', $
			filter=[['*.ini','*.*'],['INI files','All files']])

	if file eq '' then return

	; read a configuration from file
	config = mg_read_config(file, /fold_case)

	; test if it appears to be a valid configuration
	n_lines = config->get('n_lines', section='visp', type=2)
	if n_lines eq !null then begin
		tmp = dialog_message('Configuration file appears to be invalid!', $
			dialog_parent=ev.top)
		return
	endif

	; fill in the configuration

	; set the whole widget insensitive and disable updates while we work
	widget_control, ev.top, sensitive=0
	widget_control, ev.top, update=0 ; this doesn't seem to do much?
	widget_control, ev.top, get_uvalue=state ; get the state

	; combination type
	widget = widget_info(ev.top, find_by_uname='combination')
	widget_control, widget, get_value=list
	combo = (where(list eq config['visp','combination']))[0]
	if combo eq -1 then combo = 0 ; sanitze
	widget_control, widget, set_combobox_select=combo
	optimizer_gui_combination, {id:widget, top:ev.top, handler:0l, index:combo}

	if combo ne 2 then begin
		; triplets need no action, optimizer_gui_combination sets all arms on
		case combo of
			1: begin ; doublets
				if(config->get('use_arm1', section='visp', /boolean)) then begin
					; if arm1 is enabled in the config, set it
					widget = widget_info(ev.top, find_by_uname='use_arm1')
				endif else begin
					; if arm1 is not enabled in the config, set arm3 enabled
					widget = widget_info(ev.top, find_by_uname='use_arm3')
				endelse
			end
			0: begin ; singlets, get the first arm that's enabled in the config
				case 1 of
					config->get('use_arm1', section='visp', /boolean): begin
						widget = widget_info(ev.top, find_by_uname='use_arm1')
					end
					config->get('use_arm2', section='visp', /boolean): begin
						widget = widget_info(ev.top, find_by_uname='use_arm2')
					end
					config->get('use_arm3', section='visp', /boolean): begin
						widget = widget_info(ev.top, find_by_uname='use_arm3')
					end
				endcase
			end
		endcase
		widget_control, widget, set_button=1
		; generate an event to make sure all buttons get set properly
		optimizer_gui_use_arm, {id:widget, top:ev.top, select:1}
	endif

	; grating
	widget = widget_info(ev.top, find_by_uname='grating')
	widget_control, widget, get_value=list
	index = (where(list eq config['Grating','grating']))[0]
	if index eq -1 then index = 0 ; sanitze
	widget_control, widget, set_combobox_select=index

	; n_lines requires an event to update the widget
	widget = widget_info(ev.top, find_by_uname='n_lines')
	widget_control, widget, set_value=n_lines
	optimizer_gui_line_base, $
		{id:widget, top:ev.top, handler:0l, value:n_lines, update:1}, $
		advanced=state.advanced

	; load parameters
	; min_eff, alpha_min, alpha_max (float)
	names = ['min_eff', 'alpha_min', 'alpha_max']
	types = [4, 4, 4]
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		widget_control, widget, set_value=config->get(names[j], section='visp', type=types[j])
	endfor

	; set buttons 
	; shadowing
	widget = widget_info(ev.top, find_by_uname='allow_shadowing')
	widget_control, widget, set_button=config->get('allow_shadowing', section='visp', type=2)

	; LHS
	; rate, err (float)
	; steps, iter (long)
	names = ['rate', 'err', 'steps', 'iter']
	types = [4, 4, 3, 3]
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname='lhs_' + names[j])
		widget_control, widget, set_value=config->get(names[j], section='lhs', type=types[j])
	endfor

	; fill in lines
	line_base = widget_info(ev.top, find_by_uname='line_base')
	for i=0,n_lines-1 do begin
		name_base = string('line', i+1, format='(a,i0)')

		; lambda, position_min, position_max (float)
		names = ['lambda', 'position_min', 'position_max']
		types = [4, 4, 4]
		for j=0,n_elements(names)-1 do begin
			widget = widget_info(ev.top, find_by_uname=name_base+'_'+names[j])
			widget_control, widget, $
				set_value=config->get(names[j], section=name_base, type=types[j])
		endfor
	endfor

	; and make the top level widget sensitive again
	widget_control, ev.top, sensitive=1
	widget_control, ev.top, update=1
end
