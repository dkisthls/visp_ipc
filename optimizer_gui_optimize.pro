pro optimizer_gui_optimize, ev
	; get instrument and camera parameters
	visp = get_visp_parameters()
	camera = get_camera_parameters()

	; set the whole widget insensitive and disable updates while we work and get the uvalue
	widget_control, ev.top, sensitive=0, get_uvalue=state

	; gather all the inputs

	; number of lines
	widget = widget_info(ev.top, find_by_uname='n_lines')
	widget_control, widget, get_value=n_lines

	; first camera arm to use
	for firstcam=1,3 do begin
		s = string(firstcam, format='(i0)')
		widget = widget_info(ev.top, find_by_uname='use_arm'+s)
		if widget_info(widget, /button_set) then break
	endfor

	; grating
	ngrat = n_elements(visp.gratings)
	firstgrat = 0l

	; optimization setup structure
	opt = {n_lines:n_lines, nmult:1, firstcam:firstcam, mineff:0d, $
		ngrats:ngrat, firstgrat:firstgrat, shdwflag:0b}

	; combination type
	widget = widget_info(ev.top, find_by_uname='combination')
	combo = widget_info(widget, /combobox_gettext)
	widget_control, widget, get_value=list
	opt.nmult = (where(list eq combo))[0] + 1

	; minimum efficiency
	widget = widget_info(ev.top, find_by_uname='min_eff')
	widget_control, widget, get_value=tmp
	opt.mineff = tmp

	; allow shadowing
	widget = widget_info(ev.top, find_by_uname='allow_shadowing')
	opt.shdwflag = widget_info(widget, /button_set)

	; LHS setup structure
	lhs_str = {rincr:0d, reduct:0d, niters:0l, nsteps:0l}

	; lhs parameters
	widget = widget_info(ev.top, find_by_uname='lhs_rate')
	widget_control, widget, get_value=tmp
	lhs_str.rincr = tmp
	widget = widget_info(ev.top, find_by_uname='lhs_err')
	widget_control, widget, get_value=tmp
	lhs_str.reduct = tmp
	widget = widget_info(ev.top, find_by_uname='lhs_iter')
	widget_control, widget, get_value=tmp
	lhs_str.niters = tmp
	widget = widget_info(ev.top, find_by_uname='lhs_steps')
	widget_control, widget, get_value=tmp
	lhs_str.nsteps = tmp

	; model structure
	tilt_str = {tiltmin:0d, tiltmax:0d}
	line_str = {wvl:0d, posmin:0d, posmax:0d, iover:0}
	wvl_str = replicate(line_str, n_lines)
	model = {tilt:tilt_str, wvl:wvl_str}

	; grating tilt parameters
	widget = widget_info(ev.top, find_by_uname='alpha_min')
	widget_control, widget, get_value=tmp
	model.tilt.tiltmin = tmp
	widget = widget_info(ev.top, find_by_uname='alpha_max')
	widget_control, widget, get_value=tmp
	model.tilt.tiltmax = tmp

	; lines
	for i=0, n_lines-1 do begin
		name_base = string('line', i+1, '_', format='(a,i0,a)')
		widget = widget_info(ev.top, find_by_uname=name_base+'lambda')
		widget_control, widget, get_value=tmp
		model.wvl[i].wvl = tmp * 1d-9 ; wavelength
		widget = widget_info(ev.top, find_by_uname=name_base+'position_min')
		widget_control, widget, get_value=tmp
		model.wvl[i].posmin = tmp ; min pos
		widget = widget_info(ev.top, find_by_uname=name_base+'position_max')
		widget_control, widget, get_value=tmp
		model.wvl[i].posmax = tmp ; max pos
		widget = widget_info(ev.top, find_by_uname=name_base+'overlap')
		model.wvl[i].iover = widget_info(widget, /button_set) ; overlap
	endfor

	progress_base = widget_base(group_leader=ev.top, /floating, title='Working...')
	progress_str = 'Working, please wait: '
	nconfigs = long(factorial(n_lines) / factorial(opt.nmult) / factorial(n_lines - opt.nmult))
	widget = widget_text(progress_base, value=progress_str, xsize=strlen(progress_str)+nconfigs)
	widget_control, progress_base, /realize
	widget_control, widget, /update

	; run the optimization
	results = [ ]
	for i=0, n_lines - opt.nmult do begin
		jmax = (opt.nmult gt 1) ? n_lines - opt.nmult + 1 : i+1
		for j=i+1, jmax do begin
			kmax = (opt.nmult gt 2) ? n_lines - opt.nmult + 2 : j+1
			for k=j+1, kmax do begin
				pwvl = [i,j,k] ; lhs ignores all but elements 0 to opt.nmult-1
				result = lhs(model, opt, lhs_str, pwvl, scalar=state.scalar)
				if result.efficiency gt 0d then begin
					results = [results, result] ; append to results
					progress_str += '+'
				endif else progress_str += '-'
				widget_control, widget, set_value=progress_str, /update
			endfor
		endfor
	endfor

	widget_control, progress_base, /destroy

	; pass the output to the calculator if it exists, or show a message if running stand-alone

	; first re-enable the widget
	widget_control, ev.top, sensitive=1

	if state.calc_base ne 0l then begin ; the calculator exists
		widget = widget_info(state.calc_base, find_by_uname='optimizer_config')
		button = widget_info(state.calc_base, find_by_uname='button_load_optimizer')
		if results eq !null then begin ; no results found, wipe it out
			value = string('N/A',format='(a-24)') ; make it 24 characters long
			widget_control, widget, set_uvalue=!null, set_value=value, sensitive=0
			widget_control, button, sensitive=0
		endif else begin ; put the results in the combobox but prepend an index number
			widget_control, widget, set_uvalue=results, /sensitive, $
				set_value=string(indgen(n_elements(results)) + 1, $
					format='(i2, " | ")') + strmid(results.name, 0, 19)
			widget_control, button, sensitive=1
			; issue an event to update the calculator to the first configuration found
			ev = {id:widget, top:state.calc_base, handler:state.calc_base, index:0l}
			calculator_gui_event, ev
		endelse
	endif else begin
		outstring = ["  # | Configuration name    | Eff.   |  Tilt  | Pos. 1 | Pos. 2 | Pos. 3 ", $
			"-------------------------------------------------------------------------"]
		formatstr = '(i3, " | ", a-21, " | ", f6.4, " | ", f6.2, " | ", ' + $
			string(opt.nmult, format='(i0)') + '(f6.2, :, " | "))'
		for i=0, n_elements(results)-1 do $
			outstring = [outstring, string(i, results[i].name, results[i].efficiency, $
			results[i].tilt, results[i].position[0:opt.nmult-1], format=formatstr)]
		tmp = dialog_message(strjoin(outstring, string(10b)), /information, $
			title='Optimizer Output')
	endelse
end
