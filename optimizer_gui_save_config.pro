pro optimizer_gui_save_config, ev
	config = mgffoptions() ; create empty config

	; fill in the configuration

	; config version
	config->put, 'config_version', '20180220', section='visp'

	; combination & grating
	names = ['combination', 'grating']
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		text = widget_info(widget, /combobox_gettext)
		config->put, names[j], text, section='visp'
	endfor

	; loop over buttons
	names = ['use_arm1', 'use_arm2', 'use_arm3', 'allow_shadowing']
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		config->put, names[j], widget_info(widget, /button_set), section='visp'
	endfor

	; special case, need to save n_lines for later
	widget = widget_info(ev.top, find_by_uname='n_lines')
	widget_control, widget, get_value=n_lines
	config->put, 'n_lines', n_lines, section='visp'

	; loop over inputs
	names = ['n_lines', 'min_eff', 'alpha_min', 'alpha_max']
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname=names[j])
		widget_control, widget, get_value=value
		config->put, names[j], value, section='visp'
	endfor

	; LHS
	names = ['rate', 'steps', 'err', 'iter']
	for j=0,n_elements(names)-1 do begin
		widget = widget_info(ev.top, find_by_uname='lhs_' + names[j])
		widget_control, widget, get_value=value
		config->put, names[j], value, section='lhs'
	endfor

	; lines
	for i=0,n_lines-1 do begin
		name_base = string('line', i+1, format='(a,i0)')
		names = ['lambda', 'position_min', 'position_max']
		for j=0,n_elements(names)-1 do begin
			widget = widget_info(ev.top, find_by_uname=name_base+'_'+names[j])
			widget_control, widget, get_value=value
			config->put, names[j], value, section=name_base
		endfor
	endfor

	; save to file
	caldat, systime(/julian,/utc), mm, dd, yyyy, hh, nn, ss
	dt_string=STRING(yyyy,mm,dd,hh,nn,ss, $
		format='(i04,i02,i02,"T",i02,i02,i02,"Z")')
	file_template = 'ViSP_Optimizer_Configuration_'+dt_string+'.ini'
	file = dialog_pickfile(default_extension='ini', $
		dialog_parent=ev.top, /write, multiple_files=0, /overwrite_prompt, $
		file=file_template, $
		filter=[['*.ini','*.*'],['INI files','All files']])
	if file ne '' then mg_write_config, file, config
end
