pro optimizer_gui_use_arm, ev
	; if the button was unselected, re-set it and return
	if ev.select eq 0 then begin
		widget_control, ev.id, set_button=1
		return
	endif

	; find all the button widgets
	button1 = widget_info(ev.top, find_by_uname='use_arm1')
	button2 = widget_info(ev.top, find_by_uname='use_arm2')
	button3 = widget_info(ev.top, find_by_uname='use_arm3')

	; find the combobox
	combobox = widget_info(ev.top, find_by_uname='combination')
	combo = widget_info(combobox, /combobox_gettext)

	; handle buttons, make sure button2 does not get unset
	if ev.id ne button1 then widget_control, button1, set_button=0
	if ev.id ne button2 and combo eq 'Singlets' then widget_control, button2, set_button=0
	if ev.id ne button3 then widget_control, button3, set_button=0
end
