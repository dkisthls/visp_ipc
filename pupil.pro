function pupil,w,h,distx,disty,x0,y0,sX,sY,icircle,iobstr

  Nx=n_elements(x0)
  Ny=n_elements(y0)

  x=x0-sX
  y=y0-sY

  z=dblarr(Nx,Ny)

  w0=w
  h0=h

  w0half=.5D*w0
  h0half=.5D*h0

  x1=-.5D*distx
  x2=-x1

  y1=-.5D*disty
  y2=-y1

  if (icircle eq 1) then begin

    r=.5D*min(w0,h0)
    r2=r*r

    for i=0L,Nx-1 do begin
     xx1=(x(i)-x1)*(x(i)-x1)
     xx2=(x(i)-x2)*(x(i)-x2)
     for j=0L,Ny-1 do begin
      yy1=(y(j)-y1)*(y(j)-y1)
      yy2=(y(j)-y2)*(y(j)-y2)

      z(i,j)=( xx1+yy1 le r2 || $
      	   xx2+yy1 le r2 || $
      	   xx1+yy2 le r2 || $
      	   xx2+yy2 le r2 )? $
		1.D -double(iobstr) : double(iobstr)-0.D
     endfor
    endfor

  endif else begin

    for i=0L,Nx-1 do begin
     dx1=abs(x(i)-x1)
     dx2=abs(x(i)-x2)
     for j=0L,Ny-1 do begin
      dy1=abs(y(j)-y1)
      dy2=abs(y(j)-y2)

      z(i,j)=( (dx1 le w0half && dy1 le h0half) || $
		   (dx2 le w0half && dy1 le h0half) || $
		   (dx1 le w0half && dy2 le h0half) || $
		   (dx2 le w0half && dy2 le h0half) )? $
		1.D -double(iobstr) : double(iobstr)-0.D
     endfor
    endfor

  endelse

  return,z

end
