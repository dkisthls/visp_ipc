function qe, lambda, arm
	; use cubic spline interpolation
	spline = 1

	; read in irradiance and material properties
	td = get_transmission_data()

	; detector
	qe = (interpol(td.qe, td.lambda, lambda, spline=spline))[0]

	return, qe
end
