; ************************************************
; readform: read in matrix data
; input:    filename:	name of ascii data file
;           columns:	number of data columns
;           offset:	header offset before data
; author: F. Woeger, KIS
; ***********************************************

function readform, filename, columns

i = LONG(1)

; Einlesen der Daten
; Read in data
; *****************************
openr,lun_in,filename,/get_lun

; Ermittle die Anzahl der Zeilen
; Find the number of rows
i = 0L
data = strarr(1)
while not eof(lun_in) do begin
  readf,lun_in,data
  i = i + 1
endwhile

;print,'Filelength: ', i, ' rows'

; Initialisiere Variablen
; init variables
liste = fltarr(columns, i)
tmp = fltarr(columns)

; Setze Dateizeiger zurueck
; reset file descriptor
point_lun,lun_in,0

; Zeilenzaehler
; number of rows
i = 0L
; String-Zeilen
j = 0L
; Einlesen:
while not eof(lun_in) do begin
  on_ioerror, str_zeile
  valid = 0
  readf,lun_in,data
  reads,data,tmp
  liste[*,i-j] = tmp
  valid = 1
  str_zeile:
  if NOT valid then j = j + 1
  i = i + 1
endwhile

close,lun_in & free_lun,lun_in

return, reform(liste[*,0:i-j-1])

end
