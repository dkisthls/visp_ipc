function resol, alpha, position, lambda, arm, grating, slit, $
	spectral_binning, spatial_binning, coherflag
	; caculates the spatial and spectral resolution for an arm configuration
	spline = 1

	visp = get_visp_parameters()
	camera = get_camera_parameters()
	arm_p = get_arm_parameters(lambda, arm)

	; fill these in; should come out of a config file? is this really the best way to do it?
	; average values for the ViSP configuration
	; hard-code?
	wvl0 = 450d-9
	slit0 = 17.6d-6

	anamorph = anamorph(alpha, position) ; anamorphic magnification

	; calculate minimum aperture for resolution calculation (considers
	; only "coherent" apertures, hence divides grating width by NmosG)
	min_aperture = min([visp.collimator_width, $
		cos(!dtor * alpha) * visp.gratings[grating].width / visp.gratings[grating].nmosg, $
		anamorph * arm_p.lens_width])

	coherent = 1 ; assumes coherent illumination by default
	; check if coherence is enforced or if illumination is incoherent
	if coherflag lt 2 then coherent = coherflag else $
		if visp.slit_widths[slit] gt 2d * lambda * slit0 / wvl0 then coherent = 0

	; define a properly sized domain to capture the FWHM;
	; the factor of 5 is added to compensate possible strong
	; vignetting, which increases the effective FWHM, and other
	; extreme cases
	npoints = 1001 ; that should provide more than enough resolution
	x0 = 5d * (lambda / wvl0) * visp.slit_widths[slit] / visp.collimator_focal_length
	x = x0 * double(lindgen(npoints) - npoints / 2) / double(npoints)

	; helper variables
	x1 = (!dpi / 2d) * (visp.slit_widths[slit] / visp.collimator_focal_length - 2d * x) / $
		(lambda / min_aperture)
	x2 = (!dpi / 2d) * (visp.slit_widths[slit] / visp.collimator_focal_length + 2d * x) / $
		(lambda / min_aperture)

	; estimate the profile
	; see Casini & De Wijn 2014, JOSAA, vol. 31, issue 9, p. 2002
	if coherent eq 1 then y = (Si(x1) + Si(x2))^2d else $
		y = Si(2d * x1) + Si(2d * x2) - sinc(x1) * sin(x1) - sinc(x2) * sin(x2)
	ny = y / max(y) ; normalize

	; find the location closest to y = 0.5
	yhwoff = min(abs(ny - 0.5d), hw)

	; interpolate x at ny = 0.5
	; this works because ny is monotonic near x[hw]
	h = (interpol(x[hw-2:hw+2], ny[hw-2:hw+2], 0.5d, spline=spline))[0]

	; calculate FWHM on detector plane (units: m)
	halfwidth = anamorph * 2d * abs(h) * arm_p.focal_length

	; calculate FWHM/pixel ratio
	X_sampling = halfwidth / (camera.pixel_pitch * spectral_binning)

	; convert FWHM and pixel pitch to wavelength (units: m)
	lambdaFWHM = lambda * halfwidth / arm_p.focal_length * $
		cos(!dtor * (position - alpha)) / (sin(!dtor * (position - alpha)) - sin(!dtor * alpha))
	lambdaPX = lambdaFWHM / X_sampling

	; calculate effective FWHM and spectral resolution after camera sampling
	effFWHM = lambdaPX * sqrt(X_sampling^2d + 4d * exp(-alog(4d/3d) * X_sampling^2.5d))
	spectral_resolution = lambda / effFWHM ; lingo, it's really resolving power

	; calculate the Airy disk size (units: ")
	airy = 1.22d * lambda / visp.telescope_diameter * (3600d * 180d / !dpi)

	; calculate Airy disk on detector plane (units: m)
	A_rad = airy * (arm_p.focal_length / visp.collimator_focal_length) / visp.plate_scale

	; calculate Airy/pixel ratio
	Y_sampling = A_rad / (camera.pixel_pitch * spatial_binning)

	; calculate effective spatial resolution after camera sampling (units: ")
	effresol = camera.pixel_pitch * spatial_binning * $
		sqrt(Y_sampling^2d + 4d * exp(-alog(4d/3d) * Y_sampling^2.5d))
	spatial_resolution = effresol * visp.plate_scale / $
		(arm_p.focal_length / visp.collimator_focal_length)

	; create the output structure
	return, {lambdaFWHM:lambdaFWHM, airy:airy, $
		X_sampling:X_sampling, Y_sampling:Y_sampling, $
		spectral_resolution:spectral_resolution, spatial_resolution:spatial_resolution }
end
