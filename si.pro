function Si,x

N=n_elements(x)

z=dblarr(N)
;
; find the various domain of approximation
;
zero=where(x eq 0.D)
lt1=where(abs(x) lt 1.D)
gt1pos=where(x gt 0.D and abs(x) ge 1.D)
gt1neg=where(x lt 0.D and abs(x) ge 1.D)
;
; calculate the value at zero
;
if (zero(0) ne -1) then z(zero)=0.D
;
; calculate the power series for abs(x) < 1
;
if (lt1(0) ne -1) then begin

 x0=x(lt1)

 x2=x0*x0

 z(lt1)=x0*(1.D -x2/6.D*(1.D/3.D -x2/20.D*(1.D/5.D $
		  -x2/42.D*(1.D/7.D -x2/648.D))))

endif
;
; calculate the rational approximation for abs(x) >= 1
;
; f(x)
;
a1=38.027264D
a2=265.187033D
a3=335.677320D
a4=38.102495D

b1=40.021433D
b2=322.624911D
b3=570.236280D
b4=157.105423D
;
; g(x)
;
c1=42.242855D
c2=302.757865D
c3=352.018498D
c4=21.821899D

d1=48.196927D
d2=482.485984D
d3=1114.978885D
d4=449.690326D
;
; positive branch
;
if (gt1pos(0) ne -1) then begin

 x0=x(gt1pos)

 x2=x0*x0

 fn=a4+x2*(a3+x2*(a2+x2*(a1+x2)))
 fd=b4+x2*(b3+x2*(b2+x2*(b1+x2)))
 
 f=(fn/fd)/x0

 gn=c4+x2*(c3+x2*(c2+x2*(c1+x2)))
 gd=d4+x2*(d3+x2*(d2+x2*(d1+x2)))

 g=(gn/gd)/x2

 z(gt1pos)=.5D*!PI-f*cos(x0)-g*sin(x0)

endif
;
; negative branch
;
if (gt1neg(0) ne -1) then begin

 x0=-x(gt1neg)

 x2=x0*x0

 fn=a4+x2*(a3+x2*(a2+x2*(a1+x2)))
 fd=b4+x2*(b3+x2*(b2+x2*(b1+x2)))

 f=(fn/fd)/x0

 gn=c4+x2*(c3+x2*(c2+x2*(c1+x2)))
 gd=d4+x2*(d3+x2*(d2+x2*(d1+x2)))

 g=(gn/gd)/x2

 z(gt1neg)=-.5D*!PI+f*cos(x0)+g*sin(x0)

endif

return,z

end
