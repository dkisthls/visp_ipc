function sinc,x

  z=sin(x)

  dump=where(x,complement=zero)

  if (zero ne -1) then begin
    x(zero)=1.D
    z(zero)=1.D
  endif

  return,z/x

end
