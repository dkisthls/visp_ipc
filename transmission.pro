; This code calculates the ViSP transmission function at each wavelength of
; the DKIST spectrum, based on the assumed transmission of individual optics.
;
; The irradiance at the slit is calculated in flux.pro.
;
; ViSP optics are handled here. Instrument optics transmission except for the
; order-sorting filter is pre-computed from vendor-provided optic coating
; curves and glass transmission data.
;
; To determine the order sorting filter transmission we search the catalog for
; the best filter and apply that transmission curve.

function transmission, lambda, arm, filter
	; use cubic spline interpolation
	spline = 1

	; read in transmission data
	td = get_transmission_data()

	; find the filter transmission and name
	index = (lambda - td.lambda[0]) / (td.lambda[-1] - td.lambda[0]) * (n_elements(td.lambda) - 1)
	if filter ge 0 then begin
		fn = td.nfilter[filter]
		ft = interpolate(smooth(reform(td.tFilter[filter,*]), 5, /edge_truncate), index, $
			cubic=-0.5*spline)
	endif else begin
		; special case, there is no filter that works, transmission is set equal to 0
		fn = '         '
		ft = 0.
	endelse

	; calculate transmittance
	trans_p = ft * (interpol(td.tArm[*,0,arm], td.lambda, lambda, spline=spline))[0]
	trans_s = ft * (interpol(td.tArm[*,1,arm], td.lambda, lambda, spline=spline))[0]
	transPS = [trans_p, trans_s]

	return, {trans:transPS, filter:fn}
end
