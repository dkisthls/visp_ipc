function vignetting, alpha, position, lambda, arm, grating
	visp = get_visp_parameters()

	; determine anamorphic magnification
	anamorph = anamorph(alpha, position)

	; get the camera lens width
	arm_p = get_arm_parameters(lambda, arm)

	; calculate vignetting
	return, min([visp.collimator_width, $
		cos(!dtor * alpha) * visp.gratings[grating].width, $
		anamorph * arm_p.lens_width]) / visp.collimator_width
end
