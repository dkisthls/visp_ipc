FUNCTION VISP_CW_FIELD, Parent, COLUMN=Column, ROW=Row, $
	EVENT_FUNC = efun, $
	DOUBLE=doubleIn, ULONG=ulongIn, $
	FOCUS_EVENTS=focusEventsIn, $
	FLOATING=Float, INTEGER=Int, LONG=Long, STRING=String, $
	FONT=LabelFont, FRAME=Frame, TITLE=Title, VALUE=TextValueIn, $
	RETURN_EVENTS=ReturnEvents, ALL_EVENTS=AllUpdates, $
	FIELDFONT=FieldFont, NOEDIT=NoEdit, TEXT_FRAME=Text_Frame, $
	XSIZE=XSize, YSIZE=YSize, UNAME=uname, TAB_MODE=tab_mode, $
	IGNORE_ACCELERATORS=ignoreAccel, $
	XOFFSET=xoffset, YOFFSET=yoffset
;   FLOOR=vmin, CEILING=vmax

	resolve_routine, 'cw_field', /no_recompile, /compile_full_file, /is_function

;   Examine our keyword list and set default values
;   for keywords that are not explicitly set.

    Column      = KEYWORD_SET(Column)
    Row         = 1 - Column
    AllEvents       = 1 - KEYWORD_SET(NoEdit)

    ; Enum Update { None, All, CRonly }
    Update      = 0
    IF KEYWORD_SET(AllUpdates) THEN Update  = 1
    IF KEYWORD_SET(ReturnEvents) THEN Update    = 2

    IF N_ELEMENTS(efun) LE 0 THEN efun = ''
    IF N_ELEMENTS(Title) EQ 0 THEN Title='Input Field:'
    TextValue = (N_ELEMENTS(TextValueIn) gt 0) ? TextValueIn : ''
    ; Convert non-string values to strings.
    if (SIZE(TextValue, /TNAME) ne 'STRING') then $
        TextValue = STRTRIM(TextValue,2)
    IF N_ELEMENTS(YSize) EQ 0 THEN YSize=1
    IF N_ELEMENTS(uname) EQ 0 THEN uname='CW_FIELD_UNAME'

    Type    = 0 ; string is default
    IF KEYWORD_SET(Float) THEN  Type    = 1
    IF KEYWORD_SET(Int) THEN    Type    = 2
    IF KEYWORD_SET(Long) THEN   Type    = 3
    if KEYWORD_SET(ulongIn) then  Type    = 4
    if KEYWORD_SET(doubleIn) then  Type    = 5

    ;   Don't allow multiline non string widgets
    if (Type ne 0) then $
        YSize=1
    YSize = YSize > 1

    ;   Build Widget

    Base    = WIDGET_BASE(Parent, ROW=Row, COLUMN=Column, UVALUE=TextValueIn, $
            EVENT_FUNC='CW_FIELD_EVENT', $
            PRO_SET_VALUE='CW_FIELD_SET', $
            FUNC_GET_VALUE='CW_FIELD_GET', $
            FRAME=Frame, UNAME=uname, XPAD=0, YPAD=0, $
            XOFFSET=xoffset, YOFFSET=yoffset)

    if ( N_ELEMENTS(tab_mode) ne 0 ) then $
        WIDGET_CONTROL, Base, TAB_MODE = tab_mode

    if strlen(Title) gt 0 then $
        Label   = WIDGET_LABEL(Base, VALUE=Title, FONT=LabelFont, $
            UNAME=uname+'_LABEL')
    Text    = WIDGET_TEXT(Base, VALUE=TextValue, $
            XSIZE=XSize, YSIZE=YSize, FONT=FieldFont, $
            ALL_EVENTS=AllEvents, $
            EDITABLE=(AllEvents AND TYPE EQ 0), $
            FRAME=Text_Frame , $
            KBRD_FOCUS_EVENTS=KEYWORD_SET(focusEventsIn), $
            IGNORE_ACCELERATORS=ignoreAccel, $
            UNAME=uname+'_TEXT')

            ; NO_ECHO=(AllEvents AND (TYPE NE 0)))

    ; Save our internal state in the first child widget
    State   = {     $
    efun: efun,         $
    TextId:Text,        $
    Title:Title,        $
    Update:Update,      $
    Type:Type       $
    }
    WIDGET_CONTROL, WIDGET_INFO(Base, /CHILD), SET_UVALUE=State, /NO_COPY
    RETURN, Base
END
